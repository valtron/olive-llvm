from io import StringIO
from olive.utils import Messages
from olive.trans.parse import Parser
from olive.phase import ast

def test_parser():
	src = StringIO('''
		module a; module b.c;
	''')
	
	msg = Messages()
	parser = Parser(msg)
	
	astsrc = ast.Source()
	tree = ast.AST([astsrc])
	parser.parse(src, astsrc)
	
	pod = ast.utils.to_pod(astsrc.members)
	
	assert pod == (
		('Module',
			('Name', None, ('ExprName', 'a', None)), None
		),
		('Module',
			('Name', ('Name', None, ('ExprName', 'b', None)), ('ExprName', 'c', None)), None
		),
	)

use sys.process: Console;
use olive: lang as ol;

void main(Console console) {
	my c = console;
	my x = "x";
	my tx = ol.typeof(x);
	my ttx = ol.typeof(tx);
	
	t(c, tx.name.eq("olive.lang.String"));
	t(c, ttx.name.eq("olive.lang.Type"));
	t(c, ol.isinstance(x, tx));
	t(c, not ol.isinstance(x, ttx));
}

void t(Console c, bool b) {
	c.print(if b: "."; else: "F";);
}

use sys.process: Console;
use tmpdev: List, i2s;

void main(List[str] args, Console console) {
	my assert = Asserter(console);
	my l = List[int](5);
	
	assert(l.capacity, 5, "Capacity should have been set via constructor");
	assert(l.count, 0, "Length should be initialized to 0");
	
	l.push(1);
	assert(l.capacity, 5, "Capacity should not have changed after 1 push");
	assert(l.count, 1, "Push should increment count");
	
	l.push(2); l.push(3);
	assert(l(0), 1, "Push should add to the end of the list");
	assert(l(1), 2, "Push should add to the end of the list");
	assert(l(2), 3, "Push should add to the end of the list");
	assert(l.capacity, 5, "Capacity should not have changed yet");
	assert(l.count, 3, "Length should now be 3");
	
	my first = l.pop();
	assert(l(0), 1, "Pop should remove top");
	assert(l.count, 2, "Length should have reduced to 2");
	assert(l.capacity, 5, "Capacity should not have changed yet");
	assert(first, 3, "Popped value should be 3");
	
	my second = l.pop();
	assert(l(0), 1, "Pop should remove top");
	assert(l.count, 1, "Length should have reduced to 1");
	assert(l.capacity, 5, "Capacity should not have changed yet");
	assert(second, 2, "Popped value should be 2");
	
	l.unshift(4);
	assert(l(0), 4, "Values should have shifted to the right");
	assert(l.count, 2, "Length should have incresased to 2");
	assert(l.capacity, 5, "Capacity should not have changed yet");
	
	l.unshift(5); l.unshift(6);
	assert(l(0), 6, "Unshift should add to the beginning of the list");
	assert(l(1), 5, "Unshift should add to the beginning of the list");
	assert(l(2), 4, "Unshift should add to the beginning of the list");
	assert(l.capacity, 5, "Capacity should not have changed yet");
	assert(l.count, 4, "Length should now be 4");
	
	l.unshift(7); l.unshift(8);
	assert(l.capacity, 10, "Unshift should cause the list to grow");
	assert(l.count, 6, "Length should now be 5");
	
	first = l.shift();
	assert(l.capacity, 10, "Capacity should not have changed");
	assert(l.count, 5, "Shift should cause length to reduce to 4");
	assert(first, 8, "Shift should remove from the beginning of the list");
	assert(l(0), 7, "Values should have shifted left");
	assert(l(1), 6, "Values should have shifted left");
	assert(l(2), 5, "Values should have shifted left");
}

class Asserter {
	Console c;
	
	new (Console c) {
		this.c = c;
	}
	
	void this(int expected, int actual, str msg) {
		if expected <> actual {
			this.c.print(i2s(expected) ~ " <> " ~ i2s(actual));
			this.c.print("; Assertion FAILED: " ~ msg ~ "\n");
		} else {
			this.c.print(".");
		}
	}
}

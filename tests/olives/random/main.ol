use sys.process: Console;
use tmpdev: Filesystem;

use tmpdev: List, i2s;

void main(List[str] args, Console console, Filesystem fsys) {
	my p = ArgProcessor(console, fsys);
	
	for arg in args {
		p.process(arg);
	}
	
	if args.count > 0 {
		my w = fsys.writer(args(0) ~ "/test.txt");
		w.write("Hello, world!");
		w.dispose();
	}
}

class ArgProcessor {
	Console c;
	Filesystem fsys;
	
	new (Console c, Filesystem fsys) {
		this.c = c;
		this.fsys = fsys;
	}
	
	void process(str arg) {
		my c = this.c;
		my fsys = this.fsys;
		
		if fsys.isfile(arg) {
			c.print("'" ~ arg ~ "' is a file containing:\n");
			
			my r = fsys.reader(arg);
			
			while r.read(100) {
				c.print(r.current);
			}
			
			r.dispose();
		} else if fsys.isdir(arg) {
			my l = fsys.list(arg);
			
			c.print("'" ~ arg ~ "' contains " ~ i2s(l.count) ~ " entries:\n");
			
			for child in l {
				c.print("- " ~ child ~ "\n");
			}
		} else {
			c.print("'" ~ arg ~ "' is null\n");
		}
		
		c.print("parent: " ~ fsys.parent(arg) ~ "\n");
	}
}

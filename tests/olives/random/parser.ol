use olive: lang as ol;
use sys.process: Console;
use tmpdev: Filesystem, List;
use orc.parse: Lexer, FileWindow;
use orc: parse as op;

void main(List[str] args, Console console, Filesystem fsys) {
	my c = console;
	
	for arg in args {
		c.print("parse: ");
		c.print(arg);
		c.print("... ");
		
		if not fsys.isfile(arg) {
			c.print("not a file\n");
			next;
		}
		
		my fr = fsys.reader(arg);
		my result = op.parse(Lexer(FileWindow(fr)));
		fr.dispose();
		
		my t = ol.typeof(result);
		c.print(t.name);
		c.print("\n");
	}
}

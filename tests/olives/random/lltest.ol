use orc: llvm;

void main() {
	my ctx = llvm.CreateContext();
	my mod = ctx.createModule("xyz");
	mod.dump();
	ctx.dispose();
}

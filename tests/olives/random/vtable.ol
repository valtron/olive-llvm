use sys.process: Console;

void main(Console console) {
	my c = console;
	my x = X();
	my y = Y();
	my z = Z();
	
	t(c, x is X);
	t(c, not (x is Y));
	t(c, not (x is Z));
	t(c, x is R1);
	t(c, x is R2);
	t(c, not (x is R3));
	
	t(c, y is X);
	t(c, y is Y);
	t(c, not (y is Z));
	t(c, y is R1);
	t(c, y is R2);
	t(c, not (y is R3));
	
	t(c, z is X);
	t(c, not (z is Y));
	t(c, z is Z);
	t(c, z is R1);
	t(c, z is R2);
	t(c, z is R3);
}

void t(Console c, bool b) {
	c.print(if b: "."; else: "F";);
}

role R1 {}
role R2: R1 {}
role R3 {}

class X: R2 { new () {} }
class Y: X, R1 { new () {} }
class Z: R3, X { new () {} }

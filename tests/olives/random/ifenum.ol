void main() {}

enum E { X; }

E foo(bool b, X x, Y y) -> if b: x; else: y;

class X {}
class Y {}

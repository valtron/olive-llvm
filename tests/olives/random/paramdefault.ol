use sys.process: Console;

void main(Console console) {
	t(console, f(1, 2, 3) = 6);
	t(console, f(1) = 8);
}

int f(int a, int b = 1, int c = f(1, 2, 3)) {
	ret a + b + c;
}

void t(Console c, bool b) {
	c.print(if b: "."; else: "F";);
}

module test;

class A {
	int x;
	bool y;
	
	new (int x, bool y) {
		this.x = x;
		this.y = y;
	}
}

class B: A {
	int z;
	bool x;
	
	new (int z, bool x) {
		this.z = z;
		this.x = x;
		this.y = x;
	}
}

void main() {
	my a = A(1, #t);
	my b = B(2, #f);
	a.x = b.z;
	a.y = b.x;
	b.y = a.y;
}

use sys.process: Console;
use tmpdev: List, Hashtable, i2s;

void main(List[str] args, Console console) {
	my c = console;
	my h = Hashtable[any](127, #null);
	
	my cond = #t;
	my i = 0;
	
	while i < 1000 {
		cond = cond and (h(i) is #null);
		i = i + 1;
	}
	t(c, cond);
	
	i = 0;
	while i < 25 {
		h(i) = #foo;
		i = i + 1;
	}
	while i < 50 {
		h(i) = #bar;
		i = i + 1;
	}
	
	i = 0;
	
	cond = #t;
	while i < 25 {
		cond = cond and (h(i) is #foo);
		i = i + 1;
	}
	t(c, cond);
	
	cond = #t;
	while i < 50 {
		cond = cond and (h(i) is #bar);
		i = i + 1;
	}
	t(c, cond);
	
	cond = #t;
	while i < 500 {
		cond = cond and (h(i) is #null);
		i = i + 1;
	}
	t(c, cond);
	
	c.print("\n");
}

void t(Console c, bool b) {
	c.print(if b: "."; else: "F";);
}

use sys.process: Console;

str GLOBAL = "GLOBAL.initialValue";

void main(Console console) {
	console.print(GLOBAL ~ "\n");
	GLOBAL = "GLOBAL.subsequentValue";
	console.print(GLOBAL ~ "\n");
	
	console.print(foo.BAR ~ "\n");
	foo.BAR = "foo.BAR.subsequentValue";
	console.print(foo.BAR ~ "\n");
}

module foo;

str BAR = "foo.BAR.initialValue";

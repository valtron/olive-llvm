use sys.process: Console;

use olive.lang: typeof;
use tmpdev: List, i2s;

void
main(Console console) {
	my assert = Asserter(console);
	
	assert(typeof(1).name.eq("olive.lang.Int"));
	
	my l = List[int](5);
	
	my i = 0;
	while i < 5 {
		l.push(i);
		assert(l(i) = i);
		i = i + 1;
	}
}

class Asserter {
	new (Console c) {
		this.c = c;
	}
	
	Console c;
	
	void this(bool b) {
		this.c.print(if b: "."; else: "F";);
	}
}

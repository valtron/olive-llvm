module test;

class A {
	new () {}
	
	A foo {
		get { ret this; }
		set { }
	}
}

void main() {
	my a = A();
	a.foo = a.foo;
}

module test;

class A {
	int x;
	
	void foo(A a) {}
}

class B: A {
	bool f(bool b) { ret #f; }
}

bool g(A a) { ret #t; }

void f(A a, B b) {
	a.foo(b);
	my r1 = g(a);
	my t2 = g(b);
	b.f(r1);
}

void main() {}

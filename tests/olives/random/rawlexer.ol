use sys.process: Console;
use tmpdev: Filesystem, List, i2s;
use orc.ast: Location, Token;
use orc.parse: Lexer, FileWindow;

void main(List[str] args, Console console, Filesystem fsys) {
	for arg in args {
		console.print("lex: ");
		console.print(arg);
		console.print("... ");
		
		if not fsys.isfile(arg) {
			console.print("not a file\n");
			next;
		}
		
		console.print("\n");
		
		my fr = fsys.reader(arg);
		my fw = FileWindow(fr);
		my l = Lexer(fw);
		
		while l.moveNext() {
			console.print(tok2str(l.current));
			console.print("\n");
		}
		
		fr.dispose();
		
		console.print("\n");
	}
}

str tok2str(Token t) {
	ret range2str(t.start, t.end) ~ " " ~ i2s(t.type) ~ " [" ~ t.data ~ "]";
}

str range2str(Location start, Location end) {
	ret loc2str(start) ~ "-" ~ loc2str(end);
}

str loc2str(Location loc) {
	ret i2s(loc.line) ~ ":" ~ i2s(loc.col);
}

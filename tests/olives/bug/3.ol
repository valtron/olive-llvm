use olive: lang as ol;

class R[T] {
	tmpdev.List[T] _impl;
	ol.Iterator[T] iter() -> this._impl.iter();
}

void main() {}

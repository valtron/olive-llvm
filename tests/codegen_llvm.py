from olive.phase import sem
from olive.trans.codegen import llvm, compiler, codewalker
from olive.utils import Messages

def test_builder():
	cu = sem.CompilationUnit(sem.Package('test'))
	cp = compiler.Compiler(Messages(), cu)
	
	lmod = cp.lmod
	lint = llvm.IntType(32)
	lfunc = llvm.Function(lmod, llvm.FunctionType(lint, [lint, lint]), 'f')
	
	B = llvm.Builder(lfunc, cp)
	
	a0 = B.createVar(lfunc.args[0].type, 'a0')
	B.store(a0, lfunc.args[0])
	a1 = B.createVar(lfunc.args[1].type, 'a1')
	B.store(a1, lfunc.args[1])
	
	bl1 = B.startBlock()
	bl2 = B.startBlock()
	
	l0 = B.load(a0)
	l1 = B.load(a1)
	
	B.branch(B.notb(B.equal(l0, l1)), bl1, bl2)
	
	B.setBlock(bl1)
	B.jump(bl2)
	B.setBlock(bl2)
	B.ret(l0)
	
	mod_str = _std(str(lmod))
	expected = _std('''
		; ModuleID = "test"
		target triple = "unknown-unknown-unknown"
		target datalayout = ""
		
		define i32 @"f"(i32 %".1", i32 %".2")
		{
		entry:
			%"a0" = alloca i32, i32 0
			%"a1" = alloca i32, i32 0
			br label %"bl"
		bl:
			store i32 %".1", i32* %"a0"
			store i32 %".2", i32* %"a1"
			%".7" = load i32, i32* %"a0"
			%".8" = load i32, i32* %"a1"
			%".9" = sext i32 %".7" to i64
			%".10" = sext i32 %".8" to i64
			%".11" = icmp eq i64 %".9", %".10"
			%".12" = xor i1 %".11", -1
			br i1 %".12", label %"bl.1", label %"bl.2"
		bl.1:
			br label %"bl.2"
		bl.2:
			ret i32 %".7"
		}
	''')
	
	assert mod_str == expected

def test_codewalker():
	cu = sem.CompilationUnit(sem.Package('test'))
	cu.packs.append(_mock_olive_lang(cu))
	cp = compiler.Compiler(Messages(), cu)
	
	lmod = cp.lmod
	lint = llvm.IntType(32)
	lfunc = llvm.Function(lmod, llvm.FunctionType(lint, [lint, lint]), 'f')
	
	fint = cu.getIntForm()
	fbool = cu.getBoolForm()
	
	sig = sem.FuncSig(fint, None, [sem.Param(fint, 'a0'), sem.Param(fint, 'a1')], None)
	func = sem.Func(None, 'f',
		sig = sig, kind = sem.FuncKind.Func,
		proto = None, isAbstract = False,
	)
	
	stmt = sem.BlockStmt([
		sem.IfStmt(
			sem.NotExpr(fbool, sem.IsExpr(fbool, sig.params[0], sig.params[1])),
			sem.BlockStmt([]),
			sem.BlockStmt([]),
		),
		sem.RetStmt(sig.params[0]),
	])
	
	cw = codewalker.CodeWalker(cp, cu, cp.msg, func, lfunc)
	cw.runStmt(stmt)
	
	mod_str = _std(str(lmod))
	expected = _std('''
		; ModuleID = "test"
		target triple = "unknown-unknown-unknown"
		target datalayout = ""
		
		define i32 @"f"(i32 %".1", i32 %".2")
		{
		entry:
			%"a0" = alloca i32, i32 0
			%"a1" = alloca i32, i32 0
			br label %"bl"
		bl:
			store i32 %".1", i32* %"a0"
			store i32 %".2", i32* %"a1"
			%".7" = load i32, i32* %"a0"
			%".8" = load i32, i32* %"a1"
			%".9" = sext i32 %".7" to i64
			%".10" = sext i32 %".8" to i64
			%".11" = icmp eq i64 %".9", %".10"
			%".12" = xor i1 %".11", -1
			br i1 %".12", label %"bl.1", label %"bl.2"
		bl.1:
			br label %"bl.3"
		bl.2:
			br label %"bl.3"
		bl.3:
			%".16" = load i32, i32* %"a0"
			ret i32 %".16"
		}
	''')
	
	assert mod_str == expected

def _mock_olive_lang(cu):
	pkg = sem.Package('olive.lang')
	
	tint = sem.ClassType(None, 'Int',
		proto = None, layout = sem.Layout.Bit32, kind = sem.ClassKind.Class
	)
	pkg.items.append(tint)
	tbool = sem.EnumType(None, 'Bool', proto = None)
	tbool.options.append(cu.getSymbolType('t'))
	tbool.options.append(cu.getSymbolType('f'))
	pkg.items.append(tbool)
	
	return pkg

def _std(s):
	import re
	s = re.sub(r'\s+', ' ', s)
	s = s.strip()
	return s

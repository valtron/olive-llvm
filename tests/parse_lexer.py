from io import StringIO
from olive.trans.parse.impl import Lexer
from olive.trans.parse.base import FileWindow
from olive.phase.ast import gentokens as tokens, Token, Location, Range

def test_lexer():
	src = StringIO('''		hello, world!
		
		class foo { 34 #a }
		"this is \\a string"
		// comment
		not comment
		/* comm
		ent */
		&
	''')
	
	toks = tuple(_lex_iter(Lexer(FileWindow(src))))
	
	assert toks == (
		Token(Range(Location(0,  2), Location(0,  7)), tokens.Id, 'hello'),
		Token(Range(Location(0,  7), Location(0,  8)), tokens.Comma, ','),
		Token(Range(Location(0,  9), Location(0, 14)), tokens.Id, 'world'),
		Token(Range(Location(0, 14), Location(0, 15)), tokens.Excl, '!'),
		Token(Range(Location(2,  2), Location(2,  7)), tokens.KClass, 'class'),
		Token(Range(Location(2,  8), Location(2, 11)), tokens.Id, 'foo'),
		Token(Range(Location(2, 12), Location(2, 13)), tokens.BCL, '{'),
		Token(Range(Location(2, 14), Location(2, 16)), tokens.Num, '34'),
		Token(Range(Location(2, 17), Location(2, 19)), tokens.Sym, '#a'),
		Token(Range(Location(2, 20), Location(2, 21)), tokens.BCR, '}'),
		Token(Range(Location(3,  2), Location(3, 21)), tokens.Str, '"this is \\a string"'),
		Token(Range(Location(5,  2), Location(5,  5)), tokens.KNot, 'not'),
		Token(Range(Location(5,  6), Location(5, 13)), tokens.Id, 'comment'),
		Token(Range(Location(8,  2), Location(8,  3)), tokens.Amp, '&'),
		Token(Range(Location(9,  1), Location(9,  1)), tokens.EOF, ''),
	)

def _lex_iter(lexer):
	while lexer.moveNext():
		yield lexer.current

from io import StringIO
from olive.utils import Messages
from olive.trans.parse import Parser
from olive.trans.analyze import SyntaxProcessor
from olive.phase import ast, sem

def test_parser():
	src = StringIO('''
		module a;
		
		enum T { #x; }
		
		use C: X as Z;
		
		class C {
			T x;
			T f(T x) {}
			T g {
				get -> #x;
				set {}
			}
			
			enum X {}
		}
		enum E {
			#true;
			
			use C.X;
			
			C;
			X;
		}
		T x = #x;
		T f(Z z) -> #x;
		
		module b.c;
		
		use a.C;
	''')
	
	msg = Messages()
	parser = Parser(msg)
	
	astsrc = ast.Source()
	tree = ast.AST([astsrc])
	parser.parse(src, astsrc)
	
	assert not msg.hasErrors
	
	cu = sem.CompilationUnit(sem.Package('test'))
	sp = SyntaxProcessor(msg, cu, suppress_predefines = True)
	sp.process(tree)
	
	cmp = Comparer()
	mtest = cmp.add(sem.Module, name = 'test', parent = None)
	ma = cmp.add(sem.Module, name = 'a', parent = mtest)
	mb = cmp.add(sem.Module, name = 'b', parent = mtest)
	mc = cmp.add(sem.Module, name = 'c', parent = mb)
	mT = cmp.add(sem.EnumType, name = 'T', parent = ma)
	mC = cmp.add(sem.ClassType, name = 'C', parent = ma)
	mE = cmp.add(sem.EnumType, name = 'E', parent = ma)
	mx = cmp.add(sem.GlobalVar, name = 'x', parent = ma)
	mf = cmp.add(sem.Func, name = 'f', parent = ma)
	mCX = cmp.add(sem.EnumType, name = 'X', parent = mC)
	mCf = cmp.add(sem.Func, name = 'f', parent = mC)
	mUnit = cmp.add(sem.EnumType, name = 'Unit', parent = None)
	mg_get = cmp.add(sem.Func, name = 'g', parent = mC, _checks = [
		(lambda f: f.sig.value is None),
	])
	mg_set = cmp.add(sem.Func, name = 'g', parent = mC, _checks = [
		(lambda f: f.sig.value is not None),
	])
	
	cmp.assert_equal_unordered(cu.items)

class Comparer:
	__slots__ = ('expected', '_mapping')
	
	def __init__(self):
		self.expected = []
		self._mapping = {}
	
	def add(self, _type, **fields):
		checks = fields.pop('_checks', None) or []
		obj = ObjInfo(_type, fields, checks)
		self.expected.append(obj)
		return obj
	
	def assert_equal_unordered(self, actual):
		unused = set(actual)
		
		for obj in self.expected:
			found = False
			for i in unused:
				if type(i) != obj.type: continue
				all_fields_match = True
				for fn, fv in obj.fields.items():
					if fv in self._mapping: fv = self._mapping[fv]
					try:
						if getattr(i, fn) == fv: continue
					except AttributeError: pass
					all_fields_match = False
					break
				if all_fields_match:
					all_checks_match = True
					for c in obj.checks:
						if not c(i):
							all_checks_match = False
							break
					if all_checks_match:
						found = True
						break
			
			if found:
				unused.remove(i)
				self._mapping[obj] = i
		
		assert not unused

class ObjInfo:
	__slots__ = ('type', 'fields', 'checks')
	
	def __init__(self, type, fields, checks):
		self.type = type
		self.fields = fields
		self.checks = checks

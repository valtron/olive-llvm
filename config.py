import os
import common

class BuildTarget:
	def __init__(self):
		self.CC = os.environ.get('CC') or common.which('gcc')
		self.CXX = os.environ.get('CXX') or common.which('g++')
		self.LLC = os.environ.get('LLC') or common.which('llc')
		self.OLIVE = 'bin/main'
	
	@classmethod
	def Get(cls):
		return target

target = BuildTarget()

module orc.llvm;

use olive: lang as ol;

Context CreateContext() __link 'LLVMContextCreate';

class Context(#addr): ol.Disposable {
	Module createModule(str name) -> _implCreateModule(name._data, this);
	
	void dispose() __link 'LLVMContextDispose';
}

class Module(#addr): ol.Disposable {
	Type? getType(str name) -> _implGetTypeByName(this, name._data);
	Function? getFunction(str name) -> _implGetNamedFunction(this, name._data);
	Function addFunction(str name, FunctionType type) -> _implAddFunction(this, name._data, type);
	void dump() __link 'LLVMDumpModule';
	
	void dispose() __link 'LLVMDisposeModule';
}

class Type(#addr) {
	void dump() __link 'LLVMDumpType';
}

class FunctionType(#addr): Type {
	
}

class Value(#addr) {
	Type type __link 'LLVMTypeOf';
	
	void dump() __link 'LLVMDumpValue';
}

class Constant(#addr): Value {
	
}

class Function(#addr): Constant {
	Params params -> ol.cast[Params](this);
}

class Params(#addr): ol.Iterable[Value] {
	int count __link 'LLVMCountParams';
	Value this(int i) __link 'LLVMGetParam';
	
	ol.Iterator[Value] iter() -> ParamsIterator(this);
}

class ParamsIterator: ol.Iterator[Value] {
	Params _p;
	int _c;
	int _i;
	
	new (Params p) {
		this._p = p;
		this._c = p.count;
		this._i = -1;
	}
	
	bool moveNext() {
		this._i = this._i + 1;
		ret this._i < this._c;
	}
	
	Value current -> this._p(this._i);
}

Module _implCreateModule(ol.Address name, Context c) __link 'LLVMModuleCreateWithNameInContext';
Type? _implGetTypeByName(Module m, ol.Address name) __link 'LLVMGetTypeByName';
Function? _implGetNamedFunction(Module m, ol.Address name) __link 'LLVMGetNamedFunction';
Function _implAddFunction(Module m, ol.Address name, FunctionType type) __link 'LLVMAddFunction';

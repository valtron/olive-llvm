module orc.parse;
use orc: ast;
use tmpdev: List;
use olive.lang: cast;
use orc.ast: Syntax, Stmt, Expr, ListSyntax;
use orc.ast: ClassDef, EnumDef, EnumMember, ExprAtomic, ExprBinary, ExprCall, ExprDot, ExprName, ExprStmt, ExprUnary, FuncCode, FuncEtter, FuncParam, GenericParam, Leaf, LeafBody, LeafInitializer, LeafName, LeafNameCtor, LeafNameOp, Module, ModuleMember, Name, NonModule, Oper, OperNot, StmtAssign, StmtBlock, StmtDo, StmtExpr, StmtFlow, StmtFor, StmtIf, StmtLoop, StmtRet, StmtSwitch, StmtUse, StmtVar, StmtWhile, SwitchCase, TypeRef, UseAlias;
void init_actions() {
	my A = ACTION;
	A(128) = Shift(14);
	A(0) = Reduce(64, 70, 0);
	A(101) = Shift(9);
	A(10) = Shift(16);
	A(107) = Shift(18);
	A(110) = Shift(25);
	A(111) = Shift(13);
	A(25) = Reduce(64, 70, 0);
	A(119) = Shift(10);
	A(121) = Shift(22);
	A(124) = Shift(12);
	A(133) = Shift(3);
	A(199) = Reduce(64, 0, 1);
	A(327) = Shift(14);
	A(300) = Shift(9);
	A(209) = Shift(16);
	A(306) = Shift(18);
	A(309) = Shift(25);
	A(310) = Shift(13);
	A(320) = Shift(22);
	A(318) = Shift(10);
	A(224) = Reduce(64, 0, 1);
	A(323) = Shift(12);
	A(332) = Shift(3);
	A(519) = Shift(22);
	A(408) = Shift(49);
	A(530) = Shift(53);
	A(-497) = Shift(16);
	A(-496) = Reduce(3, 77, 1);
	A(-495) = Reduce(55, 43, 1);
	A(-494) = #accept;
	A(-493) = Reduce(6, 0, 1);
	A(1724) = Reduce(103, 28, 0);
	A(1615) = Reduce(103, 28, 0);
	A(1616) = Reduce(103, 28, 0);
	A(1713) = Reduce(103, 28, 0);
	A(1602) = Reduce(103, 28, 0);
	A(1619) = Shift(330);
	A(1621) = Reduce(103, 28, 0);
	A(1622) = Reduce(103, 28, 0);
	A(-491) = Reduce(10, 0, 1);
	A(-490) = Shift(16);
	A(2321) = Reduce(10, 0, 1);
	A(2212) = Reduce(10, 0, 1);
	A(2213) = Reduce(10, 0, 1);
	A(2310) = Reduce(10, 0, 1);
	A(2199) = Reduce(10, 0, 1);
	A(2216) = Reduce(10, 0, 1);
	A(2215) = Shift(322);
	A(2219) = Reduce(10, 0, 1);
	A(2218) = Reduce(10, 0, 1);
	A(-488) = Reduce(41, 0, 1);
	A(-487) = Reduce(41, 0, 1);
	A(-486) = Reduce(41, 0, 1);
	A(-485) = Reduce(34, 0, 1);
	A(3316) = Reduce(90, 28, 0);
	A(3194) = Reduce(90, 28, 0);
	A(3207) = Reduce(90, 28, 0);
	A(3204) = Reduce(90, 28, 0);
	A(3205) = Reduce(90, 28, 0);
	A(3206) = Shift(314);
	A(3241) = Reduce(90, 28, 0);
	A(3208) = Reduce(90, 28, 0);
	A(3210) = Reduce(90, 28, 0);
	A(3211) = Reduce(90, 28, 0);
	A(3212) = Reduce(90, 28, 0);
	A(3213) = Reduce(90, 28, 0);
	A(3214) = Reduce(90, 28, 0);
	A(3236) = Reduce(90, 28, 0);
	A(3216) = Reduce(90, 28, 0);
	A(3284) = Reduce(90, 28, 0);
	A(3286) = Reduce(90, 28, 0);
	A(3224) = Reduce(90, 28, 0);
	A(3225) = Reduce(90, 28, 0);
	A(3226) = Reduce(90, 28, 0);
	A(3227) = Reduce(90, 28, 0);
	A(3228) = Reduce(90, 28, 0);
	A(3229) = Reduce(90, 28, 0);
	A(3230) = Reduce(90, 28, 0);
	A(3232) = Reduce(90, 28, 0);
	A(3233) = Reduce(90, 28, 0);
	A(3234) = Reduce(90, 28, 0);
	A(3235) = Reduce(90, 28, 0);
	A(3300) = Reduce(90, 28, 0);
	A(3301) = Reduce(90, 28, 0);
	A(3238) = Reduce(90, 28, 0);
	A(3239) = Reduce(90, 28, 0);
	A(3240) = Reduce(90, 28, 0);
	A(3305) = Reduce(90, 28, 0);
	A(3307) = Reduce(90, 28, 0);
	A(3309) = Reduce(90, 28, 0);
	A(3237) = Reduce(90, 28, 0);
	A(-483) = Reduce(34, 0, 1);
	A(-482) = Reduce(41, 0, 1);
	A(-481) = Reduce(6, 0, 1);
	A(4012) = Reduce(58, 28, 0);
	A(4013) = Reduce(58, 28, 0);
	A(4000) = Reduce(58, 28, 0);
	A(4002) = Shift(28);
	A(4004) = Reduce(58, 28, 0);
	A(4118) = Reduce(58, 28, 0);
	A(4008) = Reduce(58, 28, 0);
	A(4009) = Reduce(58, 28, 0);
	A(-479) = Shift(292);
	A(4410) = Reduce(43, 19, 1);
	A(4411) = Reduce(43, 19, 1);
	A(4398) = Reduce(43, 19, 1);
	A(4400) = Reduce(43, 19, 1);
	A(4402) = Reduce(43, 19, 1);
	A(4516) = Reduce(43, 19, 1);
	A(4388) = Shift(291);
	A(4407) = Reduce(43, 19, 1);
	A(-477) = Reduce(34, 0, 1);
	A(-476) = Reduce(34, 0, 1);
	A(-475) = Shift(26);
	A(5206) = Reduce(58, 28, 0);
	A(5207) = Reduce(58, 28, 0);
	A(5194) = Reduce(58, 28, 0);
	A(5196) = Shift(28);
	A(5198) = Reduce(58, 28, 0);
	A(5312) = Reduce(58, 28, 0);
	A(5202) = Reduce(58, 28, 0);
	A(5203) = Reduce(58, 28, 0);
	A(-473) = Shift(39);
	A(5582) = Reduce(56, 28, 0);
	A(5688) = Shift(29);
	A(5698) = Shift(30);
	A(5595) = Reduce(104, 86, 0);
	A(-471) = Reduce(56, 0, 1);
	A(-470) = Reduce(56, 0, 1);
	A(-469) = Shift(38);
	A(-468) = Reduce(98, 85, 1);
	A(-467) = Shift(37);
	A(6796) = Shift(35);
	A(6789) = Reduce(104, 0, 1);
	A(6975) = Reduce(56, 28, 0);
	A(7081) = Shift(29);
	A(7091) = Shift(30);
	A(-464) = Reduce(98, 58, 3);
	A(-463) = Reduce(58, 9, 3);
	A(-462) = Reduce(54, 81, 2);
	A(7889) = Shift(14);
	A(7862) = Shift(9);
	A(7894) = Shift(3);
	A(7786) = Reduce(60, 96, 0);
	A(7882) = Shift(22);
	A(7771) = Shift(16);
	A(7868) = Shift(18);
	A(7885) = Shift(12);
	A(7774) = Shift(43);
	A(7871) = Shift(25);
	A(7872) = Shift(13);
	A(8081) = Shift(22);
	A(7970) = Shift(49);
	A(8092) = Shift(53);
	A(7989) = Shift(52);
	A(-459) = Reduce(99, 21, 1);
	A(-458) = Shift(48);
	A(-457) = Shift(47);
	A(-456) = Reduce(100, 0, 1);
	A(9083) = Shift(14);
	A(9056) = Shift(9);
	A(8980) = Reduce(60, 0, 1);
	A(9066) = Shift(13);
	A(9076) = Shift(22);
	A(8965) = Shift(16);
	A(9062) = Shift(18);
	A(9079) = Shift(12);
	A(8968) = Shift(43);
	A(9065) = Shift(25);
	A(9088) = Shift(3);
	A(-454) = Reduce(99, 29, 2);
	A(-453) = Reduce(100, 0, 2);
	A(-452) = Reduce(31, 60, 6);
	A(9783) = Reduce(77, 0, 1);
	A(9784) = Reduce(77, 0, 1);
	A(9771) = Reduce(77, 0, 1);
	A(9773) = Reduce(77, 0, 1);
	A(9775) = Reduce(77, 0, 1);
	A(9889) = Reduce(77, 0, 1);
	A(9779) = Shift(268);
	A(9780) = Reduce(77, 0, 1);
	A(-450) = Reduce(77, 0, 1);
	A(-449) = Reduce(77, 0, 1);
	A(-448) = Reduce(100, 0, 2);
	A(-447) = Reduce(77, 0, 1);
	A(10778) = Reduce(58, 28, 0);
	A(10779) = Reduce(58, 28, 0);
	A(10766) = Reduce(58, 28, 0);
	A(10768) = Shift(28);
	A(10770) = Reduce(58, 28, 0);
	A(10884) = Reduce(58, 28, 0);
	A(10774) = Reduce(58, 28, 0);
	A(10775) = Reduce(58, 28, 0);
	A(10977) = Reduce(9, 28, 0);
	A(10978) = Reduce(9, 28, 0);
	A(10965) = Shift(56);
	A(10969) = Reduce(9, 28, 0);
	A(11083) = Reduce(9, 28, 0);
	A(10974) = Reduce(9, 28, 0);
	A(11154) = Shift(16);
	A(11165) = Reduce(36, 95, 0);
	A(11245) = Shift(9);
	A(11375) = Shift(59);
	A(11376) = Shift(63);
	A(11481) = Shift(66);
	A(11372) = Shift(65);
	A(11367) = Shift(60);
	A(-442) = Reduce(17, 0, 1);
	A(11871) = Shift(70);
	A(11872) = Shift(69);
	A(11873) = Shift(86);
	A(11875) = Shift(74);
	A(11753) = Shift(84);
	A(11751) = Shift(16);
	A(11752) = Shift(89);
	A(11849) = Shift(78);
	A(11754) = Shift(95);
	A(11788) = Shift(94);
	A(11856) = Shift(101);
	A(11761) = Shift(97);
	A(11782) = Shift(71);
	A(11864) = Shift(79);
	A(12069) = Shift(251);
	A(12070) = Shift(70);
	A(12071) = Shift(111);
	A(12072) = Shift(86);
	A(12073) = Shift(3);
	A(12074) = Shift(74);
	A(12075) = Shift(126);
	A(11950) = Shift(16);
	A(11951) = Shift(89);
	A(11952) = Shift(84);
	A(11953) = Shift(95);
	A(11960) = Shift(97);
	A(11965) = Reduce(61, 90, 0);
	A(12045) = Shift(124);
	A(12048) = Shift(118);
	A(12053) = Shift(134);
	A(12054) = Shift(252);
	A(12055) = Shift(132);
	A(12058) = Shift(136);
	A(12060) = Shift(112);
	A(12062) = Shift(129);
	A(12067) = Shift(127);
	A(-439) = Reduce(17, 0, 1);
	A(-438) = Reduce(48, 68, 5);
	A(12667) = Shift(70);
	A(12668) = Shift(69);
	A(12669) = Shift(86);
	A(12671) = Shift(74);
	A(12645) = Shift(78);
	A(12547) = Shift(16);
	A(12548) = Shift(89);
	A(12549) = Shift(84);
	A(12550) = Shift(95);
	A(12584) = Shift(94);
	A(12652) = Shift(101);
	A(12557) = Shift(97);
	A(12578) = Shift(71);
	A(12660) = Shift(79);
	A(-436) = Reduce(59, 0, 1);
	A(-435) = Reduce(59, 28, 1);
	A(-434) = Shift(67);
	A(-433) = Shift(68);
	A(-432) = Reduce(59, 9, 3);
	A(13861) = Shift(70);
	A(13742) = Shift(89);
	A(13863) = Shift(86);
	A(13865) = Shift(74);
	A(13772) = Shift(71);
	A(13741) = Shift(16);
	A(13854) = Shift(79);
	A(13743) = Shift(84);
	A(13744) = Shift(95);
	A(13751) = Shift(97);
	A(13778) = Shift(94);
	A(-430) = Reduce(38, 0, 1);
	A(-429) = Reduce(8, 0, 1);
	A(14444) = Reduce(101, 28, 0);
	A(14458) = Reduce(101, 28, 0);
	A(14460) = Reduce(101, 28, 0);
	A(14462) = Reduce(101, 28, 0);
	A(14369) = Reduce(101, 28, 0);
	A(14338) = Reduce(101, 28, 0);
	A(14339) = Reduce(101, 28, 0);
	A(14340) = Reduce(101, 28, 0);
	A(14341) = Reduce(101, 28, 0);
	A(14375) = Reduce(101, 28, 0);
	A(14453) = Reduce(39, 0, 1);
	A(14348) = Reduce(101, 28, 0);
	A(14349) = Reduce(39, 0, 1);
	A(14352) = Reduce(39, 0, 1);
	A(14428) = Reduce(39, 0, 1);
	A(14451) = Shift(229);
	A(14356) = Reduce(39, 0, 1);
	A(14357) = Reduce(39, 0, 1);
	A(14358) = Reduce(39, 0, 1);
	A(14445) = Shift(232);
	A(14547) = Shift(159);
	A(14548) = Reduce(21, 0, 1);
	A(14584) = Reduce(21, 0, 1);
	A(14551) = Reduce(21, 0, 1);
	A(14553) = Shift(160);
	A(14555) = Reduce(21, 0, 1);
	A(14556) = Reduce(21, 0, 1);
	A(14557) = Reduce(21, 0, 1);
	A(14559) = Reduce(21, 0, 1);
	A(14627) = Reduce(21, 0, 1);
	A(14567) = Reduce(21, 0, 1);
	A(14568) = Reduce(21, 0, 1);
	A(14569) = Reduce(21, 0, 1);
	A(14570) = Reduce(21, 0, 1);
	A(14571) = Reduce(21, 0, 1);
	A(14572) = Reduce(21, 0, 1);
	A(14573) = Reduce(21, 0, 1);
	A(14575) = Reduce(21, 0, 1);
	A(14576) = Reduce(21, 0, 1);
	A(14577) = Reduce(21, 0, 1);
	A(14578) = Reduce(21, 0, 1);
	A(14643) = Reduce(21, 0, 1);
	A(14644) = Reduce(21, 0, 1);
	A(14581) = Reduce(21, 0, 1);
	A(14582) = Reduce(21, 0, 1);
	A(14583) = Reduce(21, 0, 1);
	A(14579) = Reduce(21, 0, 1);
	A(14650) = Reduce(21, 0, 1);
	A(14652) = Reduce(21, 0, 1);
	A(14580) = Reduce(21, 0, 1);
	A(-426) = Reduce(38, 0, 1);
	A(-425) = Reduce(29, 0, 1);
	A(-424) = Reduce(35, 0, 1);
	A(15344) = Reduce(4, 0, 1);
	A(15347) = Reduce(4, 0, 1);
	A(15351) = Reduce(4, 0, 1);
	A(15352) = Reduce(4, 0, 1);
	A(15353) = Reduce(4, 0, 1);
	A(15355) = Reduce(4, 0, 1);
	A(15439) = Reduce(4, 0, 1);
	A(15423) = Reduce(4, 0, 1);
	A(15363) = Reduce(4, 0, 1);
	A(15364) = Reduce(4, 0, 1);
	A(15365) = Shift(220);
	A(15366) = Shift(219);
	A(15367) = Shift(223);
	A(15369) = Reduce(4, 0, 1);
	A(15371) = Reduce(4, 0, 1);
	A(15372) = Reduce(4, 0, 1);
	A(15373) = Shift(222);
	A(15374) = Reduce(4, 0, 1);
	A(15375) = Reduce(4, 0, 1);
	A(15376) = Reduce(4, 0, 1);
	A(15377) = Reduce(4, 0, 1);
	A(15378) = Reduce(4, 0, 1);
	A(15379) = Reduce(4, 0, 1);
	A(15380) = Reduce(4, 0, 1);
	A(15446) = Reduce(4, 0, 1);
	A(15448) = Reduce(4, 0, 1);
	A(15440) = Reduce(4, 0, 1);
	A(-422) = Shift(110);
	A(-421) = Reduce(13, 0, 1);
	A(-420) = Reduce(20, 39, 1);
	A(-419) = Reduce(26, 0, 1);
	A(16450) = Shift(86);
	A(16441) = Shift(79);
	A(16338) = Shift(97);
	A(16452) = Shift(74);
	A(16448) = Shift(70);
	A(16359) = Shift(71);
	A(16328) = Shift(16);
	A(16329) = Shift(89);
	A(16330) = Shift(84);
	A(16331) = Shift(95);
	A(16365) = Shift(94);
	A(16617) = Shift(194);
	A(16538) = Reduce(0, 0, 1);
	A(16541) = Reduce(0, 0, 1);
	A(16545) = Reduce(0, 0, 1);
	A(16546) = Reduce(0, 0, 1);
	A(16547) = Reduce(0, 0, 1);
	A(16642) = Reduce(0, 0, 1);
	A(-416) = Reduce(38, 0, 1);
	A(16936) = Reduce(23, 0, 1);
	A(16972) = Shift(214);
	A(16939) = Reduce(23, 0, 1);
	A(16943) = Reduce(23, 0, 1);
	A(16944) = Reduce(23, 0, 1);
	A(16945) = Reduce(23, 0, 1);
	A(16947) = Reduce(23, 0, 1);
	A(17015) = Reduce(23, 0, 1);
	A(16955) = Shift(212);
	A(16956) = Shift(213);
	A(16961) = Shift(216);
	A(16963) = Shift(215);
	A(16964) = Shift(211);
	A(16966) = Reduce(23, 0, 1);
	A(17031) = Reduce(23, 0, 1);
	A(16968) = Reduce(23, 0, 1);
	A(16969) = Reduce(23, 0, 1);
	A(16970) = Reduce(23, 0, 1);
	A(16971) = Shift(217);
	A(16967) = Reduce(23, 0, 1);
	A(17038) = Reduce(23, 0, 1);
	A(17040) = Reduce(23, 0, 1);
	A(17032) = Reduce(23, 0, 1);
	A(-414) = Reduce(38, 0, 1);
	A(17443) = Shift(70);
	A(17445) = Shift(86);
	A(17447) = Shift(74);
	A(17354) = Shift(71);
	A(17323) = Shift(16);
	A(17324) = Shift(89);
	A(17325) = Shift(84);
	A(17326) = Shift(95);
	A(17333) = Shift(97);
	A(17360) = Shift(94);
	A(17544) = Shift(202);
	A(17612) = Reduce(19, 0, 1);
	A(17533) = Reduce(19, 0, 1);
	A(17541) = Reduce(19, 0, 1);
	A(17563) = Shift(208);
	A(17628) = Reduce(19, 0, 1);
	A(17629) = Reduce(19, 0, 1);
	A(17566) = Shift(203);
	A(17567) = Shift(205);
	A(17536) = Reduce(19, 0, 1);
	A(17564) = Shift(207);
	A(17635) = Reduce(19, 0, 1);
	A(17540) = Reduce(19, 0, 1);
	A(17637) = Reduce(19, 0, 1);
	A(17542) = Reduce(19, 0, 1);
	A(17565) = Shift(204);
	A(-411) = Reduce(38, 0, 1);
	A(-410) = Reduce(59, 9, 2);
	A(-409) = Reduce(35, 0, 1);
	A(-408) = Reduce(47, 24, 1);
	A(18632) = Shift(109);
	A(18536) = Shift(201);
	A(-406) = Reduce(8, 0, 1);
	A(-405) = Reduce(38, 0, 1);
	A(19125) = Reduce(28, 0, 1);
	A(19161) = Reduce(28, 0, 1);
	A(19128) = Reduce(28, 0, 1);
	A(19132) = Reduce(28, 0, 1);
	A(19133) = Reduce(28, 0, 1);
	A(19134) = Reduce(28, 0, 1);
	A(19136) = Reduce(28, 0, 1);
	A(19204) = Reduce(28, 0, 1);
	A(19144) = Reduce(28, 0, 1);
	A(19145) = Reduce(28, 0, 1);
	A(19146) = Reduce(28, 0, 1);
	A(19147) = Reduce(28, 0, 1);
	A(19148) = Reduce(28, 0, 1);
	A(19149) = Shift(199);
	A(19150) = Reduce(28, 0, 1);
	A(19152) = Reduce(28, 0, 1);
	A(19153) = Reduce(28, 0, 1);
	A(19154) = Reduce(28, 0, 1);
	A(19155) = Reduce(28, 0, 1);
	A(19156) = Reduce(28, 0, 1);
	A(19157) = Reduce(28, 0, 1);
	A(19158) = Reduce(28, 0, 1);
	A(19159) = Reduce(28, 0, 1);
	A(19160) = Reduce(28, 0, 1);
	A(19220) = Reduce(28, 0, 1);
	A(19227) = Reduce(28, 0, 1);
	A(19229) = Reduce(28, 0, 1);
	A(19221) = Reduce(28, 0, 1);
	A(19433) = Shift(70);
	A(19434) = Shift(69);
	A(19435) = Shift(86);
	A(19437) = Shift(74);
	A(19315) = Shift(84);
	A(19313) = Shift(16);
	A(19314) = Shift(89);
	A(19411) = Shift(78);
	A(19316) = Shift(95);
	A(19350) = Shift(94);
	A(19418) = Shift(101);
	A(19323) = Shift(97);
	A(19344) = Shift(71);
	A(19426) = Shift(79);
	A(-402) = Reduce(46, 0, 1);
	A(-401) = Reduce(2, 0, 1);
	A(-400) = Reduce(47, 26, 1);
	A(20229) = Shift(70);
	A(20110) = Shift(89);
	A(20119) = Shift(97);
	A(20233) = Shift(74);
	A(20231) = Shift(86);
	A(20140) = Shift(71);
	A(20109) = Shift(16);
	A(20222) = Shift(79);
	A(20111) = Shift(84);
	A(20112) = Shift(95);
	A(20146) = Shift(94);
	A(-398) = Reduce(35, 0, 1);
	A(-397) = Reduce(35, 0, 1);
	A(20720) = Shift(110);
	A(20724) = Shift(107);
	A(20821) = Shift(109);
	A(-395) = Reduce(79, 0, 1);
	A(21224) = Shift(70);
	A(21105) = Shift(89);
	A(21114) = Shift(97);
	A(21228) = Shift(74);
	A(21226) = Shift(86);
	A(21135) = Shift(71);
	A(21104) = Shift(16);
	A(21217) = Shift(79);
	A(21106) = Shift(84);
	A(21107) = Shift(95);
	A(21141) = Shift(94);
	A(21425) = Shift(86);
	A(21304) = Shift(89);
	A(21313) = Shift(97);
	A(21427) = Shift(74);
	A(21423) = Shift(70);
	A(21334) = Shift(71);
	A(21303) = Shift(16);
	A(21416) = Shift(79);
	A(21305) = Shift(84);
	A(21306) = Shift(95);
	A(21340) = Shift(94);
	A(-392) = Shift(186);
	A(-391) = Reduce(49, 0, 1);
	A(22020) = Shift(70);
	A(22021) = Shift(111);
	A(22022) = Shift(86);
	A(22023) = Shift(3);
	A(22024) = Shift(74);
	A(22025) = Shift(126);
	A(21995) = Shift(124);
	A(21900) = Shift(16);
	A(21901) = Shift(89);
	A(21998) = Shift(118);
	A(21903) = Shift(95);
	A(21902) = Shift(84);
	A(22003) = Shift(134);
	A(22005) = Shift(132);
	A(21910) = Shift(97);
	A(22008) = Shift(136);
	A(22010) = Shift(112);
	A(21915) = Reduce(61, 90, 0);
	A(22012) = Shift(129);
	A(22017) = Shift(127);
	A(22219) = Shift(70);
	A(22212) = Shift(79);
	A(22221) = Shift(86);
	A(22223) = Shift(74);
	A(22130) = Shift(71);
	A(22099) = Shift(16);
	A(22100) = Shift(89);
	A(22101) = Shift(84);
	A(22102) = Shift(95);
	A(22109) = Shift(97);
	A(22136) = Shift(94);
	A(-388) = Shift(171);
	A(-387) = Reduce(51, 0, 1);
	A(-386) = Reduce(51, 0, 1);
	A(22917) = Shift(158);
	A(22911) = Shift(160);
	A(22905) = Shift(159);
	A(22914) = Reduce(80, 97, 1);
	A(-384) = Shift(157);
	A(-383) = Reduce(51, 0, 1);
	A(-382) = Shift(110);
	A(-381) = Reduce(51, 0, 1);
	A(-380) = Reduce(51, 0, 1);
	A(-379) = Shift(155);
	A(-378) = Reduce(51, 0, 1);
	A(-377) = Reduce(51, 0, 1);
	A(-376) = Reduce(57, 0, 1);
	A(-375) = Reduce(51, 0, 1);
	A(25204) = Shift(70);
	A(25197) = Shift(79);
	A(25206) = Shift(86);
	A(25208) = Shift(74);
	A(25115) = Shift(71);
	A(25094) = Shift(97);
	A(25084) = Shift(16);
	A(25085) = Shift(89);
	A(25086) = Shift(84);
	A(25087) = Shift(95);
	A(25121) = Shift(94);
	A(25403) = Shift(70);
	A(25404) = Shift(69);
	A(25405) = Shift(86);
	A(25407) = Shift(74);
	A(25314) = Shift(71);
	A(25283) = Shift(16);
	A(25284) = Shift(89);
	A(25285) = Shift(84);
	A(25286) = Shift(95);
	A(25320) = Shift(94);
	A(25381) = Shift(78);
	A(25388) = Shift(101);
	A(25293) = Shift(97);
	A(25396) = Shift(79);
	A(25302) = Shift(151);
	A(-372) = Reduce(51, 0, 1);
	A(-371) = Reduce(57, 0, 1);
	A(-370) = Reduce(51, 0, 1);
	A(26199) = Shift(70);
	A(26200) = Shift(111);
	A(26201) = Shift(86);
	A(26202) = Shift(3);
	A(26203) = Shift(74);
	A(26204) = Shift(126);
	A(26174) = Shift(124);
	A(26079) = Shift(16);
	A(26080) = Shift(89);
	A(26177) = Shift(118);
	A(26082) = Shift(95);
	A(26081) = Shift(84);
	A(26182) = Shift(134);
	A(26184) = Shift(132);
	A(26089) = Shift(97);
	A(26187) = Shift(136);
	A(26189) = Shift(112);
	A(26094) = Reduce(61, 0, 1);
	A(26191) = Shift(129);
	A(26196) = Shift(127);
	A(26398) = Shift(70);
	A(26279) = Shift(89);
	A(26400) = Shift(86);
	A(26402) = Shift(74);
	A(26309) = Shift(71);
	A(26288) = Shift(97);
	A(26278) = Shift(16);
	A(26391) = Shift(79);
	A(26280) = Shift(84);
	A(26281) = Shift(95);
	A(26315) = Shift(94);
	A(-367) = Shift(143);
	A(-366) = Shift(139);
	A(-365) = Reduce(51, 0, 1);
	A(-364) = Shift(110);
	A(-363) = Reduce(63, 23, 1);
	A(-362) = Reduce(52, 14, 2);
	A(-361) = Shift(140);
	A(27990) = Shift(70);
	A(27983) = Shift(79);
	A(27880) = Shift(97);
	A(27994) = Shift(74);
	A(27901) = Shift(71);
	A(27992) = Shift(86);
	A(27870) = Shift(16);
	A(27871) = Shift(89);
	A(27872) = Shift(84);
	A(27873) = Shift(95);
	A(27907) = Shift(94);
	A(28083) = Shift(110);
	A(28184) = Shift(109);
	A(-358) = Reduce(24, 48, 5);
	A(-357) = Reduce(65, 54, 2);
	A(28680) = Shift(110);
	A(28781) = Shift(109);
	A(28985) = Reduce(96, 28, 0);
	A(28986) = Reduce(96, 28, 0);
	A(28987) = Reduce(96, 28, 0);
	A(28988) = Reduce(96, 28, 0);
	A(28989) = Reduce(96, 28, 0);
	A(28990) = Reduce(96, 28, 0);
	A(28960) = Reduce(96, 28, 0);
	A(28865) = Reduce(96, 28, 0);
	A(28866) = Reduce(96, 28, 0);
	A(28963) = Reduce(96, 28, 0);
	A(28868) = Reduce(96, 28, 0);
	A(28867) = Reduce(96, 28, 0);
	A(28964) = Shift(147);
	A(28968) = Reduce(96, 28, 0);
	A(28970) = Reduce(96, 28, 0);
	A(28875) = Reduce(96, 28, 0);
	A(28973) = Reduce(96, 28, 0);
	A(28975) = Reduce(96, 28, 0);
	A(28880) = Reduce(96, 28, 0);
	A(28977) = Reduce(96, 28, 0);
	A(28982) = Reduce(96, 28, 0);
	A(-354) = Reduce(12, 33, 4);
	A(29277) = Shift(110);
	A(29368) = Shift(132);
	A(-352) = Reduce(96, 9, 2);
	A(-351) = Reduce(96, 9, 2);
	A(-350) = Reduce(63, 10, 2);
	A(-349) = Reduce(50, 53, 2);
	A(-348) = Reduce(50, 44, 2);
	A(30471) = Shift(110);
	A(30572) = Shift(109);
	A(-346) = Reduce(67, 51, 3);
	A(-345) = Reduce(33, 3, 3);
	A(-344) = Reduce(15, 17, 2);
	A(-343) = Reduce(51, 0, 2);
	A(31572) = Shift(70);
	A(31573) = Shift(69);
	A(31574) = Shift(86);
	A(31576) = Shift(74);
	A(31483) = Shift(71);
	A(31452) = Shift(16);
	A(31453) = Shift(89);
	A(31550) = Shift(78);
	A(31455) = Shift(95);
	A(31454) = Shift(84);
	A(31489) = Shift(94);
	A(31557) = Shift(101);
	A(31462) = Shift(97);
	A(31565) = Shift(79);
	A(31771) = Shift(70);
	A(31772) = Shift(69);
	A(31773) = Shift(86);
	A(31775) = Shift(74);
	A(31653) = Shift(84);
	A(31651) = Shift(16);
	A(31652) = Shift(89);
	A(31749) = Shift(78);
	A(31654) = Shift(95);
	A(31688) = Shift(94);
	A(31756) = Shift(101);
	A(31661) = Shift(97);
	A(31662) = Reduce(81, 93, 0);
	A(31682) = Shift(71);
	A(31764) = Shift(79);
	A(-340) = Shift(16);
	A(-339) = Reduce(11, 1, 3);
	A(-338) = Reduce(91, 63, 1);
	A(-337) = Shift(169);
	A(-336) = Reduce(18, 0, 1);
	A(32856) = Reduce(81, 0, 1);
	A(32865) = Shift(167);
	A(33159) = Shift(109);
	A(33064) = Reduce(18, 0, 1);
	A(33055) = Reduce(18, 0, 1);
	A(33363) = Shift(70);
	A(33364) = Shift(69);
	A(33365) = Shift(86);
	A(33367) = Shift(74);
	A(33274) = Shift(71);
	A(33243) = Shift(16);
	A(33244) = Shift(89);
	A(33245) = Shift(84);
	A(33246) = Shift(95);
	A(33280) = Shift(94);
	A(33341) = Shift(78);
	A(33348) = Shift(101);
	A(33253) = Shift(97);
	A(33356) = Shift(79);
	A(-332) = Reduce(91, 65, 3);
	A(-331) = Reduce(16, 2, 4);
	A(-330) = Reduce(70, 6, 3);
	A(-329) = Shift(172);
	A(34358) = Shift(70);
	A(34359) = Shift(69);
	A(34360) = Shift(86);
	A(34362) = Shift(74);
	A(34269) = Shift(71);
	A(34238) = Shift(16);
	A(34239) = Shift(89);
	A(34240) = Shift(84);
	A(34241) = Shift(95);
	A(34275) = Shift(94);
	A(34336) = Shift(78);
	A(34343) = Shift(101);
	A(34248) = Shift(97);
	A(34351) = Shift(79);
	A(-327) = Reduce(7, 46, 4);
	A(34650) = Shift(175);
	A(34751) = Shift(109);
	A(34850) = Reduce(105, 91, 0);
	A(34940) = Shift(178);
	A(34934) = Shift(179);
	A(-324) = Shift(185);
	A(35248) = Reduce(105, 0, 1);
	A(35338) = Shift(178);
	A(35332) = Shift(179);
	A(35552) = Shift(70);
	A(35433) = Shift(89);
	A(35554) = Shift(86);
	A(35556) = Shift(74);
	A(35463) = Shift(71);
	A(35432) = Shift(16);
	A(35545) = Shift(79);
	A(35434) = Shift(84);
	A(35435) = Shift(95);
	A(35442) = Shift(97);
	A(35469) = Shift(94);
	A(-321) = Shift(110);
	A(-320) = Reduce(93, 72, 1);
	A(-319) = Reduce(85, 25, 2);
	A(36242) = Shift(110);
	A(36343) = Shift(109);
	A(-317) = Reduce(85, 73, 3);
	A(-316) = Reduce(93, 8, 2);
	A(-315) = Reduce(78, 13, 5);
	A(37038) = Shift(110);
	A(37129) = Shift(101);
	A(37042) = Shift(107);
	A(-313) = Reduce(32, 0, 1);
	A(-312) = Reduce(32, 0, 1);
	A(-311) = Reduce(45, 4, 5);
	A(37935) = Shift(109);
	A(37839) = Shift(191);
	A(-309) = Reduce(79, 27, 3);
	A(38308) = Shift(194);
	A(38229) = Reduce(0, 62, 3);
	A(38232) = Reduce(0, 62, 3);
	A(38236) = Reduce(0, 62, 3);
	A(38237) = Reduce(0, 62, 3);
	A(38238) = Reduce(0, 62, 3);
	A(38333) = Reduce(0, 62, 3);
	A(38537) = Shift(70);
	A(38418) = Shift(89);
	A(38427) = Shift(97);
	A(38541) = Shift(74);
	A(38539) = Shift(86);
	A(38448) = Shift(71);
	A(38417) = Shift(16);
	A(38530) = Shift(79);
	A(38419) = Shift(84);
	A(38420) = Shift(95);
	A(38454) = Shift(94);
	A(-306) = Reduce(40, 0, 1);
	A(-305) = Reduce(29, 7, 3);
	A(-304) = Shift(197);
	A(-303) = Reduce(35, 9, 3);
	A(39532) = Shift(70);
	A(39422) = Shift(97);
	A(39536) = Shift(74);
	A(39534) = Shift(86);
	A(39412) = Shift(16);
	A(39413) = Shift(89);
	A(39414) = Shift(84);
	A(39415) = Shift(95);
	A(-301) = Reduce(68, 0, 1);
	A(-300) = Reduce(28, 37, 3);
	A(-299) = Reduce(46, 0, 2);
	A(-298) = Reduce(87, 0, 1);
	A(-297) = Reduce(87, 0, 1);
	A(-296) = Reduce(87, 0, 1);
	A(-295) = Reduce(87, 0, 1);
	A(41124) = Shift(70);
	A(41126) = Shift(86);
	A(41128) = Shift(74);
	A(41035) = Shift(71);
	A(41014) = Shift(97);
	A(41004) = Shift(16);
	A(41005) = Shift(89);
	A(41006) = Shift(84);
	A(41007) = Shift(95);
	A(41041) = Shift(94);
	A(-293) = Reduce(87, 0, 1);
	A(-292) = Reduce(87, 0, 1);
	A(41612) = Reduce(23, 40, 3);
	A(41615) = Reduce(23, 40, 3);
	A(41619) = Reduce(23, 40, 3);
	A(41620) = Reduce(23, 40, 3);
	A(41621) = Reduce(23, 40, 3);
	A(41623) = Reduce(23, 40, 3);
	A(41707) = Reduce(23, 40, 3);
	A(41691) = Reduce(23, 40, 3);
	A(41631) = Shift(212);
	A(41632) = Shift(213);
	A(41637) = Shift(216);
	A(41639) = Shift(215);
	A(41640) = Shift(211);
	A(41642) = Reduce(23, 40, 3);
	A(41643) = Reduce(23, 40, 3);
	A(41644) = Reduce(23, 40, 3);
	A(41645) = Reduce(23, 40, 3);
	A(41646) = Reduce(23, 40, 3);
	A(41647) = Shift(217);
	A(41648) = Shift(214);
	A(41714) = Reduce(23, 40, 3);
	A(41716) = Reduce(23, 40, 3);
	A(41708) = Reduce(23, 40, 3);
	A(41920) = Shift(70);
	A(41810) = Shift(97);
	A(41924) = Shift(74);
	A(41922) = Shift(86);
	A(41831) = Shift(71);
	A(41800) = Shift(16);
	A(41801) = Shift(89);
	A(41802) = Shift(84);
	A(41803) = Shift(95);
	A(41837) = Shift(94);
	A(-289) = Reduce(75, 0, 1);
	A(-288) = Reduce(75, 0, 1);
	A(-287) = Reduce(75, 0, 1);
	A(-286) = Reduce(75, 0, 1);
	A(-285) = Reduce(75, 0, 1);
	A(-284) = Reduce(75, 0, 1);
	A(-283) = Reduce(75, 0, 1);
	A(43403) = Reduce(4, 35, 3);
	A(43439) = Reduce(4, 35, 3);
	A(43406) = Reduce(4, 35, 3);
	A(43410) = Reduce(4, 35, 3);
	A(43411) = Reduce(4, 35, 3);
	A(43412) = Reduce(4, 35, 3);
	A(43414) = Reduce(4, 35, 3);
	A(43482) = Reduce(4, 35, 3);
	A(43422) = Reduce(4, 35, 3);
	A(43423) = Reduce(4, 35, 3);
	A(43424) = Shift(220);
	A(43425) = Shift(219);
	A(43426) = Shift(223);
	A(43428) = Reduce(4, 35, 3);
	A(43430) = Reduce(4, 35, 3);
	A(43431) = Reduce(4, 35, 3);
	A(43432) = Shift(222);
	A(43433) = Reduce(4, 35, 3);
	A(43434) = Reduce(4, 35, 3);
	A(43499) = Reduce(4, 35, 3);
	A(43436) = Reduce(4, 35, 3);
	A(43437) = Reduce(4, 35, 3);
	A(43438) = Reduce(4, 35, 3);
	A(43498) = Reduce(4, 35, 3);
	A(43505) = Reduce(4, 35, 3);
	A(43507) = Reduce(4, 35, 3);
	A(43435) = Reduce(4, 35, 3);
	A(-281) = Reduce(69, 0, 1);
	A(-280) = Reduce(69, 0, 1);
	A(44109) = Shift(70);
	A(44111) = Shift(86);
	A(44113) = Shift(74);
	A(44020) = Shift(71);
	A(43989) = Shift(16);
	A(43990) = Shift(89);
	A(43991) = Shift(84);
	A(43992) = Shift(95);
	A(43999) = Shift(97);
	A(44026) = Shift(94);
	A(-278) = Reduce(69, 0, 1);
	A(-277) = Reduce(69, 0, 1);
	A(-276) = Reduce(2, 32, 3);
	A(-275) = Reduce(26, 52, 2);
	A(-274) = Reduce(39, 15, 2);
	A(-273) = Reduce(47, 57, 2);
	A(-272) = Shift(235);
	A(-271) = Reduce(101, 0, 1);
	A(45900) = Shift(70);
	A(45790) = Shift(97);
	A(45904) = Shift(74);
	A(45902) = Shift(86);
	A(45811) = Shift(71);
	A(45780) = Shift(16);
	A(45781) = Shift(89);
	A(45782) = Shift(84);
	A(45783) = Shift(95);
	A(45817) = Shift(94);
	A(-269) = Reduce(89, 0, 1);
	A(46298) = Reduce(101, 28, 0);
	A(46300) = Reduce(101, 28, 0);
	A(46302) = Reduce(101, 28, 0);
	A(46209) = Reduce(101, 28, 0);
	A(46178) = Reduce(101, 28, 0);
	A(46179) = Reduce(101, 28, 0);
	A(46180) = Reduce(101, 28, 0);
	A(46181) = Reduce(101, 28, 0);
	A(46215) = Reduce(101, 28, 0);
	A(46284) = Reduce(101, 28, 0);
	A(46188) = Reduce(101, 28, 0);
	A(46291) = Shift(229);
	A(-267) = Reduce(72, 36, 2);
	A(46598) = Shift(202);
	A(46666) = Reduce(19, 45, 3);
	A(46594) = Reduce(19, 45, 3);
	A(46691) = Reduce(19, 45, 3);
	A(46617) = Shift(208);
	A(46682) = Reduce(19, 45, 3);
	A(46619) = Shift(204);
	A(46620) = Shift(203);
	A(46621) = Shift(205);
	A(46590) = Reduce(19, 45, 3);
	A(46618) = Shift(207);
	A(46689) = Reduce(19, 45, 3);
	A(46587) = Reduce(19, 45, 3);
	A(46595) = Reduce(19, 45, 3);
	A(46596) = Reduce(19, 45, 3);
	A(46683) = Reduce(19, 45, 3);
	A(-265) = Reduce(72, 59, 2);
	A(46988) = Shift(237);
	A(47089) = Shift(109);
	A(47188) = Reduce(94, 61, 0);
	A(47278) = Shift(239);
	A(47272) = Shift(241);
	A(-262) = Reduce(25, 76, 1);
	A(47691) = Shift(70);
	A(47572) = Shift(89);
	A(47693) = Shift(86);
	A(47695) = Shift(74);
	A(47602) = Shift(71);
	A(47571) = Shift(16);
	A(47684) = Shift(79);
	A(47573) = Shift(84);
	A(47574) = Shift(95);
	A(47581) = Shift(97);
	A(47608) = Shift(94);
	A(47785) = Reduce(94, 0, 1);
	A(47875) = Shift(239);
	A(47869) = Shift(241);
	A(47983) = Shift(110);
	A(47987) = Shift(107);
	A(-258) = Shift(243);
	A(-257) = Reduce(44, 5, 5);
	A(-256) = Reduce(73, 87, 2);
	A(-255) = Reduce(25, 50, 2);
	A(48978) = Shift(110);
	A(48982) = Shift(107);
	A(49079) = Shift(109);
	A(-253) = Reduce(73, 71, 3);
	A(49377) = Shift(255);
	A(49466) = Shift(252);
	A(49481) = Shift(251);
	A(-251) = Reduce(27, 84, 1);
	A(49774) = Shift(110);
	A(49783) = Shift(63);
	A(49888) = Shift(66);
	A(49779) = Shift(65);
	A(-249) = Reduce(30, 0, 1);
	A(-248) = Reduce(30, 0, 1);
	A(-247) = Reduce(71, 20, 2);
	A(-246) = Reduce(27, 69, 2);
	A(-245) = Reduce(17, 9, 3);
	A(-244) = Reduce(42, 12, 2);
	A(-243) = Reduce(95, 64, 1);
	A(-242) = Shift(267);
	A(51562) = Reduce(36, 0, 1);
	A(51571) = Shift(265);
	A(-240) = Shift(261);
	A(51971) = Shift(263);
	A(51960) = Reduce(88, 28, 0);
	A(51969) = Reduce(88, 28, 0);
	A(-238) = Reduce(76, 83, 3);
	A(52467) = Shift(70);
	A(52468) = Shift(69);
	A(52469) = Shift(86);
	A(52471) = Shift(74);
	A(52349) = Shift(84);
	A(52347) = Shift(16);
	A(52348) = Shift(89);
	A(52445) = Shift(78);
	A(52350) = Shift(95);
	A(52384) = Shift(94);
	A(52452) = Shift(101);
	A(52357) = Shift(97);
	A(52378) = Shift(71);
	A(52460) = Shift(79);
	A(-236) = Reduce(88, 9, 2);
	A(52745) = Shift(16);
	A(52836) = Shift(9);
	A(-234) = Reduce(95, 82, 3);
	A(-233) = Reduce(9, 9, 3);
	A(53364) = Shift(277);
	A(53432) = Shift(290);
	A(53372) = Shift(272);
	A(53373) = Shift(270);
	A(53374) = Shift(287);
	A(53375) = Shift(286);
	A(53377) = Shift(274);
	A(53378) = Shift(282);
	A(53379) = Shift(285);
	A(53380) = Shift(280);
	A(53381) = Shift(284);
	A(53382) = Shift(276);
	A(53383) = Shift(283);
	A(53384) = Shift(281);
	A(53385) = Shift(288);
	A(53386) = Shift(278);
	A(53387) = Shift(279);
	A(53388) = Shift(273);
	A(53389) = Shift(271);
	A(53455) = Shift(275);
	A(53457) = Shift(289);
	A(-231) = Reduce(83, 55, 3);
	A(-230) = Reduce(97, 0, 1);
	A(-229) = Reduce(97, 0, 1);
	A(-228) = Reduce(97, 0, 1);
	A(-227) = Reduce(97, 0, 1);
	A(-226) = Reduce(97, 0, 1);
	A(-225) = Reduce(97, 0, 1);
	A(-224) = Reduce(97, 0, 1);
	A(-223) = Reduce(97, 0, 1);
	A(-222) = Reduce(97, 0, 1);
	A(-221) = Reduce(97, 0, 1);
	A(-220) = Reduce(97, 0, 1);
	A(-219) = Reduce(97, 0, 1);
	A(-218) = Reduce(97, 0, 1);
	A(-217) = Reduce(97, 0, 1);
	A(-216) = Reduce(97, 0, 1);
	A(-215) = Reduce(97, 0, 1);
	A(-214) = Reduce(97, 0, 1);
	A(-213) = Reduce(97, 0, 1);
	A(-212) = Reduce(97, 0, 1);
	A(-211) = Reduce(97, 0, 1);
	A(-210) = Reduce(97, 0, 1);
	A(-209) = Reduce(43, 47, 2);
	A(58132) = Reduce(74, 28, 0);
	A(58128) = Shift(293);
	A(58136) = Reduce(74, 28, 0);
	A(58130) = Reduce(74, 28, 0);
	A(-207) = Shift(309);
	A(58538) = Reduce(58, 28, 0);
	A(58539) = Reduce(58, 28, 0);
	A(58526) = Reduce(58, 28, 0);
	A(58528) = Shift(28);
	A(58530) = Reduce(58, 28, 0);
	A(58644) = Reduce(58, 28, 0);
	A(58534) = Reduce(58, 28, 0);
	A(58535) = Reduce(58, 28, 0);
	A(58729) = Reduce(102, 28, 0);
	A(58733) = Shift(297);
	A(-204) = Shift(303);
	A(59127) = Reduce(86, 88, 0);
	A(59113) = Shift(16);
	A(59204) = Shift(9);
	A(-202) = Reduce(102, 9, 2);
	A(-201) = Reduce(92, 74, 1);
	A(59724) = Reduce(86, 0, 1);
	A(59730) = Shift(301);
	A(59909) = Shift(16);
	A(60000) = Shift(9);
	A(-198) = Reduce(92, 80, 3);
	A(60425) = Shift(14);
	A(60430) = Shift(3);
	A(60398) = Shift(9);
	A(60418) = Shift(22);
	A(60322) = Reduce(62, 56, 0);
	A(60307) = Shift(16);
	A(60404) = Shift(18);
	A(60421) = Shift(12);
	A(60407) = Shift(25);
	A(60408) = Shift(13);
	A(-196) = Reduce(106, 11, 1);
	A(60823) = Shift(14);
	A(60802) = Shift(18);
	A(60796) = Shift(9);
	A(60816) = Shift(22);
	A(60720) = Reduce(62, 0, 1);
	A(60705) = Shift(16);
	A(60806) = Shift(13);
	A(60819) = Shift(12);
	A(60805) = Shift(25);
	A(60828) = Shift(3);
	A(-194) = Shift(307);
	A(-193) = Reduce(22, 16, 8);
	A(-192) = Reduce(106, 34, 2);
	A(-191) = Shift(310);
	A(-190) = Reduce(74, 9, 3);
	A(61921) = Reduce(9, 28, 0);
	A(61922) = Reduce(9, 28, 0);
	A(61909) = Shift(56);
	A(61913) = Reduce(9, 28, 0);
	A(62027) = Reduce(9, 28, 0);
	A(61918) = Reduce(9, 28, 0);
	A(62120) = Shift(59);
	A(62112) = Shift(60);
	A(62226) = Shift(66);
	A(62117) = Shift(65);
	A(62121) = Shift(63);
	A(-187) = Reduce(48, 42, 4);
	A(62496) = Shift(16);
	A(62587) = Shift(9);
	A(62509) = Reduce(66, 94, 0);
	A(-185) = Reduce(1, 22, 2);
	A(-184) = Reduce(82, 75, 1);
	A(63113) = Shift(320);
	A(63106) = Reduce(66, 0, 1);
	A(-182) = Shift(319);
	A(-181) = Reduce(90, 9, 3);
	A(63690) = Shift(16);
	A(63781) = Shift(9);
	A(-179) = Reduce(82, 79, 3);
	A(-178) = Shift(16);
	A(-177) = Reduce(3, 18, 3);
	A(64500) = Shift(326);
	A(64502) = Shift(322);
	A(64505) = Shift(325);
	A(-175) = Reduce(37, 67, 3);
	A(64874) = Reduce(64, 70, 0);
	A(65002) = Shift(14);
	A(65007) = Shift(3);
	A(64884) = Shift(16);
	A(64981) = Shift(18);
	A(64984) = Shift(25);
	A(64985) = Shift(13);
	A(64899) = Reduce(64, 70, 0);
	A(64993) = Shift(10);
	A(64995) = Shift(22);
	A(64998) = Shift(12);
	A(64975) = Shift(9);
	A(-173) = Shift(328);
	A(-172) = Reduce(37, 41, 5);
	A(-171) = Reduce(14, 66, 2);
	A(-170) = Reduce(103, 0, 1);
	A(66094) = Shift(322);
	A(66096) = Shift(334);
	A(66097) = Shift(333);
	A(-167) = Reduce(5, 30, 3);
	A(-166) = Shift(16);
	A(-165) = Reduce(84, 92, 1);
	A(66890) = Shift(322);
	A(66966) = Shift(341);
	A(66893) = Reduce(53, 49, 1);
	A(66894) = Reduce(53, 49, 1);
	A(67092) = Shift(338);
	A(67093) = Shift(339);
	A(-162) = Reduce(5, 31, 5);
	A(-161) = Shift(16);
	A(-160) = Reduce(84, 89, 3);
	A(-159) = Shift(342);
	A(-158) = Reduce(53, 78, 3);
	A(-157) = Reduce(55, 38, 2);
}
Syntax reduce(int rid, List[any] S, Location start, Location end) {
	if rid < 49 {
		if rid < 24 {
			if rid < 12 {
				if rid < 6 {
					if rid < 3 {
						if rid < 1 {
							ret S(0);
						}
						else {
							if rid < 2 {
								ret ExprDot(start, end, S(0), S(2));
							}
							else {
								ret ExprCall(start, end, S(0), S(2));
							}
						}
					}
					else {
						if rid < 4 {
							ret StmtBlock(start, end, S(1));
						}
						else {
							if rid < 5 {
								ret StmtIf(start, end, S(1), S(2), S(4));
							}
							else {
								ret StmtSwitch(start, end, S(1), S(3));
							}
						}
					}
				}
				else {
					if rid < 9 {
						if rid < 7 {
							ret StmtAssign(start, end, S(0), S(2));
						}
						else {
							if rid < 8 {
								ret ExprBinary(start, end, S(1), S(0), S(2));
							}
							else {
								ret cast[ListSyntax[SwitchCase]](S(0)).push(end, S(1));
							}
						}
					}
					else {
						if rid < 10 {
							ret S(1);
						}
						else {
							if rid < 11 {
								ret cast[ListSyntax[Stmt]](S(0)).push(end, S(1));
							}
							else {
								ret ListSyntax[NonModule](start, end, S(0));
							}
						}
					}
				}
			}
			else {
				if rid < 18 {
					if rid < 15 {
						if rid < 13 {
							ret LeafInitializer(start, end, S(1));
						}
						else {
							if rid < 14 {
								ret StmtSwitch(start, end, S(1), S(3));
							}
							else {
								ret StmtLoop(start, end, S(1));
							}
						}
					}
					else {
						if rid < 16 {
							ret ExprUnary(start, end, S(0), S(1));
						}
						else {
							if rid < 17 {
								ret ClassDef(start, end, S(0), S(1), S(2), S(3), S(4), S(6));
							}
							else {
								ret StmtDo(start, end, S(1));
							}
						}
					}
				}
				else {
					if rid < 21 {
						if rid < 19 {
							ret Name(start, end, S(0), S(2));
						}
						else {
							if rid < 20 {
								ret LeafNameCtor(start, end, #null);
							}
							else {
								ret FuncEtter(start, end, S(0), S(1));
							}
						}
					}
					else {
						if rid < 22 {
							ret ListSyntax[EnumMember](start, end, S(0));
						}
						else {
							if rid < 23 {
								ret ExprName(start, end, S(0), S(1));
							}
							else {
								ret ListSyntax[Stmt](start, end, S(0));
							}
						}
					}
				}
			}
		}
		else {
			if rid < 36 {
				if rid < 30 {
					if rid < 27 {
						if rid < 25 {
							ret ExprStmt(start, end, S(0));
						}
						else {
							if rid < 26 {
								ret SwitchCase(start, end, #null, S(1));
							}
							else {
								ret ExprStmt(start, end, S(0));
							}
						}
					}
					else {
						if rid < 28 {
							ret StmtRet(start, end, S(1));
						}
						else {
							if rid < 29 {
								ret cast[Syntax](#null);
							}
							else {
								ret cast[ListSyntax[EnumMember]](S(0)).push(end, S(1));
							}
						}
					}
				}
				else {
					if rid < 33 {
						if rid < 31 {
							ret StmtUse(start, end, S(1), #null);
						}
						else {
							if rid < 32 {
								ret StmtUse(start, end, S(1), S(3));
							}
							else {
								ret ExprBinary(start, end, S(1), S(0), S(2));
							}
						}
					}
					else {
						if rid < 34 {
							ret StmtIf(start, end, S(1), S(2), S(3));
						}
						else {
							if rid < 35 {
								ret cast[ListSyntax[NonModule]](S(0)).push(end, S(1));
							}
							else {
								ret ExprBinary(start, end, S(1), S(0), S(2));
							}
						}
					}
				}
			}
			else {
				if rid < 42 {
					if rid < 39 {
						if rid < 37 {
							ret OperNot(start, end, S(0), S(1));
						}
						else {
							if rid < 38 {
								ret ExprBinary(start, end, S(1), S(0), S(2));
							}
							else {
								ret cast[ListSyntax[ModuleMember]](S(0)).push(end, S(1));
							}
						}
					}
					else {
						if rid < 40 {
							ret ExprAtomic(start, end, S(0));
						}
						else {
							if rid < 41 {
								ret ExprBinary(start, end, S(1), S(0), S(2));
							}
							else {
								ret Module(start, end, S(1), S(3));
							}
						}
					}
				}
				else {
					if rid < 45 {
						if rid < 43 {
							ret Leaf(start, end, S(0), S(1), #null, S(2), S(3));
						}
						else {
							if rid < 44 {
								ret ListSyntax[ModuleMember](start, end, S(0));
							}
							else {
								ret StmtRet(start, end, S(1));
							}
						}
					}
					else {
						if rid < 47 {
							if rid < 46 {
								ret ExprBinary(start, end, S(1), S(0), S(2));
							}
							else {
								ret StmtVar(start, end, S(1), S(3));
							}
						}
						else {
							if rid < 48 {
								ret LeafNameCtor(start, end, S(1));
							}
							else {
								ret StmtFor(start, end, S(1), S(3), S(4));
							}
						}
					}
				}
			}
		}
	}
	else {
		if rid < 73 {
			if rid < 61 {
				if rid < 55 {
					if rid < 52 {
						if rid < 50 {
							ret UseAlias(start, end, S(0), #null);
						}
						else {
							if rid < 51 {
								ret cast[ListSyntax[SwitchCase]](S(0)).push(end, S(1));
							}
							else {
								ret StmtWhile(start, end, S(1), S(2));
							}
						}
					}
					else {
						if rid < 53 {
							ret ExprUnary(start, end, S(0), S(1));
						}
						else {
							if rid < 54 {
								ret StmtRet(start, end, #null);
							}
							else {
								ret StmtFlow(start, end, S(0));
							}
						}
					}
				}
				else {
					if rid < 58 {
						if rid < 56 {
							ret LeafNameOp(start, end, S(2), S(0));
						}
						else {
							if rid < 57 {
								ret ListSyntax[NonModule](start, end);
							}
							else {
								ret ExprStmt(start, end, S(1));
							}
						}
					}
					else {
						if rid < 59 {
							ret cast[ListSyntax[GenericParam]](S(0)).push(end, S(2));
						}
						else {
							if rid < 60 {
								ret OperNot(start, end, S(1), S(0));
							}
							else {
								ret EnumDef(start, end, S(1), S(2), S(4));
							}
						}
					}
				}
			}
			else {
				if rid < 67 {
					if rid < 64 {
						if rid < 62 {
							ret ListSyntax[SwitchCase](start, end);
						}
						else {
							if rid < 63 {
								ret ExprBinary(start, end, S(1), S(0), S(2));
							}
							else {
								ret ListSyntax[Expr](start, end, S(0));
							}
						}
					}
					else {
						if rid < 65 {
							ret ListSyntax[FuncParam](start, end, S(0));
						}
						else {
							if rid < 66 {
								ret cast[ListSyntax[Expr]](S(0)).push(end, S(2));
							}
							else {
								ret TypeRef(start, end, S(0), S(1));
							}
						}
					}
				}
				else {
					if rid < 70 {
						if rid < 68 {
							ret Module(start, end, S(1), #null);
						}
						else {
							if rid < 69 {
								ret Leaf(start, end, S(1), S(2), S(0), S(3), S(4));
							}
							else {
								ret cast[ListSyntax[FuncEtter]](S(0)).push(end, S(1));
							}
						}
					}
					else {
						if rid < 71 {
							ret ListSyntax[ModuleMember](start, end);
						}
						else {
							if rid < 72 {
								ret SwitchCase(start, end, S(1), S(2));
							}
							else {
								ret ListSyntax[SwitchCase](start, end, S(0));
							}
						}
					}
				}
			}
		}
		else {
			if rid < 85 {
				if rid < 79 {
					if rid < 76 {
						if rid < 74 {
							ret SwitchCase(start, end, S(1), S(2));
						}
						else {
							if rid < 75 {
								ret ListSyntax[TypeRef](start, end, S(0));
							}
							else {
								ret ListSyntax[TypeRef](start, end, S(0));
							}
						}
					}
					else {
						if rid < 77 {
							ret ListSyntax[SwitchCase](start, end, S(0));
						}
						else {
							if rid < 78 {
								ret Name(start, end, #null, S(0));
							}
							else {
								ret UseAlias(start, end, S(0), S(2));
							}
						}
					}
				}
				else {
					if rid < 82 {
						if rid < 80 {
							ret cast[ListSyntax[TypeRef]](S(0)).push(end, S(2));
						}
						else {
							if rid < 81 {
								ret cast[ListSyntax[TypeRef]](S(0)).push(end, S(2));
							}
							else {
								ret GenericParam(start, end, S(1), S(0));
							}
						}
					}
					else {
						if rid < 83 {
							ret cast[ListSyntax[FuncParam]](S(0)).push(end, S(2));
						}
						else {
							if rid < 84 {
								ret FuncParam(start, end, S(1), S(0), S(2));
							}
							else {
								ret ListSyntax[FuncEtter](start, end, S(0));
							}
						}
					}
				}
			}
			else {
				if rid < 91 {
					if rid < 88 {
						if rid < 86 {
							ret ListSyntax[GenericParam](start, end, S(0));
						}
						else {
							if rid < 87 {
								ret ListSyntax[GenericParam](start, end);
							}
							else {
								ret SwitchCase(start, end, #null, S(1));
							}
						}
					}
					else {
						if rid < 89 {
							ret ListSyntax[TypeRef](start, end);
						}
						else {
							if rid < 90 {
								ret cast[ListSyntax[UseAlias]](S(0)).push(end, S(2));
							}
							else {
								ret ListSyntax[Stmt](start, end);
							}
						}
					}
				}
				else {
					if rid < 94 {
						if rid < 92 {
							ret ListSyntax[SwitchCase](start, end);
						}
						else {
							if rid < 93 {
								ret ListSyntax[UseAlias](start, end, S(0));
							}
							else {
								ret ListSyntax[Expr](start, end);
							}
						}
					}
					else {
						if rid < 96 {
							if rid < 95 {
								ret ListSyntax[TypeRef](start, end);
							}
							else {
								ret ListSyntax[FuncParam](start, end);
							}
						}
						else {
							if rid < 97 {
								ret ListSyntax[EnumMember](start, end);
							}
							else {
								ret StmtExpr(start, end, S(0));
							}
						}
					}
				}
			}
		}
	}
}
void init_goto() {
	my G = GOTO;
	G(64) = 6;
	G(1) = 4;
	G(34) = 19;
	G(3) = 11;
	G(5) = 17;
	G(6) = 5;
	G(41) = 21;
	G(10) = 8;
	G(43) = 20;
	G(14) = 2;
	G(48) = 15;
	G(22) = 23;
	G(55) = 1;
	G(31) = 24;
	G(37) = 7;
	G(200) = 4;
	G(233) = 19;
	G(202) = 11;
	G(236) = 7;
	G(205) = 343;
	G(240) = 21;
	G(209) = 8;
	G(242) = 20;
	G(213) = 2;
	G(247) = 15;
	G(221) = 23;
	G(230) = 24;
	G(204) = 17;
	G(481) = 51;
	G(441) = 50;
	G(475) = 54;
	G(598) = 4;
	G(600) = 332;
	G(52027) = 262;
	G(52337) = 166;
	G(52338) = 76;
	G(52339) = 77;
	G(52341) = 85;
	G(52345) = 87;
	G(52348) = 103;
	G(52350) = 82;
	G(52353) = 102;
	G(52355) = 264;
	G(52356) = 72;
	G(52357) = 91;
	G(52358) = 96;
	G(52360) = 88;
	G(52363) = 99;
	G(52365) = 81;
	G(52366) = 83;
	G(52372) = 73;
	G(52375) = 80;
	G(52376) = 75;
	G(52381) = 92;
	G(52382) = 100;
	G(52384) = 164;
	G(1695) = 329;
	G(52736) = 4;
	G(52745) = 8;
	G(52738) = 11;
	G(52811) = 266;
	G(52749) = 260;
	G(1991) = 4;
	G(1993) = 324;
	G(53429) = 269;
	G(8956) = 4;
	G(8989) = 44;
	G(8958) = 11;
	G(9055) = 46;
	G(8960) = 17;
	G(8996) = 21;
	G(8965) = 8;
	G(8998) = 20;
	G(8969) = 40;
	G(9003) = 15;
	G(8977) = 23;
	G(8986) = 24;
	G(3274) = 315;
	G(4038) = 311;
	G(5232) = 27;
	G(5628) = 31;
	G(5676) = 33;
	G(5670) = 34;
	G(5626) = 32;
	G(7021) = 31;
	G(7019) = 36;
	G(58182) = 294;
	G(58564) = 295;
	G(7762) = 4;
	G(7795) = 44;
	G(7764) = 11;
	G(7861) = 41;
	G(7766) = 17;
	G(7802) = 21;
	G(7771) = 8;
	G(7804) = 20;
	G(7775) = 40;
	G(7809) = 15;
	G(7860) = 45;
	G(7783) = 23;
	G(7821) = 42;
	G(7792) = 24;
	G(8043) = 51;
	G(8003) = 50;
	G(8037) = 54;
	G(59104) = 4;
	G(59106) = 11;
	G(59189) = 298;
	G(59113) = 8;
	G(59195) = 300;
	G(59117) = 299;
	G(59900) = 4;
	G(59909) = 8;
	G(59902) = 11;
	G(59913) = 302;
	G(60298) = 4;
	G(60331) = 304;
	G(60300) = 11;
	G(60302) = 17;
	G(60338) = 21;
	G(60307) = 8;
	G(60340) = 20;
	G(60311) = 2;
	G(60345) = 15;
	G(60319) = 23;
	G(60403) = 305;
	G(60359) = 306;
	G(60328) = 24;
	G(60743) = 15;
	G(60696) = 4;
	G(60729) = 308;
	G(60698) = 11;
	G(60700) = 17;
	G(60717) = 23;
	G(60736) = 21;
	G(60705) = 8;
	G(60738) = 20;
	G(60709) = 2;
	G(60726) = 24;
	G(10804) = 55;
	G(10954) = 57;
	G(11145) = 4;
	G(11147) = 11;
	G(11180) = 258;
	G(11154) = 8;
	G(11220) = 257;
	G(11158) = 260;
	G(11239) = 259;
	G(11360) = 62;
	G(11385) = 61;
	G(11402) = 58;
	G(11376) = 64;
	G(62487) = 4;
	G(62552) = 318;
	G(62489) = 11;
	G(62496) = 8;
	G(62568) = 317;
	G(62500) = 316;
	G(11741) = 93;
	G(11742) = 76;
	G(11743) = 77;
	G(11745) = 85;
	G(11749) = 87;
	G(11752) = 103;
	G(11754) = 82;
	G(11757) = 102;
	G(11760) = 72;
	G(11761) = 91;
	G(11762) = 96;
	G(11764) = 88;
	G(11767) = 99;
	G(11769) = 81;
	G(11770) = 83;
	G(11776) = 73;
	G(11779) = 80;
	G(11780) = 75;
	G(11785) = 92;
	G(11786) = 100;
	G(11787) = 256;
	G(11788) = 98;
	G(11941) = 76;
	G(12007) = 130;
	G(11945) = 120;
	G(12010) = 117;
	G(12011) = 249;
	G(11951) = 103;
	G(11952) = 119;
	G(12018) = 114;
	G(11955) = 135;
	G(12020) = 116;
	G(11960) = 91;
	G(11964) = 122;
	G(11967) = 248;
	G(11970) = 250;
	G(11956) = 102;
	G(11975) = 115;
	G(11978) = 80;
	G(11947) = 123;
	G(11990) = 125;
	G(11991) = 137;
	G(11992) = 113;
	G(11997) = 133;
	G(12003) = 131;
	G(12001) = 121;
	G(12005) = 128;
	G(12537) = 93;
	G(12538) = 76;
	G(12539) = 77;
	G(12541) = 85;
	G(12545) = 87;
	G(12548) = 103;
	G(12550) = 82;
	G(12553) = 102;
	G(12556) = 72;
	G(12557) = 91;
	G(12558) = 96;
	G(12560) = 88;
	G(12563) = 99;
	G(12565) = 81;
	G(12566) = 83;
	G(12572) = 73;
	G(12575) = 80;
	G(12576) = 75;
	G(12581) = 92;
	G(12582) = 100;
	G(12583) = 90;
	G(12584) = 98;
	G(63681) = 4;
	G(63690) = 8;
	G(63683) = 11;
	G(63694) = 321;
	G(64079) = 323;
	G(13731) = 236;
	G(13732) = 76;
	G(13733) = 77;
	G(13766) = 73;
	G(13735) = 85;
	G(13769) = 80;
	G(13770) = 75;
	G(13739) = 87;
	G(13742) = 103;
	G(13744) = 82;
	G(13747) = 102;
	G(13750) = 72;
	G(13751) = 91;
	G(13752) = 96;
	G(13754) = 88;
	G(13757) = 99;
	G(13759) = 81;
	G(13760) = 83;
	G(64938) = 327;
	G(64875) = 4;
	G(64908) = 19;
	G(64877) = 11;
	G(64911) = 7;
	G(64880) = 5;
	G(64915) = 21;
	G(64884) = 8;
	G(64917) = 20;
	G(64888) = 2;
	G(64922) = 15;
	G(64896) = 23;
	G(64929) = 1;
	G(64879) = 17;
	G(64905) = 24;
	G(14400) = 231;
	G(14417) = 230;
	G(14429) = 228;
	G(61898) = 312;
	G(15392) = 221;
	G(66467) = 4;
	G(66469) = 336;
	G(66550) = 337;
	G(66519) = 335;
	G(62121) = 64;
	G(62130) = 61;
	G(62147) = 58;
	G(62105) = 313;
	G(16319) = 76;
	G(16320) = 77;
	G(16353) = 73;
	G(16322) = 85;
	G(16356) = 80;
	G(16357) = 226;
	G(16326) = 87;
	G(16329) = 103;
	G(16331) = 82;
	G(16334) = 102;
	G(16337) = 72;
	G(16338) = 91;
	G(16339) = 96;
	G(16341) = 88;
	G(16344) = 99;
	G(16346) = 81;
	G(16557) = 193;
	G(16990) = 210;
	G(17329) = 102;
	G(17314) = 76;
	G(17348) = 73;
	G(17333) = 91;
	G(17334) = 96;
	G(17351) = 80;
	G(17321) = 87;
	G(17339) = 225;
	G(17324) = 103;
	G(17341) = 81;
	G(17599) = 206;
	G(18556) = 106;
	G(19172) = 198;
	G(19303) = 166;
	G(19304) = 76;
	G(19305) = 77;
	G(19307) = 85;
	G(19311) = 87;
	G(19314) = 103;
	G(19316) = 82;
	G(19319) = 102;
	G(19321) = 196;
	G(19322) = 72;
	G(19323) = 91;
	G(19324) = 96;
	G(19326) = 88;
	G(19329) = 99;
	G(19331) = 81;
	G(19332) = 83;
	G(19338) = 73;
	G(19341) = 80;
	G(19342) = 75;
	G(19347) = 92;
	G(19348) = 100;
	G(19350) = 164;
	G(20099) = 104;
	G(20100) = 76;
	G(20101) = 77;
	G(20134) = 73;
	G(20103) = 85;
	G(20137) = 80;
	G(20138) = 75;
	G(20107) = 87;
	G(20110) = 103;
	G(20112) = 82;
	G(20115) = 102;
	G(20118) = 72;
	G(20119) = 91;
	G(20120) = 96;
	G(20122) = 88;
	G(20125) = 99;
	G(20127) = 81;
	G(20128) = 83;
	G(20745) = 106;
	G(20729) = 105;
	G(20775) = 108;
	G(21095) = 76;
	G(21096) = 77;
	G(21129) = 73;
	G(21098) = 85;
	G(21132) = 80;
	G(21133) = 75;
	G(21102) = 87;
	G(21105) = 103;
	G(21107) = 82;
	G(21110) = 102;
	G(21113) = 72;
	G(21114) = 91;
	G(21115) = 96;
	G(21117) = 88;
	G(21120) = 99;
	G(21122) = 81;
	G(21123) = 192;
	G(21293) = 190;
	G(21294) = 76;
	G(21295) = 77;
	G(21328) = 73;
	G(21297) = 85;
	G(21331) = 80;
	G(21332) = 75;
	G(21301) = 87;
	G(21304) = 103;
	G(21306) = 82;
	G(21309) = 102;
	G(21312) = 72;
	G(21313) = 91;
	G(21314) = 96;
	G(21316) = 88;
	G(21319) = 99;
	G(21321) = 81;
	G(21322) = 83;
	G(21891) = 76;
	G(21957) = 130;
	G(21895) = 120;
	G(21960) = 117;
	G(21897) = 123;
	G(21901) = 103;
	G(21902) = 119;
	G(21968) = 114;
	G(21905) = 135;
	G(21970) = 116;
	G(21910) = 91;
	G(21914) = 122;
	G(21906) = 102;
	G(21925) = 115;
	G(21928) = 80;
	G(21940) = 125;
	G(21941) = 137;
	G(21942) = 113;
	G(21947) = 133;
	G(21955) = 128;
	G(21951) = 121;
	G(21953) = 131;
	G(22089) = 174;
	G(22090) = 76;
	G(22091) = 77;
	G(22124) = 73;
	G(22093) = 85;
	G(22127) = 80;
	G(22128) = 75;
	G(22097) = 87;
	G(22100) = 103;
	G(22102) = 82;
	G(22105) = 102;
	G(22108) = 72;
	G(22109) = 91;
	G(22110) = 96;
	G(22112) = 88;
	G(22115) = 99;
	G(22117) = 81;
	G(22118) = 83;
	G(23515) = 156;
	G(25074) = 153;
	G(25075) = 76;
	G(25076) = 77;
	G(25109) = 73;
	G(25078) = 85;
	G(25112) = 80;
	G(25113) = 75;
	G(25082) = 87;
	G(25085) = 103;
	G(25087) = 82;
	G(25090) = 102;
	G(25093) = 72;
	G(25094) = 91;
	G(25095) = 96;
	G(25097) = 88;
	G(25100) = 99;
	G(25102) = 81;
	G(25103) = 83;
	G(25273) = 93;
	G(25274) = 76;
	G(25275) = 77;
	G(25277) = 85;
	G(25281) = 87;
	G(25284) = 103;
	G(25286) = 82;
	G(25289) = 102;
	G(25292) = 72;
	G(25293) = 91;
	G(25294) = 96;
	G(25296) = 88;
	G(25299) = 99;
	G(25301) = 81;
	G(25302) = 83;
	G(25308) = 73;
	G(25311) = 80;
	G(25312) = 75;
	G(25317) = 92;
	G(25318) = 100;
	G(25319) = 152;
	G(25320) = 98;
	G(26134) = 128;
	G(26136) = 130;
	G(26080) = 103;
	G(26107) = 80;
	G(26139) = 117;
	G(26070) = 76;
	G(26149) = 116;
	G(26076) = 123;
	G(26081) = 119;
	G(26147) = 114;
	G(26084) = 135;
	G(26085) = 102;
	G(26119) = 125;
	G(26120) = 150;
	G(26121) = 113;
	G(26126) = 133;
	G(26093) = 122;
	G(26089) = 91;
	G(26104) = 115;
	G(26074) = 120;
	G(26268) = 144;
	G(26269) = 76;
	G(26270) = 77;
	G(26303) = 73;
	G(26272) = 85;
	G(26306) = 80;
	G(26307) = 75;
	G(26276) = 87;
	G(26279) = 103;
	G(26281) = 82;
	G(26284) = 102;
	G(26287) = 72;
	G(26288) = 91;
	G(26289) = 96;
	G(26291) = 88;
	G(26294) = 99;
	G(26296) = 81;
	G(26297) = 83;
	G(27097) = 138;
	G(27860) = 141;
	G(27861) = 76;
	G(27862) = 77;
	G(27895) = 73;
	G(27864) = 85;
	G(27898) = 80;
	G(27899) = 75;
	G(27868) = 87;
	G(27871) = 103;
	G(27873) = 82;
	G(27876) = 102;
	G(27879) = 72;
	G(27880) = 91;
	G(27881) = 96;
	G(27883) = 88;
	G(27886) = 99;
	G(27888) = 81;
	G(27889) = 83;
	G(28092) = 142;
	G(28108) = 106;
	G(28689) = 145;
	G(28705) = 106;
	G(28951) = 146;
	G(29286) = 148;
	G(29265) = 149;
	G(30480) = 154;
	G(30496) = 106;
	G(31442) = 93;
	G(31443) = 76;
	G(31444) = 77;
	G(31446) = 85;
	G(31450) = 87;
	G(31453) = 103;
	G(31455) = 82;
	G(31458) = 102;
	G(31461) = 72;
	G(31462) = 91;
	G(31463) = 96;
	G(31465) = 88;
	G(31468) = 99;
	G(31470) = 81;
	G(31471) = 83;
	G(31477) = 73;
	G(31480) = 80;
	G(31481) = 75;
	G(31486) = 92;
	G(31487) = 100;
	G(31488) = 170;
	G(31489) = 98;
	G(31641) = 166;
	G(31642) = 76;
	G(31643) = 77;
	G(31645) = 85;
	G(31649) = 87;
	G(31652) = 103;
	G(31654) = 82;
	G(31657) = 102;
	G(31722) = 163;
	G(31659) = 162;
	G(31660) = 72;
	G(31661) = 91;
	G(31662) = 96;
	G(31664) = 88;
	G(31667) = 99;
	G(31732) = 165;
	G(31669) = 81;
	G(31670) = 83;
	G(31676) = 73;
	G(31679) = 80;
	G(31680) = 75;
	G(31685) = 92;
	G(31686) = 100;
	G(31688) = 164;
	G(31841) = 161;
	G(33083) = 106;
	G(33233) = 166;
	G(33234) = 76;
	G(33235) = 77;
	G(33237) = 85;
	G(33241) = 87;
	G(33244) = 103;
	G(33246) = 82;
	G(33249) = 102;
	G(33251) = 168;
	G(33252) = 72;
	G(33253) = 91;
	G(33254) = 96;
	G(33256) = 88;
	G(33259) = 99;
	G(33261) = 81;
	G(33262) = 83;
	G(33268) = 73;
	G(33271) = 80;
	G(33272) = 75;
	G(33277) = 92;
	G(33278) = 100;
	G(33280) = 164;
	G(34228) = 93;
	G(34229) = 76;
	G(34230) = 77;
	G(34232) = 85;
	G(34236) = 87;
	G(34239) = 103;
	G(34241) = 82;
	G(34244) = 102;
	G(34247) = 72;
	G(34248) = 91;
	G(34249) = 96;
	G(34251) = 88;
	G(34254) = 99;
	G(34256) = 81;
	G(34257) = 83;
	G(34263) = 73;
	G(34266) = 80;
	G(34267) = 75;
	G(34272) = 92;
	G(34273) = 100;
	G(34274) = 173;
	G(34275) = 98;
	G(34675) = 106;
	G(34930) = 176;
	G(34918) = 177;
	G(34910) = 180;
	G(35308) = 184;
	G(35422) = 182;
	G(35423) = 76;
	G(35424) = 77;
	G(35457) = 73;
	G(35426) = 85;
	G(35460) = 80;
	G(35461) = 75;
	G(35430) = 87;
	G(35433) = 103;
	G(35435) = 82;
	G(35438) = 102;
	G(35441) = 72;
	G(35442) = 91;
	G(35443) = 96;
	G(35445) = 88;
	G(35448) = 99;
	G(35450) = 81;
	G(35451) = 83;
	G(35654) = 181;
	G(36267) = 106;
	G(36251) = 183;
	G(37046) = 189;
	G(37047) = 105;
	G(37059) = 188;
	G(37093) = 187;
	G(37859) = 106;
	G(38248) = 193;
	G(38408) = 76;
	G(38409) = 77;
	G(38442) = 73;
	G(38411) = 85;
	G(38445) = 80;
	G(38446) = 195;
	G(38415) = 87;
	G(38418) = 103;
	G(38420) = 82;
	G(38423) = 102;
	G(38426) = 72;
	G(38427) = 91;
	G(38428) = 96;
	G(38430) = 88;
	G(38433) = 99;
	G(38435) = 81;
	G(39418) = 102;
	G(39403) = 76;
	G(39437) = 73;
	G(39422) = 91;
	G(39423) = 96;
	G(39440) = 80;
	G(39413) = 103;
	G(39430) = 200;
	G(40995) = 76;
	G(40996) = 77;
	G(41029) = 73;
	G(40998) = 209;
	G(41032) = 80;
	G(41002) = 87;
	G(41005) = 103;
	G(41010) = 102;
	G(41014) = 91;
	G(41015) = 96;
	G(41020) = 99;
	G(41022) = 81;
	G(41666) = 210;
	G(41806) = 102;
	G(41791) = 76;
	G(41792) = 218;
	G(41825) = 73;
	G(41810) = 91;
	G(41811) = 96;
	G(41828) = 80;
	G(41798) = 87;
	G(41816) = 99;
	G(41801) = 103;
	G(41818) = 81;
	G(15555) = 227;
	G(43451) = 221;
	G(43995) = 102;
	G(43980) = 76;
	G(44014) = 73;
	G(43999) = 91;
	G(44000) = 96;
	G(44017) = 80;
	G(43987) = 87;
	G(44005) = 224;
	G(43990) = 103;
	G(44007) = 81;
	G(45771) = 76;
	G(45772) = 77;
	G(45805) = 73;
	G(45774) = 85;
	G(45808) = 80;
	G(45778) = 87;
	G(45781) = 103;
	G(45786) = 102;
	G(45790) = 91;
	G(45791) = 96;
	G(45793) = 234;
	G(45796) = 99;
	G(45798) = 81;
	G(46269) = 233;
	G(46653) = 206;
	G(58807) = 296;
	G(47013) = 106;
	G(47188) = 240;
	G(47257) = 242;
	G(47236) = 238;
	G(47561) = 246;
	G(47562) = 76;
	G(47563) = 77;
	G(47596) = 73;
	G(47565) = 85;
	G(47599) = 80;
	G(47600) = 75;
	G(47569) = 87;
	G(47572) = 103;
	G(47574) = 82;
	G(47577) = 102;
	G(47580) = 72;
	G(47581) = 91;
	G(47582) = 96;
	G(47584) = 88;
	G(47587) = 99;
	G(47589) = 81;
	G(47590) = 83;
	G(47833) = 245;
	G(47992) = 105;
	G(48038) = 244;
	G(67462) = 4;
	G(67464) = 336;
	G(67514) = 340;
	G(48987) = 105;
	G(49033) = 247;
	G(49003) = 106;
	G(49382) = 250;
	G(49423) = 254;
	G(49783) = 64;
	G(49809) = 253;
}

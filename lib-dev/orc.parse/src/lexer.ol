module orc.parse;

use orc.ast: Token, tokens;

// Ignores whitespace, comments. Turns some identifier tokens to keyword tokens.
class Lexer {
	new (FileWindow fw) {
		this._impl = RawLexer(fw);
	}
	
	RawLexer _impl;
	
	bool moveNext() {
		while this._impl.moveNext() {
			my t = this._impl.current;
			
			if t.type is tokens.WS {
				next;
			}
			
			if t.type is tokens.CSL {
				next;
			}
			
			if t.type is tokens.CML {
				next;
			}
			
			if t.type is tokens.Id {
				_classifyKeyword(t);
			}
			
			ret #t;
		}
		
		ret #f;
	}
	
	Token current -> this._impl.current;
}

void _classifyKeyword(Token t) {
	my data = t.data;
	
	if data.length < 2 {
		ret;
	}
	
	my c = data(0);
	
	// 97 = a
	if c < 97 {
		// 95 = _
		if c is 95 {
			if data.eq("__builtin") {
				t.type = tokens.KBuiltin;
			} else if data.eq("__link") {
				t.type = tokens.KLink;
			}
		}
		
		ret;
	}
	
	// 121 = y
	if c > 121 {
		ret;
	}
	
	if c < 109 {
		// a - l
		
		if c < 102 {
			// a - e
			
			if data.eq("as") {
				t.type = tokens.KAs;
			} else if data.eq("and") {
				t.type = tokens.KAnd;
			} else if data.eq("any") {
				t.type = tokens.KAny;
			} else if data.eq("break") {
				t.type = tokens.KBreak;
			} else if data.eq("catch") {
				t.type = tokens.KCatch;
			} else if data.eq("class") {
				t.type = tokens.KClass;
			} else if data.eq("do") {
				t.type = tokens.KDo;
			} else if data.eq("else") {
				t.type = tokens.KElse;
			} else if data.eq("enum") {
				t.type = tokens.KEnum;
			} else if data.eq("error") {
				t.type = tokens.KError;
			}
		} else {
			// f - l
			
			if data.eq("finally") {
				t.type = tokens.KFinally;
			} else if data.eq("for") {
				t.type = tokens.KFor;
			} else if data.eq("get") {
				t.type = tokens.KGet;
			} else if data.eq("if") {
				t.type = tokens.KIf;
			} else if data.eq("in") {
				t.type = tokens.KIn;
			} else if data.eq("is") {
				t.type = tokens.KIs;
			} else if data.eq("loop") {
				t.type = tokens.KLoop;
			}
		}
	} else {
		// m - y
		
		if c < 115 {
			// m - r
			
			if data.eq("module") {
				t.type = tokens.KModule;
			} else if data.eq("my") {
				t.type = tokens.KMy;
			} else if data.eq("new") {
				t.type = tokens.KNew;
			} else if data.eq("next") {
				t.type = tokens.KNext;
			} else if data.eq("not") {
				t.type = tokens.KNot;
			} else if data.eq("or") {
				t.type = tokens.KOr;
			} else if data.eq("out") {
				t.type = tokens.KOut;
			} else if data.eq("ret") {
				t.type = tokens.KRet;
			} else if data.eq("role") {
				t.type = tokens.KRole;
			}
		} else {
			// s - y
			
			if data.eq("set") {
				t.type = tokens.KSet;
			} else if data.eq("super") {
				t.type = tokens.KSuper;
			} else if data.eq("switch") {
				t.type = tokens.KSwitch;
			} else if data.eq("this") {
				t.type = tokens.KThis;
			} else if data.eq("use") {
				t.type = tokens.KUse;
			} else if data.eq("value") {
				t.type = tokens.KValue;
			} else if data.eq("while") {
				t.type = tokens.KWhile;
			} else if data.eq("yield") {
				t.type = tokens.KYield;
			}
		}
	}
}

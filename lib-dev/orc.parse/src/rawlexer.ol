module orc.parse;
use olive.lang: denull;
use orc.ast: Token;
use tmpdev: List;
class RawLexer {
	FileWindow fh;
	Token? t;
	int s;
	int c;
	new (FileWindow fh) {
		this.fh = fh;
		this.t = #null;
		this.s = 0;
		this.c = fh.step();
	}
	Token current -> denull[Token](this.t);
	bool moveNext() {
		my fh = this.fh;
		loop {
			my s = this.s;
			my c = this.c;
			if s < 14 {
				if s < 7 {
					if s < 3 {
						if s < 1 {
							if c is 36 {
								this.c = fh.step();
								this.s = 1;
								next;
							}
							if charclass.a(c) {
								this.c = fh.step();
								this.s = 3;
								next;
							}
							if charclass.n(c) {
								this.c = fh.step();
								this.s = 4;
								next;
							}
							if c is 35 {
								this.c = fh.step();
								this.s = 5;
								next;
							}
							if charclass.ws(c) {
								this.c = fh.step();
								this.s = 7;
								next;
							}
							if c is 39 {
								this.c = fh.step();
								this.s = 23;
								next;
							}
							if c is 34 {
								this.c = fh.step();
								this.s = 25;
								next;
							}
							if c is 40 {
								this.c = fh.step();
								this._emit(20);
								ret #t;
							}
							if c is 41 {
								this.c = fh.step();
								this._emit(21);
								ret #t;
							}
							if c is 91 {
								this.c = fh.step();
								this._emit(22);
								ret #t;
							}
							if c is 93 {
								this.c = fh.step();
								this._emit(23);
								ret #t;
							}
							if c is 123 {
								this.c = fh.step();
								this._emit(24);
								ret #t;
							}
							if c is 125 {
								this.c = fh.step();
								this._emit(25);
								ret #t;
							}
							if c is 46 {
								this.c = fh.step();
								this._emit(26);
								ret #t;
							}
							if c is 63 {
								this.c = fh.step();
								this.s = 8;
								next;
							}
							if c is 58 {
								this.c = fh.step();
								this._emit(28);
								ret #t;
							}
							if c is 59 {
								this.c = fh.step();
								this._emit(29);
								ret #t;
							}
							if c is 44 {
								this.c = fh.step();
								this._emit(30);
								ret #t;
							}
							if c is 64 {
								this.c = fh.step();
								this._emit(31);
								ret #t;
							}
							if c is 61 {
								this.c = fh.step();
								this._emit(32);
								ret #t;
							}
							if c is 43 {
								this.c = fh.step();
								this.s = 9;
								next;
							}
							if c is 45 {
								this.c = fh.step();
								this.s = 10;
								next;
							}
							if c is 42 {
								this.c = fh.step();
								this.s = 11;
								next;
							}
							if c is 47 {
								this.c = fh.step();
								this.s = 12;
								next;
							}
							if c is 92 {
								this.c = fh.step();
								this.s = 13;
								next;
							}
							if c is 94 {
								this.c = fh.step();
								this.s = 14;
								next;
							}
							if c is 126 {
								this.c = fh.step();
								this.s = 15;
								next;
							}
							if c is 33 {
								this.c = fh.step();
								this._emit(47);
								ret #t;
							}
							if c is 124 {
								this.c = fh.step();
								this._emit(48);
								ret #t;
							}
							if c is 38 {
								this.c = fh.step();
								this._emit(49);
								ret #t;
							}
							if c is 37 {
								this.c = fh.step();
								this._emit(50);
								ret #t;
							}
							if c is 60 {
								this.c = fh.step();
								this.s = 16;
								next;
							}
							if c is 62 {
								this.c = fh.step();
								this.s = 17;
								next;
							}
							if c is -1 {
								if fh._j > fh._i {
									this._emit(1);
									ret #t;
								}
								this._emit(0);
								this.s = 999999;
								ret #t;
							}
							else {
								this.c = fh.step();
								this._emit(1);
								ret #t;
							}
						}
						else {
							if s < 2 {
								if charclass.a(c) {
									this.c = fh.step();
									this.s = 2;
									next;
								}
								else {
									this._emit(1);
									this.s = 0;
									ret #t;
								}
							}
							else {
								if charclass.an(c) {
									this.c = fh.step();
									next;
								}
								else {
									this._emit(10);
									this.s = 0;
									ret #t;
								}
							}
						}
					}
					else {
						if s < 5 {
							if s < 4 {
								if charclass.an(c) {
									this.c = fh.step();
									next;
								}
								else {
									this._emit(10);
									this.s = 0;
									ret #t;
								}
							}
							else {
								if charclass.n(c) {
									this.c = fh.step();
									next;
								}
								else {
									this._emit(12);
									this.s = 0;
									ret #t;
								}
							}
						}
						else {
							if s < 6 {
								if charclass.a(c) {
									this.c = fh.step();
									this.s = 6;
									next;
								}
								else {
									this._emit(1);
									this.s = 0;
									ret #t;
								}
							}
							else {
								if charclass.an(c) {
									this.c = fh.step();
									next;
								}
								else {
									this._emit(13);
									this.s = 0;
									ret #t;
								}
							}
						}
					}
				}
				else {
					if s < 10 {
						if s < 8 {
							if charclass.ws(c) {
								this.c = fh.step();
								next;
							}
							else {
								this._emit(2);
								this.s = 0;
								ret #t;
							}
						}
						else {
							if s < 9 {
								if c is 63 {
									this.c = fh.step();
									this._emit(58);
									this.s = 0;
									ret #t;
								}
								else {
									this._emit(27);
									this.s = 0;
									ret #t;
								}
							}
							else {
								if c is 61 {
									this.c = fh.step();
									this._emit(60);
									this.s = 0;
									ret #t;
								}
								else {
									this._emit(40);
									this.s = 0;
									ret #t;
								}
							}
						}
					}
					else {
						if s < 12 {
							if s < 11 {
								if c is 62 {
									this.c = fh.step();
									this._emit(33);
									this.s = 0;
									ret #t;
								}
								if c is 61 {
									this.c = fh.step();
									this._emit(61);
									this.s = 0;
									ret #t;
								}
								else {
									this._emit(41);
									this.s = 0;
									ret #t;
								}
							}
							else {
								if c is 61 {
									this.c = fh.step();
									this._emit(62);
									this.s = 0;
									ret #t;
								}
								else {
									this._emit(42);
									this.s = 0;
									ret #t;
								}
							}
						}
						else {
							if s < 13 {
								if c is 61 {
									this.c = fh.step();
									this._emit(63);
									this.s = 0;
									ret #t;
								}
								if c is 47 {
									this.c = fh.step();
									this.s = 20;
									next;
								}
								if c is 42 {
									this.c = fh.step();
									this.s = 21;
									next;
								}
								else {
									this._emit(43);
									this.s = 0;
									ret #t;
								}
							}
							else {
								if c is 61 {
									this.c = fh.step();
									this._emit(64);
									this.s = 0;
									ret #t;
								}
								else {
									this._emit(44);
									this.s = 0;
									ret #t;
								}
							}
						}
					}
				}
			}
			else {
				if s < 21 {
					if s < 17 {
						if s < 15 {
							if c is 61 {
								this.c = fh.step();
								this._emit(65);
								this.s = 0;
								ret #t;
							}
							else {
								this._emit(45);
								this.s = 0;
								ret #t;
							}
						}
						else {
							if s < 16 {
								if c is 61 {
									this.c = fh.step();
									this._emit(66);
									this.s = 0;
									ret #t;
								}
								else {
									this._emit(46);
									this.s = 0;
									ret #t;
								}
							}
							else {
								if c is 61 {
									this.c = fh.step();
									this._emit(52);
									this.s = 0;
									ret #t;
								}
								if c is 62 {
									this.c = fh.step();
									this._emit(55);
									this.s = 0;
									ret #t;
								}
								if c is 60 {
									this.c = fh.step();
									this.s = 18;
									next;
								}
								else {
									this._emit(51);
									this.s = 0;
									ret #t;
								}
							}
						}
					}
					else {
						if s < 19 {
							if s < 18 {
								if c is 61 {
									this.c = fh.step();
									this._emit(54);
									this.s = 0;
									ret #t;
								}
								if c is 62 {
									this.c = fh.step();
									this.s = 19;
									next;
								}
								else {
									this._emit(53);
									this.s = 0;
									ret #t;
								}
							}
							else {
								if c is 61 {
									this.c = fh.step();
									this._emit(67);
									this.s = 0;
									ret #t;
								}
								else {
									this._emit(56);
									this.s = 0;
									ret #t;
								}
							}
						}
						else {
							if s < 20 {
								if c is 61 {
									this.c = fh.step();
									this._emit(68);
									this.s = 0;
									ret #t;
								}
								else {
									this._emit(57);
									this.s = 0;
									ret #t;
								}
							}
							else {
								if charclass.nnl(c) {
									this.c = fh.step();
									next;
								}
								else {
									this._emit(3);
									this.s = 0;
									ret #t;
								}
							}
						}
					}
				}
				else {
					if s < 24 {
						if s < 22 {
							if c is 42 {
								this.c = fh.step();
								this.s = 22;
								next;
							}
							else {
								this.c = fh.step();
								next;
							}
						}
						else {
							if s < 23 {
								if c is 47 {
									this.c = fh.step();
									this._emit(4);
									this.s = 0;
									ret #t;
								}
								else {
									this._emit(1);
									this.s = 0;
									ret #t;
								}
							}
							else {
								if c is 39 {
									this.c = fh.step();
									this._emit(11);
									this.s = 0;
									ret #t;
								}
								if c is 92 {
									this.c = fh.step();
									this.s = 24;
									next;
								}
								if charclass.nnl(c) {
									this.c = fh.step();
									next;
								}
								else {
									this._emit(1);
									this.s = 0;
									ret #t;
								}
							}
						}
					}
					else {
						if s < 26 {
							if s < 25 {
								if charclass.nnl(c) {
									this.c = fh.step();
									this.s = 23;
									next;
								}
								else {
									this._emit(1);
									this.s = 0;
									ret #t;
								}
							}
							else {
								if c is 34 {
									this.c = fh.step();
									this._emit(11);
									this.s = 0;
									ret #t;
								}
								if c is 92 {
									this.c = fh.step();
									this.s = 26;
									next;
								}
								if charclass.nnl(c) {
									this.c = fh.step();
									next;
								}
								else {
									this._emit(1);
									this.s = 0;
									ret #t;
								}
							}
						}
						else {
							if s < 27 {
								if charclass.nnl(c) {
									this.c = fh.step();
									this.s = 25;
									next;
								}
								else {
									this._emit(1);
									this.s = 0;
									ret #t;
								}
							}
							else {
								ret #f;
							}
						}
					}
				}
			}
		}
	}
	void _emit(int tt) {
		this.t = Token(this.fh.start, this.fh.end, tt, this.fh.flush());
	}
}

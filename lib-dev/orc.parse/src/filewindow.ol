module orc.parse;

use tmpdev: FileReader;
use orc.ast: Location;

class FileWindow {
	new (FileReader fr) {
		this.head = -1;
		this.start = Location(0, 0);
		this.end = Location(0, 0);
		
		this._fr = fr;
		this._fileDone = #f;
		this._i = 0;
		this._j = -1;
		this._k = 0;
		this._buf = '';
		this._c = -1;
	}
	
	int head;
	Location start;
	Location end;
	
	FileReader _fr;
	bool _fileDone;
	int _i;
	int _j;
	int _k;
	str _buf;
	int _c;
	
	str flush() {
		my span = this._buf.slice(this._i, this._j);
		this._i = this._i + this._j;
		this._k = this._k + this._j;
		this._j = 0;
		this.start = this.end;
		this.end = Location(this.start.line, this.start.col);
		ret span;
	}
	
	bool needsFlush() -> this._j > 0;
	
	int step() {
		if this._c is 10 {
			this.end.line = this.end.line + 1;
			this.end.col = 0;
		} else {
			this.end.col = this.end.col + 1;
		}
		my c = this._stepAux();
		this._c = c;
		ret c;
	}
	
	int _stepAux() {
		this._j = this._j + 1;
		my k = this._i + this._j;
		
		if k < this._buf.length {
			ret this._buf(k);
		}
		
		if this._fileDone {
			ret -1;
		}
		
		// Grow the buffer
		
		if not this._fr.read(100) {
			this._fileDone = #t;
		}
		
		my read = this._fr.current;
		
		if read.length {
			if this._i > 100 {
				this._buf = this._buf.slice(this._i, this._buf.length);
				this._i = 0;
			}
			
			this._buf = this._buf ~ read;
			ret this._buf(this._i + this._j);
		}
		
		ret -1;
	}
}

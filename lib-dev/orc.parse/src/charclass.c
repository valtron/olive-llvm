#include <olive.lang/inc/types.h>

#include <ctype.h>

bool orc__charclass_nnl(int c) {
	return c >= 0 && c != 0x0A;
}

bool orc__charclass_alpha(int c) {
	return isalpha(c) != 0 || c == '_';
}

bool orc__charclass_num(int c) {
	return isdigit(c) != 0;
}

bool orc__charclass_alnum(int c) {
	return isalnum(c) != 0 || c == '_';
}

bool orc__charclass_ws(int c) {
	return isspace(c) != 0;
}

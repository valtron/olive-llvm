module orc.parse;

use olive: lang as ol;
use orc: ast;
use tmpdev: List, Hashtable;

ast.Syntax parse(Lexer l) {
	if not TABLES_INITIALIZED {
		init_actions();
		init_goto();
		TABLES_INITIALIZED = #t;
	}
	
	my stack = List[ParserState]();
	stack.push(ParserState());
	my S = List[any](10);
	my stop = #f;
	
	while l.moveNext() {
		my tok = l.current;
		
		loop {
			my state = stack.top();
			my a = ACTION(state.id - 500);
			
			if a is #null {
				a = ACTION(state.id * 199 + tok.type);
			}
			
			if a is Reduce {
				my r = ol.cast[Reduce](a);
				S.clear();
				my h = stack.count;
				my i = h - r.count;
				while i < h {
					S.push(stack(i).syntax);
					i = i + 1;
				}
				stack.popn(r.count);
				state = stack.top();
				my start = state.loc;
				my obj = reduce(r.reductionId, S, start, state.loc);
				stack.push(ParserState(GOTO(state.id * 199 + r.headId), obj, start));
				next;
			}
			
			if a is Shift {
				my s = ol.cast[Shift](a);
				stack.push(ParserState(s.sid, tok, tok.end));
				break;
			}
			
			if a is #accept {
				ret state.syntax;
			}
			
			stop = #t;
			break;
		}
		
		if stop {
			break;
		}
	}
	
	my state = stack.top();
	stack.dispose();
	S.dispose();
	
	my loc = state.loc;
	ret ast.Error(loc, loc);
}

class ParserState {
	int id;
	ast.Syntax syntax;
	ast.Location loc;
	
	new () {
		this.id = 0;
		this.loc = ast.Location(0, 0);
		this.syntax = ast.Error(this.loc, this.loc);
	}
	
	new (int id, ast.Syntax syntax, ast.Location loc) {
		this.id = id;
		this.syntax = syntax;
		this.loc = loc;
	}
}

enum Action {
	Reduce;
	Shift;
	#accept;
}

class Reduce {
	// The nonterminal that was reduced to
	int headId;
	// The id of the semantic action of the reduction
	int reductionId;
	// The number of symbols popped off the stack
	int count;
	
	new (int headId, int reductionId, int count) {
		this.headId = headId;
		this.reductionId = reductionId;
		this.count = count;
	}
}

class Shift {
	// The state to go to after shifting
	int sid;
	
	new (int sid) {
		this.sid = sid;
	}
}

Hashtable[Action] ACTION = Hashtable[Action](313, #null);
Hashtable[int] GOTO = Hashtable[int](313, #null);
bool TABLES_INITIALIZED = #f;

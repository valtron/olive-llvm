module orc.parse.charclass;

bool nnl(int c) __link 'orc__charclass_nnl';
bool a(int c) __link 'orc__charclass_alpha';
bool n(int c) __link 'orc__charclass_num';
bool an(int c) __link 'orc__charclass_alnum';
bool ws(int c) __link 'orc__charclass_ws';

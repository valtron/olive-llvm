module olive.lang;

class Int(#int32) {}
class Bool(#bit) {}
class Address(#addr) {}
class Any(#any) {}

enum Void {}

class Symbol {}

role Object {}

enum Nullable[T] {
	T;
	#null;
}

role Disposable {
	void dispose();
}

role Iterator[out T]: Disposable {
	bool moveNext();
	T current;
	
	void dispose() {}
}

role Iterable[out T] {
	Iterator[T] iter();
}

#include "../inc/types.h"

bool ol__bool_op_infix_and(bool a, bool b) {
	return a && b;
}

bool ol__bool_op_infix_or(bool a, bool b) {
	return a || b;
}

bool ol__bool_op_prefix_not(bool a) {
	return !a;
}

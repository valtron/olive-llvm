module olive.lang;

class Type {
	str name;
	int classHeight;
}

Type typeof(any obj) __link 'ol__typeof';
bool isinstance(any obj, Type t) __link 'ol__isa';

T cast[T](any obj) -> obj;
T denull[T](T? obj) -> cast[T](obj);

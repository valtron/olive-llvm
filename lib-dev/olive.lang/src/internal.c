#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../inc/common.h"
#include "../inc/types.h"

rawptr ol__mem_alloc(int len) {
	return malloc(len);
}

void ol__mem_dealloc(rawptr ptr) {
	free(ptr);
}

void ol__mem_zero(rawptr ptr, size_t nbytes) {
	memset(ptr, 0, nbytes);
}

void ol__mem_copy(rawptr dest, const rawptr src, size_t n) {
	memcpy(dest, src, n);
}

methptr ol__method(vtable* vt, int midx) {
	return VTABLE_METHODS(vt)[midx];
}

methptr ol__role_method(vtable* vt, OL_Type* role, int midx) {
	typeptr* roles = VTABLE_ROLES(vt);
	
	for (int i = 0; i < vt->nrole; ++i) {
		if (roles[i] != role) {
			continue;
		}
		
		methptr* methods = VTABLE_METHODS(vt);
		short roleoffset = VTABLE_ROLEOFFSETS(vt)[i];
		return methods[roleoffset + midx];
	}
	
	OLIVE_FATAL("can't find role in vtable");
	return nullptr;
}

bool ol__isav(vtable* vt, OL_Type* t) {
	if (t->classHeight) {
		if (t->classHeight > vt->nclass) {
			return false;
		}
		
		typeptr* types = VTABLE_CLASSES(vt);
		return types[t->classHeight - 1] == t;
	}
	
	int ntypes = vt->nrole;
	typeptr* types = VTABLE_ROLES(vt);
	
	for (int i = 0; i < ntypes; ++i) {
		if (types[i] == t) {
			return true;
		}
	}
	
	return false;
}

bool ol__isa(OL_Obj* obj, OL_Type* t) {
	return ol__isav(obj->vtbl, t);
}

OL_Type* ol__typeof(OL_Obj* obj) {
	vtable* vt = obj->vtbl;
	typeptr* types = VTABLE_CLASSES(vt);
	return types[vt->nclass - 1];
}

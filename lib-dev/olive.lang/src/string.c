#include <string.h>

#include "../inc/internal.h"
#include "../inc/types.h"

static vtable* OL_String__vtable;

OL_String*
ol__string_create(int len, const char* data) {
	OL_String* str = OL_ALLOC(OL_String);
	str->vtbl = OL_String__vtable;
	str->len = len;
	str->data = (char*)ol__mem_alloc(len);
	ol__mem_copy(str->data, (void*)data, len);
	return str;
}

OL_String*
ol__string_from_cstr(const char* cstr) {
	int len = strlen(cstr);
	return ol__string_create(len, cstr);
}

char*
ol__string_get_cstr(OL_String* str) {
	int l = str->len;
	char* cstr = (char*)ol__mem_alloc(l + 1);
	ol__mem_copy(cstr, str->data, l);
	cstr[l] = '\0';
	return cstr;
}

OL_String*
ol__string_concat(OL_String* a, OL_String* b) {
	int len = a->len + b->len;
	char* data = (char*)ol__mem_alloc(len);
	ol__mem_copy(data + 0     , a->data, a->len);
	ol__mem_copy(data + a->len, b->data, b->len);
	return ol__string_create(len, data);
}

OL_String*
ol__string_slice(OL_String* self, int offset, int len) {
	int strlen = self->len;
	
	if (offset < 0) {
		offset = strlen + offset;
	}
	
	if (offset < 0) {
		offset = 0;
	}
	
	if (offset >= strlen) {
		offset = strlen;
	}
	
	if (len < 0) {
		len = 0;
	}
	
	int maxlen = strlen - offset;
	
	if (len > maxlen) {
		len = maxlen;
	}
	
	return ol__string_create(len, self->data + offset);
}

bool
ol__string_equal(OL_String* self, OL_String* b) {
	int l = self->len;
	
	if (l != b->len) {
		return false;
	}
	
	char* da = self->data;
	char* db = b->data;
	
	while (l) {
		if (*da != *db) {
			return false;
		}
		
		da += 1;
		db += 1;
		l -= 1;
	}
	
	return true;
}

bool
ol__string_unequal(OL_String* self, OL_String* b) {
	return !ol__string_equal(self, b);
}

int
ol__string_at(OL_String* self, int idx) {
	return self->data[idx];
}

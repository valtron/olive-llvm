module olive.lang;

class String {
	int _len;
	Address _data;
	
	int length {
		ret this._len;
	}
	
	String slice(int offset, int len) __link 'ol__string_slice';
	
	bool eq(String other) __link 'ol__string_equal';
	
	bool neq(String other) __link 'ol__string_unequal';
	
	int this(int idx) __link 'ol__string_at';
}

String infix:~(String a, String b)
	__link 'ol__string_concat';

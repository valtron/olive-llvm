#include "../inc/types.h"

int ol__int_op_infix_plus(int a, int b) {
	return a + b;
}

int ol__int_op_infix_minus(int a, int b) {
	return a - b;
}

int ol__int_op_infix_star(int a, int b) {
	return a * b;
}

int ol__int_op_infix_rdivide(int a, int b) {
	return a / b;
}

int ol__int_op_infix_mod(int a, int b) {
	return a % b;
}

bool ol__int_op_infix_equal(int a, int b) {
	return a == b;
}

bool ol__int_op_infix_unequal(int a, int b) {
	return a != b;
}

bool ol__int_op_infix_lt(int a, int b) {
	return a < b;
}

bool ol__int_op_infix_lte(int a, int b) {
	return a <= b;
}

bool ol__int_op_infix_gt(int a, int b) {
	return a > b;
}

bool ol__int_op_infix_gte(int a, int b) {
	return a >= b;
}

int ol__int_op_prefix_minus(int a) {
	return -a;
}

module olive.lang;

Bool infix:and(Bool a, Bool b)
	__link 'ol__bool_op_infix_and';

Bool infix:or(Bool a, Bool b)
	__link 'ol__bool_op_infix_or';

Bool prefix:not(Bool a)
	__link 'ol__bool_op_prefix_not';

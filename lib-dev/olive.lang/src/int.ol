module olive.lang;

Int prefix:-(Int a)
	__link 'ol__int_op_prefix_minus';

Int infix:+(Int a, Int b)
	__link 'ol__int_op_infix_plus';

Int infix:-(Int a, Int b)
	__link 'ol__int_op_infix_minus';

Int infix:*(Int a, Int b)
	__link 'ol__int_op_infix_star';

Int infix:/(Int a, Int b)
	__link 'ol__int_op_infix_rdivide';

Int infix:%(Int a, Int b)
	__link 'ol__int_op_infix_mod';

Bool infix:=(Int a, Int b)
	__link 'ol__int_op_infix_equal';

Bool infix:<>(Int a, Int b)
	__link 'ol__int_op_infix_unequal';

Bool infix:<(Int a, Int b)
	__link 'ol__int_op_infix_lt';

Bool infix:<=(Int a, Int b)
	__link 'ol__int_op_infix_lte';

Bool infix:>(Int a, Int b)
	__link 'ol__int_op_infix_gt';

Bool infix:>=(Int a, Int b)
	__link 'ol__int_op_infix_gte';

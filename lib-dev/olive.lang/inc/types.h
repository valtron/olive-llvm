#pragma once

#include "../inc/common.h"

typedef char bool;

OLIVE_TYPE(OL_Obj, {});

OLIVE_TYPE(OL_Type, {
	OL_String* name;
	int classHeight;
});

OLIVE_TYPE(OL_String, {
	int len;
	char* data;
});

#define nullptr ((void*)0);
#define false 0
#define true 1

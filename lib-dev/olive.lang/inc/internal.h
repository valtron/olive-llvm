#include <stdlib.h>

void* ol__mem_alloc(int);
void ol__mem_dealloc(void*);
void ol__mem_copy(void*, const void*, size_t);
void ol__mem_zero(void*, size_t);

#define OL_ALLOC(T) ((T*)ol__mem_alloc(sizeof(T)))

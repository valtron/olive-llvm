#include "../inc/types.h"

#define OLIVE_STRING_CONST(s) ol__string_create(sizeof(s) / sizeof(s[0]), s)

OL_String* ol__string_create(int len, const char* data);
OL_String* ol__string_from_cstr(const char* cstr);
char* ol__string_get_cstr(OL_String* str);

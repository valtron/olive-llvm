#pragma once

#define OLIVE_TDEF(name) typedef struct s##_##name name

OLIVE_TDEF(OL_Type);
OLIVE_TDEF(OL_Obj);
OLIVE_TDEF(OL_String);

typedef struct {
	short nmeth;
	char nrole;
	char nclass;
	void* data[]; /* = {
		methptr methods[nmeth];
		typeptr roles[nrole];
		typeptr classes[nclass];
		short roleoffsets[nrole];
	} */
} vtable;

#define VTABLE_METHODS(vt) ((methptr*)(&(vt)->data))
#define VTABLE_ROLES(vt) ((typeptr*)(VTABLE_METHODS(vt) + vt->nmeth))
#define VTABLE_CLASSES(vt) ((typeptr*)(VTABLE_ROLES(vt) + vt->nrole))
#define VTABLE_ROLEOFFSETS(vt) ((short*)(VTABLE_CLASSES(vt) + vt->nclass))

typedef void* rawptr;
typedef void* methptr;
typedef OL_Type* typeptr;

#define OLIVE_TYPE(name, body) \
	struct s##_##name { vtable* vtbl; struct body; }; \
	typedef struct s##_##name name

#define OLIVE_FATAL(msg) { \
		fprintf(stderr, msg); \
		exit(EXIT_FAILURE); \
	}

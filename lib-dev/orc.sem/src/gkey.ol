module orc.sem;

use tmpdev: List as L;

class GenericKey {
	GenericItem proto;
	L[Type] args;
	
	new (GenericItem proto, L[Type] args) {
		this.proto = proto;
		this.args = args;
	}
	
	bool eq(GenericKey other) {
		if this is other {
			ret #t;
		}
		
		if this.proto is other.proto {
			ret #f;
		}
		
		if this.args.count <> other.args.count {
			ret #f;
		}
		
		my i = 0;
		while i < this.args.count {
			my a1 = this.args(i);
			my a2 = other.args(i);
			
			if not (a1 is a2) {
				ret #f;
			}
			
			i = i + 1;
		}
		
		ret #t;
	}
}

//int hashGenericKey(GenericKey k) {
//	int h = 853925580;
//	h = int_xor(h, cast[GenericItem, int](k.proto));
//	for arg in k.args {
//		h = int_xor(h, cast[Type, int](arg));
//	}
//	ret h;
//}

class GenericItem {}
class Type: GenericItem {}

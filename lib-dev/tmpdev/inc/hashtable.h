#include <olive.lang/inc/common.h>

typedef struct s_TDEV_Hashtable_Entry TDEV_Hashtable_Entry;

struct s_TDEV_Hashtable_Entry {
	int key;
	void* value;
	TDEV_Hashtable_Entry* next;
};

OLIVE_TYPE(TDEV_Hashtable, {
	int sizem1;
	void* default_;
	TDEV_Hashtable_Entry** data;
});

void tmpdev__hashtable_init_size(TDEV_Hashtable* self, int size, void* default_);
void* tmpdev__hashtable_get(TDEV_Hashtable* self, int key);
void tmpdev__hashtable_insert(TDEV_Hashtable* self, void* value, int key);

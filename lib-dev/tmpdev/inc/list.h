#include <olive.lang/inc/common.h>

OLIVE_TYPE(TDEV_List, {
	int len;
	int cap;
	void** data;
});

void tmpdev__list_init_empty(TDEV_List* self);
void tmpdev__list_init_size(TDEV_List* self, int size);
void* tmpdev__list_get(TDEV_List* self, int idx);
void tmpdev__list_set(TDEV_List* self, void* value, int idx);
void tmpdev__list_push(TDEV_List* self, void* value);
void* tmpdev__list_pop(TDEV_List* self);
void* tmpdev__list_top(TDEV_List* self);
void* tmpdev__list_shift(TDEV_List* self);
void tmpdev__list_unshift(TDEV_List* self, void* value);

#include <stdio.h>

#include <olive.lang/inc/string.h>

OL_String* tmpdev__utils_i2s(int i) {
	char buffer[20];
	int len = snprintf(buffer, sizeof(buffer), "%d", i);
	return ol__string_create(len, buffer);
}

module tmpdev.mem;

use olive.lang: Address as addr;

addr alloc(int len) __link 'ol__mem_alloc';

void dealloc(addr ptr) __link 'ol__mem_dealloc';
void copy(addr dest, addr src, int len) __link 'ol__mem_copy';

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

#include <olive.lang/inc/internal.h>
#include <olive.lang/inc/string.h>
#include <olive.lang/inc/types.h>

#include "../inc/list.h"

OLIVE_TYPE(TDEV_Filesystem, {});

OLIVE_TYPE(TDEV_FileReader, {
	FILE* fh;
	OL_String* current;
});

OLIVE_TYPE(TDEV_FileWriter, {
	FILE* fh;
});

// TODO: These need to be initialized
static vtable* TDEV_FileReader__vtable;
static vtable* TDEV_FileWriter__vtable;

bool
tmpdev__fs_isfile(TDEV_Filesystem* self, OL_String* path) {
	struct stat info;
	
	char* cstr = ol__string_get_cstr(path);
	int c = stat(cstr, &info);
	ol__mem_dealloc(cstr);
	
	if (c != 0) {
		return 0;
	}
	
	return S_ISREG(info.st_mode);
}

bool
tmpdev__fs_isdir(TDEV_Filesystem* self, OL_String* path) {
	struct stat info;
	
	char* cstr = ol__string_get_cstr(path);
	int c = stat(cstr, &info);
	ol__mem_dealloc(cstr);
	
	if (c != 0) {
		return 0;
	}
	
	return S_ISDIR(info.st_mode);
}

TDEV_List*
tmpdev__fs_list(TDEV_Filesystem* self, OL_String* path) {
	TDEV_List* list = OL_ALLOC(TDEV_List);
	tmpdev__list_init_empty(list);
	
	char* cstr = ol__string_get_cstr(path);
	DIR* dir = opendir(cstr);
	ol__mem_dealloc(cstr);
	
	if (dir != NULL) {
		struct dirent *ent;
		while ((ent = readdir(dir)) != NULL) {
			char* dname = ent->d_name;
			
			if (dname[0] == '.' && (dname[1] == '\0' || (dname[1] == '.' && dname[2] == '\0'))) {
				continue;
			}
			
			OL_String* subpath = ol__string_from_cstr(dname);
			tmpdev__list_push(list, subpath);
		}
		closedir(dir);
	}
	
	return list;
}

OL_String*
tmpdev__fs_parent(TDEV_Filesystem* self, OL_String* path) {
	char* d = path->data;
	int i = path->len - 1;
	while (i > 0) {
		if (d[i] == '/') {
			break;
		}
		--i;
	}
	return ol__string_create(i, d);
}

void
tmpdev__fs_remove(TDEV_Filesystem* self, OL_String* path) {
	char* cstr = ol__string_get_cstr(path);
	remove(cstr);
	ol__mem_dealloc(cstr);
}

TDEV_FileReader*
tmpdev__fs_reader(TDEV_Filesystem* self, OL_String* path) {
	TDEV_FileReader* reader = OL_ALLOC(TDEV_FileReader);
	reader->vtbl = TDEV_FileReader__vtable;
	char* cstr = ol__string_get_cstr(path);
	reader->fh = fopen(cstr, "r");
	reader->current = NULL;
	ol__mem_dealloc(cstr);
	return reader;
}

TDEV_FileWriter*
tmpdev__fs_writer(TDEV_Filesystem* self, OL_String* path) {
	TDEV_FileWriter* writer = OL_ALLOC(TDEV_FileWriter);
	writer->vtbl = TDEV_FileWriter__vtable;
	char* cstr = ol__string_get_cstr(path);
	writer->fh = fopen(cstr, "w");
	ol__mem_dealloc(cstr);
	return writer;
}

bool
tmpdev__fr_read(TDEV_FileReader* self, int n) {
	if (!self->fh) {
		return 0;
	}
	
	char* line = ol__mem_alloc(n);
	int read = fread(line, 1, n, self->fh);
	bool eof = (read < n);
	
	if (eof) {
		fclose(self->fh);
		self->fh = 0;
	}
	
	self->current = ol__string_create(read, line);
	ol__mem_dealloc(line);
	
	return !eof;
}

OL_String*
tmpdev__fr_current(TDEV_FileReader* self) {
	return self->current;
}

void
tmpdev__fr_close(TDEV_FileReader* self) {
	fclose(self->fh);
}

void
tmpdev__fw_write(TDEV_FileWriter* self, OL_String* data) {
	fwrite(data->data, 1, data->len, self->fh);
}

void
tmpdev__fw_close(TDEV_FileWriter* self) {
	fclose(self->fh);
}

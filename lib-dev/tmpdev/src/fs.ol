module tmpdev;

use olive.lang: Disposable;

class Filesystem {
	bool isfile(str path) __link 'tmpdev__fs_isfile';
	bool isdir(str path) __link 'tmpdev__fs_isdir';
	
	List[str] list(str dir) __link 'tmpdev__fs_list';
	str parent(str path) __link 'tmpdev__fs_parent';
	void remove(str path) __link 'tmpdev__fs_remove';
	
	FileReader reader(str path) __link 'tmpdev__fs_reader';
	FileWriter writer(str path) __link 'tmpdev__fs_writer';
}

class FileReader: Disposable {
	// read at most n bytes; returns #f if eof reached
	bool read(int n) __link 'tmpdev__fr_read';
	
	// get the data from the last call to read
	str current __link 'tmpdev__fr_current';
	
	void dispose() __link 'tmpdev__fr_close';
}

class FileWriter: Disposable {
	void write(str data) __link 'tmpdev__fw_write';
	void dispose() __link 'tmpdev__fw_close';
}

#include "../inc/hashtable.h"

#include <olive.lang/inc/internal.h>

void
tmpdev__hashtable_init_size(TDEV_Hashtable* self, int size, void* default_) {
	int s = 4;
	while (size > s) {
		s <<= 1;
	}
	self->sizem1 = s - 1;
	self->default_ = default_;
	int n = sizeof(TDEV_Hashtable_Entry*) * s;
	self->data = ol__mem_alloc(n);
	ol__mem_zero(self->data, n);
}

// TODO: Less crappy hash
#define CRAPPY_HASH(x) (((x)*2654435761)&self->sizem1)

void*
tmpdev__hashtable_get(TDEV_Hashtable* self, int key) {
	int h = CRAPPY_HASH(key);
	TDEV_Hashtable_Entry* entry = self->data[h];
	while (entry) {
		if (entry->key == key) return entry->value;
		entry = entry->next;
	}
	return self->default_;
}

void
tmpdev__hashtable_insert(TDEV_Hashtable* self, void* value, int key) {
	int h = CRAPPY_HASH(key);
	TDEV_Hashtable_Entry* old_entry = self->data[h];
	
	TDEV_Hashtable_Entry* new_entry = old_entry;
	while (new_entry) {
		if (new_entry->key == key) break;
		new_entry = new_entry->next;
	}
	
	if (!new_entry) {
		new_entry = ol__mem_alloc(sizeof(TDEV_Hashtable_Entry));
		new_entry->next = old_entry;
		new_entry->key = key;
		self->data[h] = new_entry;
	}
	
	new_entry->value = value;
}

#undef CRAPPY_HASH

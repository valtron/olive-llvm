module tmpdev;

use olive: lang as ol;

// Fixed-size, int-keyed hashtable. Not very useful.
class Hashtable[T] {
	int _sizem1;
	any _default;
	ol.Address _data;
	
	new (int size, any default) __link 'tmpdev__hashtable_init_size';
	
	T this(int key) {
		get __link 'tmpdev__hashtable_get';
		set __link 'tmpdev__hashtable_insert';
	}
}

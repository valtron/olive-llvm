#include <stdint.h>

#include <olive.lang/inc/types.h>

vtable* tmpdev__vtable(OL_Obj* obj) {
	return obj->vtbl;
}

rawptr tmpdev__shift_addr(char* a, int n) {
	return a + n;
}

int tmpdev__read_i8(int8_t* a, int offset) {
	return (int)a[offset];
}

int tmpdev__read_i16(int16_t* a, int offset) {
	return (int)a[offset >> 1];
}

int tmpdev__read_i32(int32_t* a, int offset) {
	return (int)a[offset >> 2];
}

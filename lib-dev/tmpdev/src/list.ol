module tmpdev;

use olive: lang as ol;

class List[T]: ol.Iterable[T], ol.Disposable {
	int _len;
	int _cap;
	ol.Address _data;
	
	new () __link 'tmpdev__list_init_empty';
	new (int l) __link 'tmpdev__list_init_size';
	
	int count -> this._len;
	int capacity -> this._cap;
	
	T this(int idx) {
		get __link 'tmpdev__list_get';
		set __link 'tmpdev__list_set';
	}
	
	T top() __link 'tmpdev__list_top';
	
	void push(T t) __link 'tmpdev__list_push';
	T pop() __link 'tmpdev__list_pop';
	void popn(int n) __link 'tmpdev__list_popn';
	void unshift(T t) __link 'tmpdev__list_unshift';
	T shift() __link 'tmpdev__list_shift';
	void clear() __link 'tmpdev__list_clear';
	void dispose() __link 'tmpdev__list_dispose';
	
	ListIterator[T] iter() -> ListIterator[T](this);
}

class ListIterator[out T]: ol.Iterator[T] {
	List[T] _l;
	int _i;
	
	new (List[T] list) {
		this._l = list;
		this._i = -1;
	}
	
	bool moveNext() {
		this._i = this._i + 1;
		ret this._i < this._l.count;
	}
	
	T current {
		my l = this._l;
		ret l(this._i);
	}
}

module tmpdev.debug;

use olive.lang: Address as addr;

addr vtable(any obj)
	__link 'tmpdev__vtable';

addr shift(addr a, int n)
	__link 'tmpdev__shift_addr';

int read_i8(addr a, int offset = 0)
	__link 'tmpdev__read_i8';

int read_i16(addr a, int offset = 0)
	__link 'tmpdev__read_i16';

int read_i32(addr a, int offset = 0)
	__link 'tmpdev__read_i32';

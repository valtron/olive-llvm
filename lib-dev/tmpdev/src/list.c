#include "../inc/list.h"

#include <olive.lang/inc/internal.h>

void
tmpdev__list_init_size(TDEV_List* self, int size) {
	self->len = 0;
	self->cap = size;
	self->data = ol__mem_alloc(sizeof(void*) * self->cap);
}

void
tmpdev__list_init_empty(TDEV_List* self) {
	tmpdev__list_init_size(self, 2);
}

void*
tmpdev__list_get(TDEV_List* self, int idx) {
	if (idx < 0 || idx >= self->len) {
		// TODO: error handling
		return 0;
	}
	
	return self->data[idx];
}

void
tmpdev__list_set(TDEV_List* self, void* value, int idx) {
	if (idx < 0 || idx >= self->len) {
		// TODO: error handling
		return;
	}
	
	self->data[idx] = value;
}

void*
tmpdev__list_top(TDEV_List* self) {
	int l = self->len;
	if (l < 1) {
		// TODO: error handling
		return 0;
	}
	return self->data[l-1];
}

/*
 * @param toOffset: position in the list the data should be copied to
 * @param fromOffset
 */
void
resize_list(TDEV_List* self, int newsize, int toOffset, int fromOffset) {
	void** newdata = ol__mem_alloc(sizeof(void*) * self->cap);
	ol__mem_copy(newdata + toOffset, self->data + fromOffset, sizeof(void*) * newsize);
	ol__mem_dealloc(self->data);
	self->data = newdata;
}

/* Add an element to the end of the list */
void
tmpdev__list_push(TDEV_List* self, void* value) {
	if (self->len >= self->cap) {
		self->cap *= 2;
		resize_list(self, self->len, 0, 0);
	}
	
	self->data[self->len] = value;
	self->len += 1;
}

/* Remove and return an element from the end of the list */
void*
tmpdev__list_pop(TDEV_List* self) {
	if (self->len <= 0) {
		// TODO error handling
		return 0;
	}
	self->len--;
	void* ret = self->data[self->len];
	
	return ret;
}

/* Remove and return an element from the beginning of the list */
void*
tmpdev__list_shift(TDEV_List* self) {
	if (self->len <= 0) {
		// TODO: error handling
		return 0;
	}
	void* ret = self->data[0];
	self->len--;
	resize_list(self, self->len, 0, 1);

	return ret;
}

/* Add an element to the beginning of the list */
void
tmpdev__list_unshift(TDEV_List* self, void* value) {
	if (self->len >= self->cap) {
		self->cap *= 2;
		resize_list(self, self->len, 1, 0);
	} else {
		int i = self->len;
		while (i > 0) {
			self->data[i] = self->data[i-1];
			i--;
		}
	}
	self->len += 1;
	self->data[0] = value;
}

void
tmpdev__list_clear(TDEV_List* self) {
	self->len = 0;
}

void
tmpdev__list_popn(TDEV_List* self, int n) {
	int l = self->len;
	if (n > l) n = l;
	self->len = l - n;
}

void
tmpdev__list_dispose(TDEV_List* self) {
	self->len = 0;
	self->cap = 0;
	ol__mem_dealloc(self->data);
	self->data = 0;
}

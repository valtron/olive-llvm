module orc.ast;
class ClassDef: Syntax {
	Token kind;
	Token name;
	Token? layout;
	ListSyntax[GenericParam]? params;
	ListSyntax[TypeRef]? supers;
	ListSyntax[NonModule] body;
	new (
		Location start, Location end
		, Token kind
		, Token name
		, Token? layout
		, ListSyntax[GenericParam]? params
		, ListSyntax[TypeRef]? supers
		, ListSyntax[NonModule] body
	)  {
		this.start = start; this.end = end;
		this.kind = kind;
		this.name = name;
		this.layout = layout;
		this.params = params;
		this.supers = supers;
		this.body = body;
	}
}
class EnumDef: Syntax {
	Token name;
	ListSyntax[GenericParam]? params;
	ListSyntax[EnumMember] body;
	new (
		Location start, Location end
		, Token name
		, ListSyntax[GenericParam]? params
		, ListSyntax[EnumMember] body
	)  {
		this.start = start; this.end = end;
		this.name = name;
		this.params = params;
		this.body = body;
	}
}
enum EnumMember {
	NonModule;
	Token;
	TypeRef;
}
class ExprAtomic: Expr {
	Token tok;
	new (
		Location start, Location end
		, Token tok
	)  {
		this.start = start; this.end = end;
		this.tok = tok;
	}
}
class ExprBinary: Expr {
	Token op;
	Expr lhs;
	Expr rhs;
	new (
		Location start, Location end
		, Token op
		, Expr lhs
		, Expr rhs
	)  {
		this.start = start; this.end = end;
		this.op = op;
		this.lhs = lhs;
		this.rhs = rhs;
	}
}
class ExprCall: Expr {
	Expr target;
	ListSyntax[Expr] args;
	new (
		Location start, Location end
		, Expr target
		, ListSyntax[Expr] args
	)  {
		this.start = start; this.end = end;
		this.target = target;
		this.args = args;
	}
}
class ExprDot: Expr {
	Expr base;
	ExprName member;
	new (
		Location start, Location end
		, Expr base
		, ExprName member
	)  {
		this.start = start; this.end = end;
		this.base = base;
		this.member = member;
	}
}
class ExprName: Syntax {
	Token name;
	ListSyntax[TypeRef]? args;
	new (
		Location start, Location end
		, Token name
		, ListSyntax[TypeRef]? args
	)  {
		this.start = start; this.end = end;
		this.name = name;
		this.args = args;
	}
}
class ExprStmt: Expr {
	Stmt stmt;
	new (
		Location start, Location end
		, Stmt stmt
	)  {
		this.start = start; this.end = end;
		this.stmt = stmt;
	}
}
class ExprUnary: Expr {
	Token op;
	Expr arg;
	new (
		Location start, Location end
		, Token op
		, Expr arg
	)  {
		this.start = start; this.end = end;
		this.op = op;
		this.arg = arg;
	}
}
enum FuncCode {
	#null;
	Expr;
	StmtBlock;
	Token;
}
class FuncEtter: Syntax {
	Token kind;
	FuncCode body;
	new (
		Location start, Location end
		, Token kind
		, FuncCode body
	)  {
		this.start = start; this.end = end;
		this.kind = kind;
		this.body = body;
	}
}
class FuncParam: Syntax {
	Token name;
	TypeRef type;
	Expr? default;
	new (
		Location start, Location end
		, Token name
		, TypeRef type
		, Expr? default
	)  {
		this.start = start; this.end = end;
		this.name = name;
		this.type = type;
		this.default = default;
	}
}
class GenericParam: Syntax {
	Token name;
	Token variance;
	new (
		Location start, Location end
		, Token name
		, Token variance
	)  {
		this.start = start; this.end = end;
		this.name = name;
		this.variance = variance;
	}
}
class Leaf: Syntax {
	LeafName name;
	ListSyntax[GenericParam]? gparams;
	TypeRef? rettype;
	ListSyntax[FuncParam]? params;
	LeafBody body;
	new (
		Location start, Location end
		, LeafName name
		, ListSyntax[GenericParam]? gparams
		, TypeRef? rettype
		, ListSyntax[FuncParam]? params
		, LeafBody body
	)  {
		this.start = start; this.end = end;
		this.name = name;
		this.gparams = gparams;
		this.rettype = rettype;
		this.params = params;
		this.body = body;
	}
}
enum LeafBody {
	FuncCode;
	LeafInitializer;
	ListSyntax[FuncEtter];
}
class LeafInitializer: Syntax {
	Expr expr;
	new (
		Location start, Location end
		, Expr expr
	)  {
		this.start = start; this.end = end;
		this.expr = expr;
	}
}
enum LeafName {
	LeafNameCtor;
	LeafNameOp;
	Token;
}
class LeafNameCtor: Syntax {
	Token? name;
	new (
		Location start, Location end
		, Token? name
	)  {
		this.start = start; this.end = end;
		this.name = name;
	}
}
class LeafNameOp: Syntax {
	Token op;
	Token fixiness;
	new (
		Location start, Location end
		, Token op
		, Token fixiness
	)  {
		this.start = start; this.end = end;
		this.op = op;
		this.fixiness = fixiness;
	}
}
class Module: Syntax {
	Name name;
	ListSyntax[ModuleMember]? members;
	new (
		Location start, Location end
		, Name name
		, ListSyntax[ModuleMember]? members
	)  {
		this.start = start; this.end = end;
		this.name = name;
		this.members = members;
	}
}
enum ModuleMember {
	Module;
	NonModule;
}
class Name: Syntax {
	Name? base;
	ExprName member;
	new (
		Location start, Location end
		, Name? base
		, ExprName member
	)  {
		this.start = start; this.end = end;
		this.base = base;
		this.member = member;
	}
}
enum NonModule {
	ClassDef;
	EnumDef;
	Leaf;
	StmtUse;
}
enum Oper {
	OperNot;
	Token;
}
class OperNot: Syntax {
	Token op;
	Token? neg;
	new (
		Location start, Location end
		, Token op
		, Token? neg
	)  {
		this.start = start; this.end = end;
		this.op = op;
		this.neg = neg;
	}
}
class StmtAssign: Stmt {
	Expr lhs;
	Expr rhs;
	new (
		Location start, Location end
		, Expr lhs
		, Expr rhs
	)  {
		this.start = start; this.end = end;
		this.lhs = lhs;
		this.rhs = rhs;
	}
}
class StmtBlock: Stmt {
	ListSyntax[Stmt] stmts;
	new (
		Location start, Location end
		, ListSyntax[Stmt] stmts
	)  {
		this.start = start; this.end = end;
		this.stmts = stmts;
	}
}
class StmtDo: Stmt {
	StmtBlock block;
	new (
		Location start, Location end
		, StmtBlock block
	)  {
		this.start = start; this.end = end;
		this.block = block;
	}
}
class StmtExpr: Stmt {
	Expr expr;
	new (
		Location start, Location end
		, Expr expr
	)  {
		this.start = start; this.end = end;
		this.expr = expr;
	}
}
class StmtFlow: Stmt {
	Token type;
	new (
		Location start, Location end
		, Token type
	)  {
		this.start = start; this.end = end;
		this.type = type;
	}
}
class StmtFor: Stmt {
	Token ident;
	Expr iterable;
	Stmt body;
	new (
		Location start, Location end
		, Token ident
		, Expr iterable
		, Stmt body
	)  {
		this.start = start; this.end = end;
		this.ident = ident;
		this.iterable = iterable;
		this.body = body;
	}
}
class StmtIf: Stmt {
	Expr cond;
	Stmt thenBody;
	Stmt? elseBody;
	new (
		Location start, Location end
		, Expr cond
		, Stmt thenBody
		, Stmt? elseBody
	)  {
		this.start = start; this.end = end;
		this.cond = cond;
		this.thenBody = thenBody;
		this.elseBody = elseBody;
	}
}
class StmtLoop: Stmt {
	Stmt body;
	new (
		Location start, Location end
		, Stmt body
	)  {
		this.start = start; this.end = end;
		this.body = body;
	}
}
class StmtRet: Stmt {
	Expr? retval;
	new (
		Location start, Location end
		, Expr? retval
	)  {
		this.start = start; this.end = end;
		this.retval = retval;
	}
}
class StmtSwitch: Stmt {
	Expr item;
	ListSyntax[SwitchCase] cases;
	new (
		Location start, Location end
		, Expr item
		, ListSyntax[SwitchCase] cases
	)  {
		this.start = start; this.end = end;
		this.item = item;
		this.cases = cases;
	}
}
class StmtUse: Stmt {
	Name prefix;
	ListSyntax[UseAlias]? aliases;
	new (
		Location start, Location end
		, Name prefix
		, ListSyntax[UseAlias]? aliases
	)  {
		this.start = start; this.end = end;
		this.prefix = prefix;
		this.aliases = aliases;
	}
}
class StmtVar: Stmt {
	Token ident;
	Expr rhs;
	new (
		Location start, Location end
		, Token ident
		, Expr rhs
	)  {
		this.start = start; this.end = end;
		this.ident = ident;
		this.rhs = rhs;
	}
}
class StmtWhile: Stmt {
	Expr cond;
	Stmt body;
	new (
		Location start, Location end
		, Expr cond
		, Stmt body
	)  {
		this.start = start; this.end = end;
		this.cond = cond;
		this.body = body;
	}
}
class SwitchCase: Syntax {
	Expr? cond;
	Stmt body;
	new (
		Location start, Location end
		, Expr? cond
		, Stmt body
	)  {
		this.start = start; this.end = end;
		this.cond = cond;
		this.body = body;
	}
}
class TypeRef: Syntax {
	Syntax name;
	Token? nullable;
	new (
		Location start, Location end
		, Syntax name
		, Token? nullable
	)  {
		this.start = start; this.end = end;
		this.name = name;
		this.nullable = nullable;
	}
}
class UseAlias: Syntax {
	Name imported;
	Token? alias;
	new (
		Location start, Location end
		, Name imported
		, Token? alias
	)  {
		this.start = start; this.end = end;
		this.imported = imported;
		this.alias = alias;
	}
}

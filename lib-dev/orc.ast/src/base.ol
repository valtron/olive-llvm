module orc.ast;

use olive: lang as ol;

class Location {
	int line;
	int col;
	
	new (int line, int col) {
		this.line = line;
		this.col = col;
	}
	
	new Copy(Location loc) {
		this.line = loc.line;
		this.col = loc.col;
	}
}

class Syntax {
	Location start;
	Location end;
}

class Error: Syntax {
	new (Location start, Location end) {
		this.start = start;
		this.end = end;
	}
}

class Token: Syntax {
	int type;
	str data;
	
	new (Location start, Location end, int type, str data) {
		this.start = start;
		this.end = end;
		this.type = type;
		this.data = data;
	}
}

class Expr: Syntax {}

class Stmt: Syntax {}

class ListSyntax[T]: Syntax, ol.Iterable[T] {
	tmpdev.List[T] _impl;
	
	new (Location start, Location end) {
		this.start = start;
		this.end = end;
		this._impl = tmpdev.List[T](1);
	}
	
	new (Location start, Location end, T item) {
		this.start = start;
		this.end = end;
		this._impl = tmpdev.List[T](3);
		this._impl.push(item);
	}
	
	ListSyntax[T] push(Location end, T item) {
		this.end = end;
		this._impl.push(item);
		ret this;
	}
	
	int count -> this._impl.count;
	T this(int idx) -> this._impl(idx);
	void push(T t) { this._impl.push(t); }
	ol.Iterator[T] iter() -> this._impl.iter();
}

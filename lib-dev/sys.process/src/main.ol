module sys.process;

use tmpdev: List;

void
_main_prelude() __link 'sp__main_prelude';

List[str]
_wrap_argv(int argc, olive.lang.Address argv) __link 'sp__wrap_argv';

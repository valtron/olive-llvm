#include <stdio.h>

#include "../../olive.lang/inc/common.h"
#include "../../olive.lang/inc/internal.h"
#include "../../olive.lang/inc/types.h"

OLIVE_TYPE(SP_Console, {});
// TODO: This needs to be initialized
static vtable* SP_Console__vtable;

static SP_Console* _console = 0;

SP_Console* sp__console_get() {
	if (!_console) {
		_console = OL_ALLOC(SP_Console);
		_console->vtbl = SP_Console__vtable;
	}
	return _console;
}

void sp__console_print(SP_Console* self, OL_String* s) {
	fwrite(s->data, 1, s->len, stdout);
}

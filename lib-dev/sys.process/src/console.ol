module sys.process;

Console
_console_get() __link 'sp__console_get';

class Console {
	void print(str s) __link 'sp__console_print';
}

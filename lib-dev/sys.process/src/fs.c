#include <olive.lang/inc/internal.h>
#include <olive.lang/inc/common.h>
#include <tmpdev/inc/fs.h>

// TODO: This needs to be initialized
static vtable* SP_Filesystem__vtable;

static TDEV_Filesystem* _filesystem = 0;

TDEV_Filesystem*
sp__fs_get() {
	if (!_filesystem) {
		_filesystem = OL_ALLOC(TDEV_Filesystem);
		_filesystem->vtbl = SP_Filesystem__vtable;
	}
	return _filesystem;
}

#include <olive.lang/inc/internal.h>
#include <olive.lang/inc/string.h>
#include <olive.lang/inc/types.h>
#include "../../tmpdev/inc/list.h"

TDEV_List* sp__wrap_argv(int argc, char** argv) {
	TDEV_List* l = OL_ALLOC(TDEV_List);
	tmpdev__list_init_empty(l);
	
	for (int i = 1; i < argc; ++i) {
		tmpdev__list_push(l, ol__string_from_cstr(argv[i]));
	}
	
	return l;
}

import argparse

def main(pkgs, libsrc, libout):
	import olive
	from common import Lib
	
	libsrc = Lib(libsrc)
	libout = Lib(libout)
	olive.buildlib(pkgs or libsrc.packages, libsrc, libout)

if __name__ == '__main__':
	p = argparse.ArgumentParser(
		description = "Build olive packages"
	)
	p.add_argument(
		'-l', dest = 'libsrc', default = 'lib-dev',
		help = "source library of packages",
	)
	p.add_argument(
		'-o', dest = 'libout', default = 'lib',
		help = "destination library to install packages in",
	)
	p.add_argument(
		'packages', metavar = 'pkg', type = str, nargs = '*',
		help = "list of packages to build",
	)
	args = p.parse_args()
	
	main(args.packages, args.libsrc, args.libout)

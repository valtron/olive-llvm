from typing import TypeVar, GenericMeta, _type_check, Union

def __getitem__(self, params):
	if not isinstance(params, tuple):
		params = (params,)
	if not params:
		raise TypeError("Cannot have empty parameter list")
	msg = "Parameters to generic types must be types."
	params = tuple(_type_check(p, msg) for p in params)
	if self.__parameters__ is None:
		for p in params:
			if not isinstance(p, TypeVar):
				raise TypeError("Initial parameters must be "
												"type variables; got %s" % p)
		if len(set(params)) != len(params):
			raise TypeError(
				"All type variables in Generic[...] must be distinct.")
	else:
		if len(params) != len(self.__parameters__):
			raise TypeError("Cannot change parameter count from %d to %d" %
											(len(self.__parameters__), len(params)))
		for new, old in zip(params, self.__parameters__):
			if isinstance(old, TypeVar):
				if not old.__constraints__:
					# Substituting for an unconstrained TypeVar is OK.
					continue
				if issubclass(new, Union[old.__constraints__]):
					# Specializing a constrained type variable is OK.
					continue
			if not issubclass(new, old):
				raise TypeError(
					"Cannot substitute %s for %s in %s" %
					(_type_repr(new), _type_repr(old), self))
	dict_copy = dict(self.__dict__)
	if '__slots__' in dict_copy:
		assert type(dict_copy['__slots__']) == tuple
		if dict_copy['__slots__']:
			del dict_copy['__slots__']
	return self.__class__(self.__name__, self.__bases__,
												dict_copy,
												parameters=params,
												origin=self,
												extra=self.__extra__)

GenericMeta.__getitem__ = __getitem__

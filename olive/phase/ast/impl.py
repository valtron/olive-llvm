from typing import List, Optional
from .genast import ModuleMember

class Source:
	def __init__(self) -> None:
		self.members = [] # type: List[ModuleMember]

class AST:
	def __init__(self, sources: Optional[List[Source]] = None) -> None:
		if sources is None:
			sources = []
		self.sources = sources

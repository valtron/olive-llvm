from typing import Optional, Union
from .base import Syntax, Expr, Stmt, Range, ListSyntax
class ClassDef(Syntax):
	__slots__ = ('kind', 'name', 'layout', 'params', 'supers', 'body')
	def __init__(self, range: Range
		, kind: 'Token'
		, name: 'Token'
		, layout: 'Optional[Token]'
		, params: 'Optional[ListSyntax[GenericParam]]'
		, supers: 'Optional[ListSyntax[TypeRef]]'
		, body: 'ListSyntax[NonModule]'
		) -> None:
		super().__init__(range)
		self.kind = kind
		self.name = name
		self.layout = layout
		self.params = params
		self.supers = supers
		self.body = body

class EnumDef(Syntax):
	__slots__ = ('name', 'params', 'body')
	def __init__(self, range: Range
		, name: 'Token'
		, params: 'Optional[ListSyntax[GenericParam]]'
		, body: 'ListSyntax[EnumMember]'
		) -> None:
		super().__init__(range)
		self.name = name
		self.params = params
		self.body = body

EnumMember = Union[
	'NonModule',
	'Token',
	'TypeRef',
]

class ExprAtomic(Expr):
	__slots__ = ('tok',)
	def __init__(self, range: Range
		, tok: 'Token'
		) -> None:
		super().__init__(range)
		self.tok = tok

class ExprBinary(Expr):
	__slots__ = ('op', 'lhs', 'rhs')
	def __init__(self, range: Range
		, op: 'Token'
		, lhs: 'Expr'
		, rhs: 'Expr'
		) -> None:
		super().__init__(range)
		self.op = op
		self.lhs = lhs
		self.rhs = rhs

class ExprCall(Expr):
	__slots__ = ('target', 'args')
	def __init__(self, range: Range
		, target: 'Expr'
		, args: 'ListSyntax[Expr]'
		) -> None:
		super().__init__(range)
		self.target = target
		self.args = args

class ExprDot(Expr):
	__slots__ = ('base', 'member')
	def __init__(self, range: Range
		, base: 'Expr'
		, member: 'ExprName'
		) -> None:
		super().__init__(range)
		self.base = base
		self.member = member

class ExprName(Syntax):
	__slots__ = ('name', 'args')
	def __init__(self, range: Range
		, name: 'Token'
		, args: 'Optional[ListSyntax[TypeRef]]'
		) -> None:
		super().__init__(range)
		self.name = name
		self.args = args

class ExprStmt(Expr):
	__slots__ = ('stmt',)
	def __init__(self, range: Range
		, stmt: 'Stmt'
		) -> None:
		super().__init__(range)
		self.stmt = stmt

class ExprUnary(Expr):
	__slots__ = ('op', 'arg')
	def __init__(self, range: Range
		, op: 'Token'
		, arg: 'Expr'
		) -> None:
		super().__init__(range)
		self.op = op
		self.arg = arg

FuncCode = Union[
	None,
	'Expr',
	'StmtBlock',
	'Token',
]

class FuncEtter(Syntax):
	__slots__ = ('kind', 'body')
	def __init__(self, range: Range
		, kind: 'Token'
		, body: 'FuncCode'
		) -> None:
		super().__init__(range)
		self.kind = kind
		self.body = body

class FuncParam(Syntax):
	__slots__ = ('name', 'type', 'default')
	def __init__(self, range: Range
		, name: 'Token'
		, type: 'TypeRef'
		, default: 'Optional[Expr]'
		) -> None:
		super().__init__(range)
		self.name = name
		self.type = type
		self.default = default

class GenericParam(Syntax):
	__slots__ = ('name', 'variance')
	def __init__(self, range: Range
		, name: 'Token'
		, variance: 'Token'
		) -> None:
		super().__init__(range)
		self.name = name
		self.variance = variance

class Leaf(Syntax):
	__slots__ = ('name', 'gparams', 'rettype', 'params', 'body')
	def __init__(self, range: Range
		, name: 'LeafName'
		, gparams: 'Optional[ListSyntax[GenericParam]]'
		, rettype: 'Optional[TypeRef]'
		, params: 'Optional[ListSyntax[FuncParam]]'
		, body: 'LeafBody'
		) -> None:
		super().__init__(range)
		self.name = name
		self.gparams = gparams
		self.rettype = rettype
		self.params = params
		self.body = body

LeafBody = Union[
	'FuncCode',
	'LeafInitializer',
	'ListSyntax[FuncEtter]',
]

class LeafInitializer(Syntax):
	__slots__ = ('expr',)
	def __init__(self, range: Range
		, expr: 'Expr'
		) -> None:
		super().__init__(range)
		self.expr = expr

LeafName = Union[
	'LeafNameCtor',
	'LeafNameOp',
	'Token',
]

class LeafNameCtor(Syntax):
	__slots__ = ('name',)
	def __init__(self, range: Range
		, name: 'Optional[Token]'
		) -> None:
		super().__init__(range)
		self.name = name

class LeafNameOp(Syntax):
	__slots__ = ('op', 'fixiness')
	def __init__(self, range: Range
		, op: 'Token'
		, fixiness: 'Token'
		) -> None:
		super().__init__(range)
		self.op = op
		self.fixiness = fixiness

class Module(Syntax):
	__slots__ = ('name', 'members')
	def __init__(self, range: Range
		, name: 'Name'
		, members: 'Optional[ListSyntax[ModuleMember]]'
		) -> None:
		super().__init__(range)
		self.name = name
		self.members = members

ModuleMember = Union[
	'Module',
	'NonModule',
]

class Name(Syntax):
	__slots__ = ('base', 'member')
	def __init__(self, range: Range
		, base: 'Optional[Name]'
		, member: 'ExprName'
		) -> None:
		super().__init__(range)
		self.base = base
		self.member = member

NonModule = Union[
	'ClassDef',
	'EnumDef',
	'Leaf',
	'StmtUse',
]

Oper = Union[
	'OperNot',
	'Token',
]

class OperNot(Syntax):
	__slots__ = ('op', 'neg')
	def __init__(self, range: Range
		, op: 'Token'
		, neg: 'Optional[Token]'
		) -> None:
		super().__init__(range)
		self.op = op
		self.neg = neg

class StmtAssign(Stmt):
	__slots__ = ('lhs', 'rhs')
	def __init__(self, range: Range
		, lhs: 'Expr'
		, rhs: 'Expr'
		) -> None:
		super().__init__(range)
		self.lhs = lhs
		self.rhs = rhs

class StmtBlock(Stmt):
	__slots__ = ('stmts',)
	def __init__(self, range: Range
		, stmts: 'ListSyntax[Stmt]'
		) -> None:
		super().__init__(range)
		self.stmts = stmts

class StmtDo(Stmt):
	__slots__ = ('block',)
	def __init__(self, range: Range
		, block: 'StmtBlock'
		) -> None:
		super().__init__(range)
		self.block = block

class StmtExpr(Stmt):
	__slots__ = ('expr',)
	def __init__(self, range: Range
		, expr: 'Expr'
		) -> None:
		super().__init__(range)
		self.expr = expr

class StmtFlow(Stmt):
	__slots__ = ('type',)
	def __init__(self, range: Range
		, type: 'Token'
		) -> None:
		super().__init__(range)
		self.type = type

class StmtFor(Stmt):
	__slots__ = ('ident', 'iterable', 'body')
	def __init__(self, range: Range
		, ident: 'Token'
		, iterable: 'Expr'
		, body: 'Stmt'
		) -> None:
		super().__init__(range)
		self.ident = ident
		self.iterable = iterable
		self.body = body

class StmtIf(Stmt):
	__slots__ = ('cond', 'thenBody', 'elseBody')
	def __init__(self, range: Range
		, cond: 'Expr'
		, thenBody: 'Stmt'
		, elseBody: 'Optional[Stmt]'
		) -> None:
		super().__init__(range)
		self.cond = cond
		self.thenBody = thenBody
		self.elseBody = elseBody

class StmtLoop(Stmt):
	__slots__ = ('body',)
	def __init__(self, range: Range
		, body: 'Stmt'
		) -> None:
		super().__init__(range)
		self.body = body

class StmtRet(Stmt):
	__slots__ = ('retval',)
	def __init__(self, range: Range
		, retval: 'Optional[Expr]'
		) -> None:
		super().__init__(range)
		self.retval = retval

class StmtSwitch(Stmt):
	__slots__ = ('item', 'cases')
	def __init__(self, range: Range
		, item: 'Expr'
		, cases: 'ListSyntax[SwitchCase]'
		) -> None:
		super().__init__(range)
		self.item = item
		self.cases = cases

class StmtUse(Stmt):
	__slots__ = ('prefix', 'aliases')
	def __init__(self, range: Range
		, prefix: 'Name'
		, aliases: 'Optional[ListSyntax[UseAlias]]'
		) -> None:
		super().__init__(range)
		self.prefix = prefix
		self.aliases = aliases

class StmtVar(Stmt):
	__slots__ = ('ident', 'rhs')
	def __init__(self, range: Range
		, ident: 'Token'
		, rhs: 'Expr'
		) -> None:
		super().__init__(range)
		self.ident = ident
		self.rhs = rhs

class StmtWhile(Stmt):
	__slots__ = ('cond', 'body')
	def __init__(self, range: Range
		, cond: 'Expr'
		, body: 'Stmt'
		) -> None:
		super().__init__(range)
		self.cond = cond
		self.body = body

class SwitchCase(Syntax):
	__slots__ = ('cond', 'body')
	def __init__(self, range: Range
		, cond: 'Optional[Expr]'
		, body: 'Stmt'
		) -> None:
		super().__init__(range)
		self.cond = cond
		self.body = body

class TypeRef(Syntax):
	__slots__ = ('name', 'nullable')
	def __init__(self, range: Range
		, name: 'Syntax'
		, nullable: 'Optional[Token]'
		) -> None:
		super().__init__(range)
		self.name = name
		self.nullable = nullable

class UseAlias(Syntax):
	__slots__ = ('imported', 'alias')
	def __init__(self, range: Range
		, imported: 'Name'
		, alias: 'Optional[Token]'
		) -> None:
		super().__init__(range)
		self.imported = imported
		self.alias = alias


from abc import ABCMeta
from typing import Generic, TypeVar, Iterator, Optional

class Location:
	__slots__ = ('line', 'col')
	
	def __init__(self, line: int, col: int) -> None:
		self.line = line
		self.col = col
	
	def clone(self) -> 'Location':
		return Location(self.line, self.col)
	
	def __eq__(self, other):
		if type(self) != type(other): return False
		return (self.line, self.col) == (other.line, other.col)

class Range:
	__slots__ = ('start', 'end')
	
	def __init__(self, start: Location, end: Location) -> None:
		self.start = start
		self.end = end
	
	def __eq__(self, other):
		if type(self) != type(other): return False
		return (self.start, self.end) == (other.start, other.end)

class Syntax(metaclass = ABCMeta):
	__slots__ = ('range',)
	
	def __init__(self, range: Range) -> None:
		self.range = range

class Token(Syntax):
	__slots__ = ('type', 'data')
	
	def __init__(self, range: Range, type: int, data: str) -> None:
		super().__init__(range)
		self.type = type
		self.data = data
	
	def __eq__(self, other):
		if type(self) != type(other): return False
		return (self.range, self.type, self.data) == (other.range, other.type, other.data)
	
	def __repr__(self):
		return '<token {} {!r}>'.format(self.type, self.data)

class Expr(Syntax, metaclass = ABCMeta):
	pass

class Stmt(Syntax, metaclass = ABCMeta):
	pass

T = TypeVar('T')
class ListSyntax(Generic[T], Syntax):
	__slots__ = ('_impl',)
	
	def __init__(self, range: Range, item: Optional[T] = None) -> None:
		super().__init__(range)
		self._impl = [] # type: List[T]
		if item is not None:
			self._impl.append(item)
	
	def push(self, item: T, end: Optional[Location] = None) -> None:
		self._impl.append(item)
		if end is not None:
			self.range.end = end
		return self
	
	def __len__(self):
		return len(self._impl)
	
	def __getitem__(self, idx: int) -> T:
		return self._impl[idx]
	
	def __iter__(self) -> Iterator[T]:
		return iter(self._impl)

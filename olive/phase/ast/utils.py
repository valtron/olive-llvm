from .base import Syntax, ListSyntax, Token

def to_pod(stx: Syntax) -> object:
	if isinstance(stx, (ListSyntax, list)):
		return tuple(to_pod(x) for x in stx)
	if isinstance(stx, Token):
		return stx.data
	
	t = type(stx)
	fields = []
	for s in t.__slots__:
		f = getattr(stx, s)
		if isinstance(f, Syntax):
			f = to_pod(f)
		fields.append(f)
	
	return (type(stx).__name__,) + tuple(fields)

Amp = 49
Arrow = 33
At = 31
BCL = 24
BCR = 25
BRL = 20
BRR = 21
BSL = 22
BSR = 23
BSlash = 44
BSlashEq = 64
Bar = 48
CML = 4
CSL = 3
Colon = 28
Comma = 30
Dot = 26
EOF = 0
ERR = 1
Eqs = 32
Excl = 47
FSlash = 43
FSlashEq = 63
GT = 53
GTE = 54
Hat = 45
HatEq = 65
Id = 10
KAnd = 100
KAny = 101
KAs = 102
KAsync = 103
KAwait = 104
KBreak = 105
KBuiltin = 137
KCatch = 106
KClass = 107
KDo = 108
KElse = 109
KEnum = 110
KError = 111
KFinally = 112
KFor = 113
KGet = 114
KIf = 115
KIn = 116
KIs = 117
KLink = 138
KLoop = 118
KModule = 119
KMy = 120
KNew = 121
KNext = 122
KNot = 123
KObject = 124
KOr = 125
KOut = 126
KRet = 127
KRole = 128
KSet = 129
KSuper = 130
KSwitch = 131
KThis = 132
KUse = 133
KValue = 134
KWhile = 135
KYield = 136
LT = 51
LTE = 52
Minus = 41
MinusEq = 61
Neq = 55
Num = 12
Perc = 50
Plus = 40
PlusEq = 60
Que = 27
QueQue = 58
Semi = 29
Shl = 56
ShlEq = 67
Shr = 57
ShrEq = 68
Str = 11
Sym = 13
Tilde = 46
TildeEq = 66
Times = 42
TimesEq = 62
WS = 2
KEYWORDS = {
	'and': 100,
	'any': 101,
	'as': 102,
	'async': 103,
	'await': 104,
	'break': 105,
	'__builtin': 137,
	'catch': 106,
	'class': 107,
	'do': 108,
	'else': 109,
	'enum': 110,
	'error': 111,
	'finally': 112,
	'for': 113,
	'get': 114,
	'if': 115,
	'in': 116,
	'is': 117,
	'__link': 138,
	'loop': 118,
	'module': 119,
	'my': 120,
	'new': 121,
	'next': 122,
	'not': 123,
	'object': 124,
	'or': 125,
	'out': 126,
	'ret': 127,
	'role': 128,
	'set': 129,
	'super': 130,
	'switch': 131,
	'this': 132,
	'use': 133,
	'value': 134,
	'while': 135,
	'yield': 136,
}

from abc import ABCMeta, abstractproperty
from typing import Optional, List, Union
from enum import Enum
from olive.utils import TODO, cached_property

TypeDef = Union['ClassType', 'EnumType']
PackageItem = Union['Module', 'Func', 'GlobalVar', TypeDef]

class Package:
	def __init__(self, name: str) -> None:
		self.name = name
		self.deps = [] # type: List[Package]
		self.items = [] # type: List[PackageItem]
		self.usedSymbols = set() # type: Set[SymbolType]

class Stmt(metaclass = ABCMeta):
	pass

class FuncDef:
	def __init__(self, func: 'Func', code: Stmt) -> None:
		self.func = func
		self.code = code

class Module:
	def __init__(self, name: str, parent: Optional['Module'] = None) -> None:
		self.name = name
		self.parent = parent
	
	@cached_property
	def fullName(self) -> str:
		return _calcFullName(self.parent, self.name)
	
	def __repr__(self) -> str:
		return self.fullName

class FuncKind(Enum):
	Func = 1
	Method = 2
	Ctor = 3

class Layout(Enum):
	# TODO: How to function pointers?
	
	Addr = 1
	Ref = 2
	Bit0 = 3
	Bit1 = 4
	Bit8 = 5
	Bit16 = 6
	Bit32 = 7
	Bit64 = 8

class Type(metaclass = ABCMeta):
	pass

class Form:
	def __init__(self, type: Type, layout: Layout, checked: bool = True) -> None:
		self.type = type
		self.layout = layout
		self.checked = checked

class Expr(metaclass = ABCMeta):
	def __init__(self, form: Form) -> None:
		self.form = form

class _VarExpr(Expr, metaclass = ABCMeta):
	def __init__(self, form: Form, name: str) -> None:
		super().__init__(form)
		self.name = name

class LocalVar(_VarExpr):
	pass

class Param(_VarExpr):
	def __init__(self, form: Form, name: str, default: Optional['Func'] = None) -> None:
		super().__init__(form, name)
		self.default = default

class Field:
	def __init__(self, owner: 'ClassType', index: int, name: str, form: Form) -> None:
		self.owner = owner
		self.index = index
		self.name = name
		self.form = form

class FuncSig:
	def __init__(self, ret: Form, this: Optional[Param], params: Optional[List[Param]], value: Optional[Param]) -> None:
		self.ret = ret
		self.this = this
		self.params = params
		self.value = value

class Variance(Enum):
	In = 1
	Out = 2
	Both = 3

class TypeParam(Type):
	def __init__(self, name: str, var: Variance) -> None:
		self.name = name
		self.var = var
	
	def __repr__(self) -> str:
		return self.name

Owner = Union[Module, 'ClassType', 'EnumType', None]

class Func:
	def __init__(self,
		parent: Owner, name: str, *,
		sig: FuncSig, kind: FuncKind, proto: Optional['Func'], isAbstract: bool) -> None:
	#
		self.parent = parent
		self.name = name
		self.sig = sig
		self.kind = kind
		self.isAbstract = isAbstract
		
		self.proto = proto
		self.genParams = [] # type: List[TypeParam]
		self.genArgs = [] # type: List[Type]
	
	@cached_property
	def id(self) -> str:
		raise TODO("impl")

class ClassKind(Enum):
	Class = 1
	Role = 2
	Object = 3
	Error = 4

class _ClassOrSymbolType(Type):
	def __init__(self, name: str) -> None:
		self.name = name
		self.supers = [] # type: List[ClassType]
		self.vtable = [] # type: List[VtableFragment]
		self.fields = [] # type: List[Field]
		self.methods = [] # type: List[Func]
	
	@abstractproperty
	def isAbstract(self) -> bool: pass
	
	@abstractproperty
	def fullName(self) -> str: pass
	
	def __repr__(self) -> str:
		return self.fullName

class VtableFragment:
	def __init__(self, type: _ClassOrSymbolType, offset: int) -> None:
		self.type = type
		self.offset = offset
		self.methods = [] # type: List[Func]

class ClassType(_ClassOrSymbolType):
	def __init__(self,
		parent: Owner, name: str, *,
		proto: Optional['ClassType'], layout: Layout, kind: ClassKind) -> None:
	#
		super().__init__(name)
		self.parent = parent
		self.layout = layout
		self.kind = kind
		
		self.proto = proto
		self.genParams = [] # type: List[TypeParam]
		self.genArgs = [] # type: List[Type]
	
	@property
	def staticLayout(self):
		return self.layout
	
	@cached_property
	def fullName(self) -> str:
		return _calcFullName(self.parent, self.name)
	
	@cached_property
	def isAbstract(self) -> bool:
		# TODO: Also check if it contains any abstract methods
		return self.kind != ClassKind.Role

class EnumType(Type):
	def __init__(self, parent: Owner, name: str, *, proto: Optional['EnumType']) -> None:
		self.parent = parent
		self.name = name
		self.options = [] # type: List[Type]
		
		self.proto = proto
		self.genParams = [] # type: List[TypeParam]
		self.genArgs = [] # type: List[Type]
	
	@cached_property
	def staticLayout(self):
		opts = self.options
		if len(opts) == 0:
			return Layout.Bit0
		if len(opts) == 1:
			return opts[0].staticLayout
		if len(opts) > 2:
			return Layout.Ref
		if opts[0].staticLayout == opts[1].staticLayout == Layout.Bit0:
			return Layout.Bit1
		return Layout.Ref
	
	@cached_property
	def fullName(self) -> str:
		return _calcFullName(self.parent, self.name)
	
	def __repr__(self) -> str:
		return self.fullName

class SymbolType(_ClassOrSymbolType):
	def __init__(self, name: str) -> None:
		super().__init__(name)
		self.layout = Layout.Bit0
		self.kind = ClassKind.Object
	
	@property
	def staticLayout(self):
		return self.layout
	
	@cached_property
	def fullName(self) -> str:
		return '#' + self.name
	
	@cached_property
	def isAbstract(self) -> bool:
		return False

class FuncType(Type):
	def __init__(self, retType: Type, paramTypes: List[Type]) -> None:
		self.retType = retType
		self.paramTypes = paramTypes
	
	def __repr__(self) -> str:
		return 'fn[{!r} -> {!r}]'.format(self.paramTypes, self.retType)

class GlobalVar(_VarExpr):
	def __init__(self, form: Form, name: str, *, parent: Owner = None, init: Optional[Func] = None) -> None:
		super().__init__(form, name)
		self.parent = parent
		self.init = init
	
	@cached_property
	def fullName(self) -> str:
		return _calcFullName(self.parent, self.name)
	
	def __repr__(self) -> str:
		return self.fullName

class BlockStmt(Stmt):
	def __init__(self, stmts: List[Stmt]) -> None:
		self.stmts = stmts

class RetStmt(Stmt):
	def __init__(self, value: Optional[Expr]) -> None:
		self.value = value

class LoopStmt(Stmt):
	def __init__(self, body: Stmt) -> None:
		self.body = body

class _LoopCtrlStmt(Stmt):
	def __init__(self, loop: LoopStmt) -> None:
		self.loop = loop

class BreakStmt(_LoopCtrlStmt): pass
class NextStmt(_LoopCtrlStmt): pass

class IfStmt(Stmt):
	def __init__(self, cond: Expr, thenBody: Stmt, elseBody: Optional[Stmt]) -> None:
		self.cond = cond
		self.thenBody = thenBody
		self.elseBody = elseBody

class AssignStmt(Stmt):
	def __init__(self, var: _VarExpr, rhs: Expr) -> None:
		self.var = var
		self.rhs = rhs

class CallArgs:
	def __init__(self, *,
		this: Optional[Expr] = None,
		args: Optional[List[Expr]] = None,
		value: Optional[Expr] = None) -> None:
	#
		self.this = this
		self.args = args
		self.value = value

class CallExpr(Expr):
	def __init__(self, func: Func, args: CallArgs) -> None:
		super().__init__(func.sig.ret)
		self.func = func
		self.args = args

class CallStmt(Stmt):
	def __init__(self, expr: CallExpr) -> None:
		self.expr = expr

class FieldSetStmt(Stmt):
	def __init__(self, obj: Expr, field: Field, rhs: Expr) -> None:
		self.obj = obj
		self.field = field
		self.rhs = rhs

class StmtExpr(Expr):
	def __init__(self, form: Form, stmt: Stmt) -> None:
		super().__init__(form)
		self.stmt = stmt

class LocalRetStmt(Stmt):
	def __init__(self, context: StmtExpr, value: Expr) -> None:
		self.context = context
		self.value = value

class FieldGetExpr(Expr):
	def __init__(self, obj: Expr, field: Field) -> None:
		super().__init__(field.form)
		self.obj = obj
		self.field = field

class NewExpr(Expr):
	def __init__(self, type: ClassType) -> None:
		assert type.layout == Layout.Ref
		super().__init__(Form(type, type.layout, True))

class BoolValue(Expr):
	def __init__(self, boolForm: Form) -> None:
		super().__init__(boolForm)

class IsaExpr(BoolValue):
	def __init__(self, boolForm: Form, lhs: Expr, rhs: Type) -> None:
		super().__init__(boolForm)
		self.lhs = lhs
		self.rhs = rhs

class IsExpr(BoolValue):
	def __init__(self, boolForm: Form, lhs: Expr, rhs: Expr) -> None:
		super().__init__(boolForm)
		self.lhs = lhs
		self.rhs = rhs

class NotExpr(BoolValue):
	def __init__(self, boolForm: Form, arg: Expr) -> None:
		super().__init__(boolForm)
		self.arg = arg

class BoolCastExpr(BoolValue):
	def __init__(self, boolForm: Form, arg: Expr) -> None:
		super().__init__(boolForm)
		self.arg = arg

class BoolConst(BoolValue):
	def __init__(self, boolForm: Form, data: bool) -> None:
		super().__init__(boolForm)
		self.data = data

class NumConst(Expr):
	def __init__(self, intForm: Form, data: int) -> None:
		super().__init__(intForm)
		self.data = data

class StrConst(Expr):
	def __init__(self, strForm: Form, data: bytes) -> None:
		super().__init__(strForm)
		self.data = data

class SymConst(Expr):
	def __init__(self, symForm: Form, data: str) -> None:
		super().__init__(symForm)
		self.data = data

class Relayout(Expr):
	def __init__(self, targetForm: Form, arg: Expr) -> None:
		super().__init__(targetForm)
		self.arg = arg

class CompilationUnit:
	def __init__(self, target: Package) -> None:
		self.target = target
		self.packs = [] # type: List[Package]
		self.items = [] # type: List[PackageItem]
		
		self._knownForms = {} # type: Dict[str, Form]
		self._usedSymbolTypes = {} # type: Dict[str, SymbolType]
		
		self._unitForm = None
	
	def getTypeForm(self) -> Form:
		return self._getForm('Type')
	
	def getIntForm(self) -> Form:
		return self._getForm('Int')
	
	def getStringForm(self) -> Form:
		return self._getForm('String')
	
	def getBoolForm(self) -> Form:
		return self._getForm('Bool')
	
	def getAnyForm(self) -> Form:
		return self._getForm('Any')
	
	def getUnitForm(self) -> Form:
		if self._unitForm is None:
			t = EnumType(None, 'Unit', proto = None)
			t.options.append(self.getSymbolType('null'))
			self.items.append(t)
			self._unitForm = Form(t, Layout.Bit0, True)
		return self._unitForm
	
	def getSymbolType(self, name: str) -> SymbolType:
		if name not in self._usedSymbolTypes:
			st = SymbolType(name)
			self._usedSymbolTypes[name] = st
		return self._usedSymbolTypes[name]
	
	def _getForm(self, name: str) -> Form:
		if name not in self._knownForms:
			if name == 'Any':
				t = self._getTypeInPackage('olive.lang', 'Object')
				checked = False
			else:
				t = self._getTypeInPackage('olive.lang', name)
				checked = True
			self._knownForms[name] = Form(t, t.staticLayout, checked)
		return self._knownForms[name]
	
	def _getTypeInPackage(self, pkgname: str, typename: str) -> TypeDef:
		pkg = self._getPackage(pkgname)
		for item in pkg.items:
			if isinstance(item, ClassType):
				if item.name == typename: return item
			elif isinstance(item, EnumType):
				if item.name == typename: return item
		raise TODO("can't find type '{}' in '{}'", typename, pkgname)
	
	def _getPackage(self, pkgname: str) -> Package:
		if self.target.name == pkgname:
			return self.target
		for p in self.packs:
			if p.name == pkgname:
				return p
		raise TODO("can't find package '{}'", pkgname)
	
	def getModule(self, parent: Optional[Module], name: str) -> Module:
		m = None # type: Optional[Module]
		found = False
		for m in self.items:
			if not isinstance(m, Module): continue
			if m.parent is parent and m.name == name:
				found = True
				break
		if not found:
			m = None
		if m is None:
			m = Module(name, parent)
			self.items.append(m)
		return m
	
	def add(self, item: PackageItem) -> None:
		# Modules shouldn't be put into Package.items
		assert not isinstance(item, Module)
		self.items.append(item)
		self.target.items.append(item)

def _calcFullName(owner: Owner, name: str) -> str:
	if owner is None: return name
	return owner.fullName + '.' + name

def isSubtype(a: Type, b: Type) -> bool:
	raise TODO("impl")

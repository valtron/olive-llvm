class Lib:
	def __init__(self, base: str) -> None:
		self.base = base
	
	def metafile(self, package: str) -> str:
		return '{}/{}/obj/meta.json'.format(self.base, package)

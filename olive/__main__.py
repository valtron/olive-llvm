from typing import List
import argparse

def main(*args: str) -> int:
	parser = argparse.ArgumentParser(prog = 'olive')
	parser.add_argument('-m', dest = 'mode', choices = ['package', 'exe'],
		help = """
			Mode.
			'package': compile to library, $base/obj/_ol.ll.
			'exe': compile executable, $name
		""",
		required = True,
	)
	parser.add_argument('-l', dest = 'libdir',
		help = "Use $libdir as library",
	)
	parser.add_argument('-d', dest = 'deps', default = [], nargs = '*', metavar = 'DEP',
		help = "Dependencies to use",
	)
	parser.add_argument('-o', dest = 'outdir',
		help = "Output directory",
	)
	parser.add_argument('-n', dest = 'name',
		help = "Name of package/exe",
	)
	parser.add_argument('base',
		help = "Base dir",
	)
	parsed = parser.parse_args(args)
	
	from olive.driver import Driver, CompileMode
	driver = Driver(
		mode = (CompileMode.Exe if parsed.mode == 'exe' else CompileMode.Package),
		lib = parsed.libdir, base = parsed.base,
		out = parsed.outdir, name = parsed.name,
	)
	success = driver.run()
	return (0 if success else -1)

if __name__ == '__main__':
	import sys
	sys.exit(main(*sys.argv[1:]))

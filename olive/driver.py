from enum import Enum
from typing import Optional
from olive.utils import Messages, Filesystem
from olive.phase import ast, sem, lib
from olive.trans import parse, analyze, meta, codegen

# +-----+  +-----+            +-----+
# | src |  | Lib +------------> exe |
# +--+--+  +--+--+            +--^--+
#    |        |                  |
# +--v--+  +--v--+  +------+  +--+--+
# | AST +--> Sem +--> LLVM +--> obj |
# +-----+  +--+--+  +------+  +-----+
#             |
#             |     +------+
#             +-----> meta |
#                   +------+
# Nodes are 'phases', (sets of) arrows are 'transforms'.
# Transforms:
# 	parse     :: src  -> AST
# 	analyze   :: AST  -> Sem
# 	meta/load :: Lib  -> Sem
# 	meta/save :: Sem  -> meta
# 	codegen   :: Sem  -> LLVM
# 	compile   :: LLVM -> obj
# 	link      :: (Lib, obj) -> exe

class CompileMode(Enum):
	Package = 1
	Exe = 2

class Driver:
	def __init__(self,
		mode: CompileMode, lib: Optional[str], base: Optional[str],
		out: Optional[str], name: Optional[str],
	) -> None:
		self.msg = Messages()
		self.mode = mode
		self.lib = 'lib' if lib is None else lib
		self.base = Filesystem.GetCurrentDir() if base is None else base
		
		if out is None:
			src = self.base
			if self.mode == CompileMode.Exe:
				if Filesystem.IsDir(src):
					self.out = src + '/' + 'a.ll'
				else:
					self.out = src[:-2] + 'll'
			else:
				if name is not None:
					self.base += '/' + name
				self.out = self.base + '/obj'
		else:
			self.out = out
		
		self.name = self.out if name is None else name
		
		self.deps = set() # type: Set[str]
		if self.mode == CompileMode.Exe:
			self.deps.add('sys.process')
	
	def run(self) -> bool:
		if self.mode == CompileMode.Exe:
			Filesystem.MakeDir(self.out)
		
		ast = ast.AST()
		
		# parse: src -> AST
		parser = parse.Parser(self.msg)
		files = [] # type: List[str]
		src = self.base
		
		if Filesystem.IsDir(src):
			files = list(Filesystem.FindFilesWithExtension(src, 'ol'))
		else:
			files.append(src)
			src = Filesystem.DirName(src)
		
		for file in files:
			print("Parsing:", file[len(src)+1:])
			parser.parse(file, ast)
		
		if self.msg.hasErrors:
			print("Aborting:", "parse errors")
			return False
		
		cu = sem.CompilationUnit(sem.Package(self.name))
		lib = lib.Library(self.lib)
		meta = meta.Meta(self.msg, lib, cu)
		
		# meta/load: Lib -> Sem
		for dep in self.deps:
			meta.load(dep)
		
		if self.msg.hasErrors:
			print("Aborting:", "meta/load errors")
			return False
		
		# analyze: AST -> Sem
		sproc = analyze.SyntaxProcessor(self.msg, cu)
		sproc.process(ast)
		
		if self.msg.hasErrors:
			print("Aborting:", "analyze errors")
		
		outdir = self.out
		llout = self.out
		
		if self.mode == CompileMode.Exe:
			outdir = Filesystem.DirName(outdir)
		else:
			llout = llout + '/a.ll'
		
		# codegen: Sem -> LLVM
		compiler = codegen.Compiler(self.msg, cu);
		compiler.run();
		if self.mode == CompileMode.Exe:
			compiler.createMain()
		compiler.saveIR(llout)
		
		# meta/save: Sem -> meta
		if self.mode == CompileMode.Package:
			meta.save(outdir + '/meta.json')
		
		return True

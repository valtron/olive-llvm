from typing import TextIO, Union
from olive.phase import ast

class charclass:
	@staticmethod
	def a(c):
		return c.isalpha() or c == '_'
	
	@staticmethod
	def n(c):
		return c.isdigit()
	
	@staticmethod
	def an(c):
		return c.isalnum() or c == '_'
	
	@staticmethod
	def ws(c):
		return c.isspace()
	
	@staticmethod
	def nnl(c):
		return c and c != '\n'

class FileWindow:
	def __init__(self, fh: TextIO) -> None:
		self.head = -1
		self.start = ast.Location(0, 0)
		self.end = ast.Location(0, -1)
		self.fh = fh
		self.fileDone = False
		self.i = 0
		self.j = -1
		self.k = 0
		self.buf = ''
		self.c = None
	
	def flush(self, tt: int) -> ast.Token:
		span = self.buf[self.i:self.i+self.j]
		token = ast.Token(ast.Range(self.start, self.end), tt, span)
		
		self.i += self.j
		self.k += self.j
		self.j = 0
		self.start = self.end
		self.end = self.start.clone()
		
		return token
	
	@property
	def needsFlush(self) -> bool:
		return self.j > 0
	
	def step(self) -> str:
		if self.c == NEWLINE:
			self.end.line += 1
			self.end.col = 0
		else:
			self.end.col += 1
		c = self._stepAux()
		self.c = c
		return c
	
	def _stepAux(self) -> str:
		self.j += 1
		k = self.i + self.j
		if k < len(self.buf):
			return self.buf[k]
		if self.fileDone:
			return NULL
		read = self.fh.read(100)
		if len(read) < 100:
			self.fileDone = True
		if read:
			if self.i > 100:
				self.buf = self.buf[self.i:]
				self.i = 0
			self.buf += read
			return self.buf[self.i + self.j]
		return NULL

NEWLINE = '\n'
NULL = '\x00'

class Reduce:
	def __init__(self, headId: int, reductionId: int, count: int) -> None:
		self.headId = headId
		self.reductionId = reductionId
		self.count = count

class Shift:
	def __init__(self, sid: int) -> None:
		self.sid = sid

@object.__new__
class Accept: pass

Action = Union[Shift, Reduce, type(Accept)]

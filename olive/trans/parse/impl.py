from typing import Optional, TextIO
from olive.utils import Messages, TODO
from olive.phase import ast
from olive.phase.ast import gentokens as tokens

from . import genlexer, genparser
from .base import FileWindow, Shift, Reduce, Accept

class Parser:
	def __init__(self, msg: Messages) -> None:
		self.msg = msg
	
	def parse(self, file: TextIO, tree: ast.Source) -> None:
		fw = FileWindow(file)
		lexer = Lexer(fw)
		stack = [] # type: List[ParserState]
		
		loc0 = ast.Location(0, 0)
		range0 = ast.Range(loc0, loc0)
		
		stack.append(ParserState(0, None, loc0))
		S = [] # type: List[ast.Syntax]
		stop = False
		
		ACTION = genparser.ACTIONS
		REDUCE = genparser.REDUCE
		GOTO = genparser.GOTO
		
		while lexer.moveNext():
			tok = lexer.current
			
			while True:
				state = stack[-1]
				
				a = ACTION.get(state.id - 500)
				if a is None:
					a = ACTION.get(state.id * 199 + tok.type)
				
				if isinstance(a, Reduce):
					S.clear()
					h = len(stack)
					i = h - a.count
					while i < h:
						S.append(stack[i].syntax)
						i += 1
					if a.count > 0:
						del stack[-a.count:]
					state = stack[-1]
					start = state.loc
					obj = REDUCE[a.reductionId](S, ast.Range(start, state.loc))
					stack.append(ParserState(GOTO[state.id * 199 + a.headId], obj, start))
					continue
				
				if isinstance(a, Shift):
					stack.append(ParserState(a.sid, tok, tok.range.end))
					break
				
				if a is Accept:
					tree.members.extend(state.syntax)
					return
				
				stop = True
				break
			
			if stop:
				break
		
		loc = stack[-1].loc
		self.msg.emit("parse error around {}:{}", loc.line, loc.col)

class ParserState:
	def __init__(self, id: int, syntax: ast.Syntax, loc: ast.Location) -> None:
		self.id = id
		self.syntax = syntax
		self.loc = loc

class Lexer:
	def __init__(self, fw: FileWindow) -> None:
		self.impl = genlexer.RawLexer(fw)
	
	def moveNext(self) -> bool:
		while self.impl.moveNext():
			t = self.impl.current
			tt = t.type
			if tt == tokens.WS:
				continue
			if tt == tokens.CSL:
				continue
			if tt == tokens.CML:
				continue
			if tt == tokens.Id:
				type = tokens.KEYWORDS.get(t.data)
				if type is not None:
					t.type = type
			return True
		return False
	
	@property
	def current(self) -> ast.Token:
		token = self.impl.current
		assert token is not None
		return token

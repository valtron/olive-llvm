from abc import ABCMeta, abstractmethod
from typing import Optional, Union, List, Dict, Tuple
from olive.utils import Messages, TODO, Naming
from olive.phase import sem, ast

from .scope import Scope, SingleScope, SideScope, ChildScope
from .common import Expr, FuncGroup, Error

class SyntaxProcessor:
	def __init__(self, msg: Messages, cu: sem.CompilationUnit, *, suppress_predefines: bool = False) -> None:
		self.msg = msg
		self.cu = cu
		self.suppress_predefines = suppress_predefines
		
		self._item_dicts = {} # type: Dict[_Container, Dict[str, Expr]]
		self._module_pop_info = [] # type: List[ModulePopInfo]
		self._containers = [] # type: List[_Container]
		self._ast2sem = {} # type: Dict[ast.Syntax, _Stage2Item]
		self._scopes = [] # type: List[SideScope[Expr]]
	
	def process(self, tree: ast.AST) -> None:
		# 0. Set up top-level modules for the package
		m = None
		for p in self.cu.target.name.split('.'):
			m = self.cu.getModule(m, p)
			self._containers.append(m)
		
		# 1. Shallow conversion of items `sem` -> `ast`
		self._walkTree(tree, Stage1(self, self.cu))
		
		# 2. Gather names visible to compilation unit
		
		# 2.1. Predefines
		predefines = SingleScope()
		if not self.suppress_predefines:
			predefines.add('int', self.cu.getIntForm())
			predefines.add('bool', self.cu.getBoolForm())
			predefines.add('void', self.cu.getUnitForm())
			predefines.add('str', self.cu.getStringForm())
		
		# 2.x. Set up scopes
		top = None # type: Scope[Expr]
		for c in self._containers:
			top = ChildScope(top, self._getItemDict(c))
		
		self._scopes.append(SideScope(predefines, top))
		
		# 2.2. Convert each source to `sem`
		self._walkTree(tree, Stage2(self, self.cu))
	
	def _walkTree(self, tree: ast.AST, walker: 'Walker') -> None:
		for source in tree.sources:
			for mm in source.members:
				self._walk(mm, walker)
			self._popModule(stub = True, scope = walker.needs_scope)
	
	def _walk(self, s: ast.Syntax, walker: 'Walker') -> None:
		if isinstance(s, ast.Module):
			members = s.members
			scope = walker.needs_scope
			is_stub = (members is None)
			if is_stub:
				self._popModule(stub = True, scope = scope)
			self._pushModule(s.name, stub = is_stub, scope = scope)
			if members is not None:
				for m in members:
					self._walk(m, walker)
				self._popModule(stub = False, scope = scope)
			return
		
		if isinstance(s, ast.EnumDef):
			walker.on_EnumDef(s)
			self._containers.append(self._ast2sem[s])
			self._walk(s.body, walker)
			self._containers.pop()
			return
		
		if isinstance(s, ast.ClassDef):
			walker.on_ClassDef(s)
			self._containers.append(self._ast2sem[s])
			self._walk(s.body, walker)
			self._containers.pop()
			return
		
		if isinstance(s, ast.Leaf):
			walker.on_Leaf(s)
			return
		
		if isinstance(s, ast.ListSyntax):
			assert isinstance(self._container, (sem.EnumType, sem.ClassType))
			for m in s:
				self._walk(m, walker)
			return
		
		if isinstance(s, ast.Token):
			assert isinstance(self._container, sem.EnumType)
			assert s.type == ast.tokens.Sym
			walker.on_Token(s)
			return
		
		if isinstance(s, ast.TypeRef):
			assert isinstance(self._container, sem.EnumType)
			walker.on_TypeRef(s)
			return
		
		if isinstance(s, ast.StmtUse):
			walker.on_StmtUse(s)
			return
		
		raise TODO("unhandled case '{}'", type(s))
	
	def _getVisibleForm(self, tref: ast.TypeRef) -> sem.Form:
		t = self._getVisibleType(tref)
		return self._type2Form(t)
	
	def _type2Form(self, t: Optional[sem.Type]) -> sem.Form:
		if t is None:
			return self.cu.getAnyForm()
		return sem.Form(t, t.staticLayout, True)
	
	def _getVisibleType(self, tref: ast.TypeRef) -> Optional[sem.Type]:
		si = self._getVisibleName(tref.name)
		if si is Error: return None
		if isinstance(si, sem.Type):
			if tref.nullable is not None:
				raise TODO("impl: `T?` syntax")
			return si
		self.msg.emit("not a type")
		return None
	
	def _getVisibleName(self, name: ast.Name) -> Expr:
		if name.base is not None:
			return self._getItemInByExprName(self._getVisibleName(name.base), name.member)
		n = _token_to_str(name.member.name)
		si = self._scope.get(n)
		if si is None:
			self.msg.emit("not found: '{}'", n)
			return Error
		return si
	
	def _getItemInByName(self, top: Expr, name: ast.Name) -> Expr:
		if name.base is not None:
			top = self._getItemInByName(top, name.base)
		return self._getItemInByExprName(top, name.member)
	
	def _getItemInByExprName(self, top: Expr, name: ast.ExprName) -> Expr:
		if top is Error: return Error
		
		n = _token_to_str(name.name)
		
		if isinstance(top, FuncGroup):
			self.msg.emit("func group has no member '{}'", n)
			return Error
		
		if isinstance(top, sem.TypeParam):
			self.msg.emit("type param has no member '{}'", n)
			return Error
		
		if isinstance(top, (sem.Module, sem.ClassType, sem.EnumType)):
			child = self._getItemDict(top).get(n)
			if child is None:
				self.msg.emit("{} has no member '{}'", type(top), n)
				return Error
			return child
		
		raise TODO("unhandled case '{}'", type(top))
	
	def _putAliasInScope(self, name: str, item: Expr) -> None:
		self._scope.add(name, item)
	
	@property
	def _container(self):
		return self._containers[-1]
	
	@property
	def _scope(self):
		return self._scopes[-1]
	
	def _pushModule(self, name: ast.Name, *, stub: bool, scope: bool) -> None:
		module = self._container
		assert isinstance(module, sem.Module)
		
		flat = _flatten_name(name)
		for part in flat:
			if part.args is not None:
				self.msg.emit("{}: invalid syntax", part.args.range)
			name = _token_to_str(part.name)
			module = self.cu.getModule(module, name)
			self._containers.append(module)
			
			if scope:
				self._scopes.append(SideScope(self._getItemDict(module), self._scope))
		self._module_pop_info.append(ModulePopInfo(len(flat), stub))
	
	def _popModule(self, *, stub: bool, scope: bool) -> None:
		size = 0
		
		if stub:
			# Only pop a stub module, if it exists.
			if self._module_pop_info:
				top = self._module_pop_info.pop()
				if top.stub:
					size += top.size
		else:
			# Pop up to the non-stub module
			top = self._module_pop_info.pop()
			size += top.size
			while top.stub:
				top = self._module_pop_info.pop()
				size += top.size
		
		if size:
			del self._containers[-size:]
			if scope:
				for _ in range(size):
					self._scopes.pop()
	
	def _getItemDict(self, container: '_Container') -> Scope[Expr]:
		if container not in self._item_dicts:
			self._item_dicts[container] = self._buildItemDict(container)
		return self._item_dicts[container]
	
	def _buildItemDict(self, container: '_Container') -> Scope[Expr]:
		# The item dict of a container contains the scope items
		# declared within; specifically, not scope items declared
		# in parent containers.
		
		out = SingleScope() # type: Scope[Expr]
		
		if isinstance(container, (sem.Module, sem.ClassType, sem.EnumType)):
			for pi in self.cu.items:
				if pi.parent is not container:
					continue
				self._addScopeItem(out, pi)
			return out
		
		raise TODO("unhandled case '{}'", type(container))
	
	def _addScopeItem(self, out: Scope[Expr], item: sem.PackageItem) -> None:
		if isinstance(item, sem.Func):
			fg = out.getTop(item.name)
			if fg is None:
				fg = FuncGroup()
				out.add(item.name, fg)
			assert isinstance(fg, FuncGroup)
			fg.items.append(item)
			return
		
		if isinstance(item, (sem.GlobalVar, sem.Module, sem.ClassType, sem.EnumType)):
			out.add(item.name, item)
			return
		
		raise TODO("unhandled case '{}'", type(item))

class Walker(metaclass = ABCMeta):
	def __init__(self, needs_scope: bool) -> None:
		self.needs_scope = needs_scope

class Stage1(Walker):
	def __init__(self, sp: SyntaxProcessor, cu: sem.CompilationUnit) -> None:
		super().__init__(False)
		self.sp = sp
		self.cu = cu
	
	def on_EnumDef(self, s: ast.EnumDef) -> None:
		name = _token_to_str(s.name)
		t = sem.EnumType(self._container, name, proto = None)
		self._addGenParams(t.genParams, s.params)
		self.cu.add(t)
		# t.options are filled in stage 2
		self._associate_semantic(s, t)
	
	def on_ClassDef(self, s: ast.ClassDef) -> None:
		name = _token_to_str(s.name)
		layout = _token_to_layout(s.layout)
		kind = _token_to_classkind(s.kind)
		t = sem.ClassType(self._container, name, proto = None, layout = layout, kind = kind)
		self._addGenParams(t.genParams, s.params)
		self.cu.add(t)
		# t.supers, t.vtable, t.fields, t.methods are filled in stage 2
		self._associate_semantic(s, t)
	
	def on_Leaf(self, s: ast.Leaf) -> None:
		# Inside ...: leaf can be ...
		# module: func, global
		# class: func (method/ctor), field
		
		name = s.name
		
		is_field_or_global = all((
			s.gparams is None, s.params is None,
			isinstance(name, ast.Token),
			isinstance(s.body, (type(None), ast.LeafInitializer)),
		))
		
		if is_field_or_global:
			# Field or global
			if isinstance(self._container, sem.ClassType):
				# Field
				f = sem.Field(self._container, -1, name, None)
				# f.index, f.form are filled in stage 2
				self._associate_semantic(s, f)
			else:
				# Global
				g = sem.GlobalVar(None, _token_to_str(name), parent = self._container)
				self.cu.add(g)
				
				# g.form, g.init are filled in stage 2
				self._associate_semantic(s, g)
		else:
			# Func
			
			if isinstance(self._container, sem.ClassType):
				if isinstance(name, ast.LeafNameCtor):
					kind = sem.FuncKind.Ctor
					assert isinstance(s.body, (ast.Token, ast.StmtBlock, type(None)))
				else:
					kind = sem.FuncKind.Method
			else:
				kind = sem.FuncKind.Func
			
			fname = _token_to_str(name)
			funcs = FuncsWithAST(s)
			
			for etter_token, body in _iter_leaf_bodies(s.body):
				isAbstract = (s.body is None)
				f = sem.Func(self._container, name = fname, sig = None, kind = kind, proto = None, isAbstract = isAbstract)
				self.cu.add(f)
				funcs.fwabs.append(FuncWithASTBody(f, body, etter_token))
			
			# f.sig is filled in stage 2
			self._associate_semantic(s, funcs)
	
	def on_Token(self, s: ast.Token) -> None:
		# Ignore. It'll be handled in stage 2.
		pass
	
	def on_TypeRef(self, s: ast.TypeRef) -> None:
		# Ignore. It'll be handled in stage 2.
		pass
	
	def on_StmtUse(self, s: ast.StmtUse) -> None:
		# Ignore. It'll be handled in stage 2.
		pass
	
	@property
	def _container(self):
		return self.sp._container
	
	def _addGenParams(self, out: List[sem.TypeParam], stx: Optional[ast.ListSyntax[ast.GenericParam]]) -> None:
		if stx is None: return
		for gp in stx:
			name = _token_to_str(gp.name)
			variance = _token_to_variance(gp.variance)
			out.append(sem.TypeParam(name, variance))
	
	def _associate_semantic(self, s: ast.Syntax, s2: '_Stage2Item') -> None:
		assert s not in self.sp._ast2sem
		self.sp._ast2sem[s] = s2

class Stage2(Walker):
	def __init__(self, sp: SyntaxProcessor, cu: sem.CompilationUnit) -> None:
		super().__init__(True)
		self.sp = sp
		self.cu = cu
	
	def on_EnumDef(self, s: ast.EnumDef) -> None:
		# Nothing to do.
		pass
	
	def on_ClassDef(self, s: ast.ClassDef) -> None:
		# Nothing to do.
		pass
	
	def on_Leaf(self, s: ast.Leaf) -> None:
		l = self._get_semantic(s)
		fret = self.sp._getVisibleForm(s.rettype)
		
		if isinstance(l, sem.Field):
			assert isinstance(self._container, sem.ClassType)
			l.index = len(self._container.fields)
			l.form = fret
			return
		if isinstance(l, sem.GlobalVar):
			l.form = fret
			l.init = self._createGlobalInitializer(l, s)
			return
		if isinstance(l, FuncsWithAST):
			plist = None
			
			stx_params = l.syntax.params
			if stx_params is not None:
				plist = []
				for stx_param in stx_params:
					default = None
					if stx_param.default is not None:
						raise TODO("impl")
					param = sem.Param(self.sp._getVisibleForm(stx_param.type), _token_to_str(stx_param.name), default)
					plist.append(param)
			
			for fwab in l.fwabs: # type: FuncWithASTBody
				f = fwab.func
				body = fwab.body
				etter = fwab.etter
				pthis = None # type: Optional[sem.Param]
				
				if isinstance(self._container, sem.ClassType):
					pthis = sem.Param(self.sp._type2Form(self._container), '$this')
				
				if etter is not None and etter.type == ast.tokens.KSet:
					pvalue = sem.Param(fret, '$value')
					fret = self.cu.getUnitForm()
				else:
					pvalue = None
				
				f.sig = sem.FuncSig(fret, pthis, plist, pvalue)
			return
		
		raise TODO("unhandled case '{}'", type(l))
	
	def on_Token(self, s: ast.Token) -> None:
		c = self._container
		assert isinstance(c, sem.EnumType)
		c.options.append(self.cu.getSymbolType(_token_to_symbol_name(s)))
	
	def on_TypeRef(self, s: ast.TypeRef) -> None:
		c = self._container
		assert isinstance(c, sem.EnumType)
		c.options.append(self.sp._getVisibleType(s))
	
	def on_StmtUse(self, s: ast.StmtUse) -> None:
		prefix = s.prefix
		aliases = s.aliases
		top = self.sp._getVisibleName(prefix)
		
		if aliases is None:
			self.sp._putAliasInScope(_token_to_str(prefix.member.name), top)
		else:
			for a in aliases:
				t = self.sp._getItemInByName(top, a.imported)
				if a.alias is None:
					alias = a.imported.member.name
				else:
					alias = a.alias
				self.sp._putAliasInScope(_token_to_str(alias), t)
	
	@property
	def _container(self):
		return self.sp._container
	
	def _get_semantic(self, s: ast.Syntax) -> '_Stage2Item':
		return self.sp._ast2sem[s]
	
	def _createGlobalInitializer(self, gv: sem.GlobalVar, stx: ast.Leaf) -> sem.Func:
		rform = self.cu.getUnitForm()
		sig = sem.FuncSig(rform, None, None, None)
		return sem.Func(None, Naming.GlobalInit(gv.fullName),
			sig = sig, kind = sem.FuncKind.Func, proto = None, isAbstract = False
		)

def _flatten_name(name: ast.Name) -> List[ast.ExprName]:
	l = []
	n = name # type: Optional[ast.Name]
	while n is not None:
		l.append(n.member)
		n = n.base
	return list(reversed(l))

def _token_to_str(token: ast.Token) -> str:
	assert token.type == ast.tokens.Id
	data = token.data
	assert data
	if data[0] == '$':
		data = data[1:]
	return data

def _token_to_variance(token: Optional[ast.Token]) -> sem.Variance:
	if token is None:
		return sem.Variance.Both
	if token.type == ast.tokens.KIn:
		return sem.Variance.In
	if token.type == ast.tokens.KOut:
		return sem.Variance.Out
	raise TODO("invalid syntax: not a variance token '{}'", token)

def _token_to_layout(token: Optional[ast.Token]) -> sem.Layout:
	if token is None:
		return sem.Layout.Ref
	assert token.type == ast.tokens.Sym
	lname = token.data[1:]
	if lname == 'ref':
		return sem.Layout.Ref
	if lname == 'addr':
		return sem.Layout.Addr
	if lname == 'bit0':
		return sem.Layout.Bit0
	if lname == 'bit1':
		return sem.Layout.Bit1
	if lname == 'bit8':
		return sem.Layout.Bit8
	if lname == 'bit16':
		return sem.Layout.Bit16
	if lname == 'bit32':
		return sem.Layout.Bit32
	if lname == 'bit64':
		return sem.Layout.Bit64
	raise TODO("unknown layout '{}'", lname)

def _token_to_classkind(token: ast.Token) -> sem.ClassKind:
	if token.type == ast.tokens.KClass:
		return sem.ClassKind.Class
	if token.type == ast.tokens.KRole:
		return sem.ClassKind.Role
	if token.type == ast.tokens.KObject:
		return sem.ClassKind.Object
	if token.type == ast.tokens.KError:
		return sem.ClassKind.Error
	raise TODO("unknown class kind '{}'", token)

def _token_to_symbol_name(token: ast.Token) -> str:
	assert token.type == ast.tokens.Sym
	assert token.data[0] == '#'
	return token.data[1:]

def _iter_leaf_bodies(s: ast.LeafBody) -> Tuple[Optional[ast.Token], Union[ast.FuncCode, ast.LeafInitializer]]:
	if isinstance(s, ast.ListSyntax):
		for fe in s:
			yield fe.kind, fe.body
		return
	yield None, s

class ModulePopInfo:
	__slots__ = ('size', 'stub')
	
	def __init__(self, size: int, stub: bool) -> None:
		self.size = size
		self.stub = stub

_Container = Union[
	sem.Module, sem.ClassType, sem.EnumType,
]

class FuncsWithAST:
	__slots__ = ('syntax', 'fwabs')
	
	def __init__(self, syntax: ast.Leaf) -> None:
		self.syntax = syntax
		self.fwabs = [] # type: List[FuncWithASTBody]

class FuncWithASTBody:
	__slots__ = ('func', 'body', 'etter')
	
	def __init__(self, func: sem.Func, body: ast.FuncCode, etter: Optional[ast.Token]) -> None:
		self.func = func
		self.body = body
		self.etter = etter

_Stage2Item = Union[
	FuncsWithAST,
	sem.ClassType,
	sem.EnumType,
	sem.GlobalVar,
	sem.Field,
]

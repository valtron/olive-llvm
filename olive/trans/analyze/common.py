from typing import Union

from olive.phase import sem

class FuncGroup:
	__slots__ = ('items',)
	def __init__(self) -> None:
		self.items = [] # type: List[sem.Func]

@object.__new__
class Error:
	__slots__ = ()

Expr = Union[
	FuncGroup, sem.ClassType, sem.EnumType, sem.TypeParam,
	sem.Module, sem.GlobalVar, sem.Expr, type(Error),
]

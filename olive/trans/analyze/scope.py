from abc import ABCMeta, abstractmethod
from typing import TypeVar, Generic, Optional, Dict

T = TypeVar('T')

class Scope(Generic[T], metaclass = ABCMeta):
	@abstractmethod
	def add(self, name: str, data: T) -> None: pass
	
	@abstractmethod
	def get(self, name: str) -> Optional[T]: pass
	
	@abstractmethod
	def getTop(self, name: str) -> Optional[T]: pass

class ChildScope(Scope[T]):
	__slots__ = ('parent', 'items')
	
	def __init__(self, parent: Optional['Scope[T]'], items: Dict[str, T]) -> None:
		self.parent = parent
		self.items = items
	
	def add(self, name: str, data: T) -> None:
		assert name not in self.items
		self.items[name] = data
	
	def get(self, name: str) -> Optional[T]:
		obj = self.getTop(name)
		if obj is None and self.parent is not None:
			obj = self.parent.get(name)
		return obj
	
	def getTop(self, name: str) -> Optional[T]:
		return self.items.get(name)

class SingleScope(Generic[T]):
	__slots__ = ('items',)
	
	def __init__(self) -> None:
		self.items = {} # type: Dict[str, T]
	
	def add(self, name: str, data: T) -> None:
		assert name not in self.items
		self.items[name] = data
	
	def get(self, name: str) -> Optional[T]:
		return self.getTop(name)
	
	def getTop(self, name: str) -> Optional[T]:
		return self.items.get(name)

class SideScope(Generic[T]):
	__slots__ = ('parent', 'side', 'items')
	
	def __init__(self, side: Scope[T], parent: Optional['Scope[T]']) -> None:
		self.parent = parent
		self.side = side
		self.items = {} # type: Dict[str, T]
	
	def add(self, name: str, data: T) -> None:
		assert self.side.get(name) is None
		assert name not in self.items
		self.items[name] = data
	
	def get(self, name: str) -> Optional[T]:
		obj = self.getTop(name)
		if obj is None and self.parent is not None:
			obj = self.parent.get(name)
		return obj
	
	def getTop(self, name: str) -> Optional[T]:
		obj = self.side.get(name)
		if obj is not None: return obj
		return self.items.get(name)

class ScopeStack(Generic[T]):
	__slots__ = ('top', '_bottom')
	
	def __init__(self, top: Scope[T]) -> None:
		self.top = top
		self._bottom = top
	
	def add(self, k: str, v: T) -> None:
		self.top.add(k, v)
	
	def get(self, k: str) -> Optional[T]:
		return self.top.get(k)
	
	def push(self, side: Optional[Scope[T]] = None) -> None:
		if side is None:
			self.top = ChildScope(self.top)
		else:
			self.top = SideScope(side, self.top)
	
	def pop(self) -> None:
		assert self.top != self._bottom
		self.top = self.top.parent

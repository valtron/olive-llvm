from typing import List, Optional, TYPE_CHECKING
from llvmlite.ir import ( # type: ignore
	Function, Constant, Module, IntType, FunctionType, VoidType,
	Value, GlobalVariable, Type, LiteralStructType,
	IdentifiedStructType as StructType, ArrayType, Argument,
	IRBuilder as _BuilderImpl, AllocaInstr, Undefined, Block,
	PointerType as _PointerType, PhiInstr as PhiNode
)
from olive.utils import TODO, cached_property

if TYPE_CHECKING:
	from .compiler import Compiler

def PtrType() -> Type:
	return IntType(8).as_pointer()

def EmptyType() -> LiteralStructType:
	return LiteralStructType([])

class Builder:
	def __init__(self, lfunc: Function, cp: 'Compiler') -> None:
		self.lfunc = lfunc
		self.cp = cp
		self.entry_block = lfunc.append_basic_block('entry')
		self.impl = _BuilderImpl(self.entry_block)
		block = self.startBlock()
		self.impl.branch(block)
		self.impl.position_at_end(block)
	
	def needsTerminator(self) -> bool:
		bl = self.currentBlock()
		if bl is None: return False
		return bl.terminator is None
	
	def isCurrentTerminated(self) -> bool:
		return self.currentBlock().terminator is not None
	
	def setBlock(self, block: Block) -> None:
		if block.terminator is None:
			self.impl.position_at_end(block)
		else:
			self.impl.position_before(block.terminator)
	
	def currentBlock(self) -> Block:
		return self.impl.block
	
	def startBlock(self) -> Block:
		return self.lfunc.append_basic_block('bl')
	
	def createVar(self, type: Type, name: str) -> AllocaInstr:
		with self.impl.goto_block(self.entry_block):
			ret = self.impl.alloca(type, 0, name)
		return ret
	
	def call(self, lfunc: Function, largs: List[Value]) -> Value:
		return self.impl.call(lfunc, largs)
	
	def store(self, alloca: AllocaInstr, value: Value) -> None:
		self.impl.store(value, alloca)
	
	def load(self, addr: Value) -> Value:
		return self.impl.load(addr)
	
	def setField(self, obj: Value, idx: int, rhs: Value) -> None:
		fptr = self.impl.gep(obj, [0, idx])
		self.impl.store(fptr, rhs)
	
	def getField(self, obj: Value, idx: int) -> Value:
		fptr = self.impl.gep(obj, [0, idx])
		return self.impl.load(fptr)
	
	def ret(self, lval: Optional[Value] = None) -> None:
		if lval is None:
			self.impl.ret_void()
		else:
			self.impl.ret(lval)
	
	def jump(self, block: Block) -> None:
		self.impl.branch(block)
	
	def branch(self, cond: Value, thenBlock: Block, elseBlock: Block) -> None:
		self.impl.cbranch(cond, thenBlock, elseBlock)
	
	def refcast(self, lv: Value, lt: Optional[StructType] = None) -> Value:
		if lt is None:
			lt = PtrType()
		else:
			lt = lt.as_pointer()
		return self.impl.bitcast(lv, lt)
	
	def notb(self, arg: Value) -> Value:
		return self.impl.not_(arg)
	
	def unequal(self, lhs: Value, rhs: Value) -> Value:
		traw = IntType(64)
		lhs = self._bitcast(lhs, traw)
		rhs = self._bitcast(rhs, traw)
		return self.impl.icmp_signed('!=', lhs, rhs)
	
	def equal(self, lhs: Value, rhs: Value) -> Value:
		traw = IntType(64)
		lhs = self._bitcast(lhs, traw)
		rhs = self._bitcast(rhs, traw)
		return self.impl.icmp_signed('==', lhs, rhs)
	
	def _bitcast(self, arg: Value, t: Type) -> Value:
		ta = arg.type
		if isinstance(ta, IntType) and isinstance(t, _PointerType):
			return self.impl.inttoptr(arg, t)
		if isinstance(ta, _PointerType) and isinstance(t, IntType):
			return self.impl.ptrtoint(arg, t)
		if isinstance(ta, _PointerType) and isinstance(t, _PointerType):
			return self.impl.bitcast(arg, t)
		return self.impl.sext(arg, t)
	
	def phiNode(self, t: Type) -> PhiNode:
		return self.impl.phi(t)
	
	def addIncoming(self, phi: PhiNode, v: Value, block: Block) -> None:
		phi.add_incoming(v, block)
	
	def alloc(self, t: StructType, vtable: Value) -> Value:
		f_alloc = self._f_alloc
		obj = self.call(f_alloc, [self.impl.trunc(self._sizeof(t), f_alloc.args[0].type)])
		casted = self._bitcast(obj, t.as_pointer())
		self.setField(casted, 0, vtable)
		return obj
	
	def isa(self, obj: Value, reflTypeObj: Value) -> Value:
		f_isa = self._f_isa
		return self.call(f_isa, [obj, reflTypeObj])
	
	def _sizeof(self, type: Type) -> Value:
		gep = Constant(type.as_pointer(), None).gep([1])
		return self.impl.ptrtoint(gep, IntType(64))
	
	@cached_property
	def _f_alloc(self) -> Function:
		return self.cp.getBuiltinAlloc()
	
	@cached_property
	def _f_isa(self) -> Function:
		return self.cp.getBuiltinIsa()

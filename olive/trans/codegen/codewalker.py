from typing import Optional, TYPE_CHECKING, Iterator, Tuple
from olive.phase import sem
from olive.utils import Messages, TODO

from . import llvm

if TYPE_CHECKING:
	from .compiler import Compiler

class CodeWalker:
	def __init__(self,
		compiler: 'Compiler',
		cu: sem.CompilationUnit, msg: Messages,
		func: sem.Func, lfunc: llvm.Function,
	) -> None:
		self.cp = compiler
		self.cu = cu
		self.msg = msg
		self.builder = llvm.Builder(lfunc, compiler)
		self.ret = func.sig.ret
		self.vars = {} # type: Dict[sem._VarExpr, llvm.AllocaInstr]
		self.loops = {} # type: Dict[sem.LoopStmt, LoopInfo]
		self.stmtExprs = {} # type: Dict[sem.StmtExpr, StmtExprInfo]
		
		sig = func.sig
		args = lfunc.args # type: List[llvm.Argument]
		for (p, lp) in zip(_iterParams(func.sig), lfunc.args):
			self.vars[p] = self._declareVar(p, lp)
	
	def runStmt(self, body: sem.Stmt) -> None:
		B = self.builder
		self._walkStmt(body)
		if B.needsTerminator():
			lrty = self.cp.getCompiledType(self.ret)
			if isinstance(lrty, llvm.VoidType):
				B.ret()
			else:
				B.ret(llvm.Constant(lrty, llvm.Undefined))
	
	def _walkStmt(self, stmt: sem.Stmt) -> None:
		B = self.builder
		
		if isinstance(stmt, sem.BlockStmt):
			for s in stmt.stmts:
				self._walkStmt(s)
			return
		
		if isinstance(stmt, sem.RetStmt):
			if stmt.value is not None:
				lv = self._walkValue(stmt.value)
				B.ret(lv)
			else:
				B.ret()
			return
		
		if isinstance(stmt, sem.LoopStmt):
			b = B.startBlock()
			li = LoopInfo(b)
			self.loops[stmt] = li
			
			B.jump(b)
			B.setBlock(b)
			self._walkStmt(stmt.body)
			
			if not B.isCurrentTerminated():
				B.jump(b)
			
			if li.bottom is not None:
				B.setBlock(li.bottom)
			
			return
		
		if isinstance(stmt, sem.BreakStmt):
			li = self.loops[stmt.loop]
			if li.bottom is None:
				li.bottom = B.startBlock()
			B.jump(li.bottom)
			return
		
		if isinstance(stmt, sem.NextStmt):
			li = self.loops[stmt.loop]
			B.jump(li.top)
			return
		
		if isinstance(stmt, sem.IfStmt):
			bthen = B.startBlock()
			belse = B.startBlock()
			
			B.branch(self._walkValue(stmt.cond), bthen, belse)
			
			bend = None # type: llvm.Block
			
			B.setBlock(bthen)
			self._walkStmt(stmt.thenBody)
			if not B.isCurrentTerminated():
				if bend is None:
					bend = B.startBlock()
				B.jump(bend)
			
			B.setBlock(belse)
			if stmt.elseBody is not None:
				self._walkStmt(stmt.elseBody)
			if not B.isCurrentTerminated():
				if bend is None:
					bend = B.startBlock()
				B.jump(bend)
			
			if bend is not None:
				B.setBlock(bend)
			
			return
		
		if isinstance(stmt, sem.AssignStmt):
			lvar = self._getVar(stmt.var)
			lrhs = self._walkValue(stmt.rhs)
			B.store(lvar, lrhs)
			return
		
		if isinstance(stmt, sem.FieldSetStmt):
			lobj = self._walkValue(stmt.obj)
			t = stmt.obj.form.type
			assert isinstance(t, sem.ClassType)
			llhs = B.refcast(lobj, self.cp.getStructType(t))
			lrhs = self._walkValue(stmt.rhs)
			B.setField(llhs, stmt.field.index + 1, lrhs)
			return
		
		if isinstance(stmt, sem.CallStmt):
			self._walkValue(stmt.expr)
			return
		
		if isinstance(stmt, sem.LocalRetStmt):
			lv = self._walkValue(stmt.value)
			info = self.stmtExprs[stmt.context]
			info.blocks.append(B.currentBlock())
			info.lvalues.append(lv)
			info.svalues.append(stmt.value)
			B.jump(info.end)
			return
		
		self.msg.emit("internal error: unknown sem.Stmt subclass '{}'", type(stmt))
		assert False
	
	def _walkValue(self, expr: sem.Expr) -> llvm.Value:
		B = self.builder
		
		if isinstance(expr, sem.FieldGetExpr):
			lobj = self._walkValue(expr.obj)
			t = expr.obj.form.type
			assert isinstance(t, sem.ClassType)
			llhs = B.refcast(lobj, self.cp.getStructType(t))
			lv = B.getField(llhs, expr.field.index + 1)
			return lv
		
		if isinstance(expr, sem._VarExpr):
			lvar = self._getVar(expr)
			return B.load(lvar)
		
		if isinstance(expr, sem.CallExpr):
			lfunc = self.cp.getCompiledFunc(expr.func)
			largs = [self._walkValue(a) for a in _iterArgs(expr.args)]
			return self.builder.call(lfunc, largs)
		
		if isinstance(expr, sem.NewExpr):
			t = expr.form.type
			assert isinstance(t, sem.ClassType)
			lt = self.cp.getStructType(t)
			lvo = self.cp.getTypeVtableObj(t)
			return B.alloc(lt, lvo)
		
		if isinstance(expr, sem.StmtExpr):
			endBlock = B.startBlock()
			info = StmtExprInfo(endBlock)
			self.stmtExprs[expr] = info
			self._walkStmt(expr.stmt)
			if not B.isCurrentTerminated():
				self.msg.emit("error: lower-expr doesn't local-ret a value in all branches")
				B.jump(endBlock)
			del self.stmtExprs[expr]
			
			B.setBlock(endBlock)
			phi = B.phiNode(self.cp.getCompiledType(expr.form))
			
			for block, lvalue in zip(info.blocks, info.lvalues):
				B.setBlock(block)
				B.addIncoming(phi, lvalue, block)
			
			B.setBlock(endBlock)
			return phi
		
		if isinstance(expr, sem.IsaExpr):
			lhs = expr.lhs
			if lhs.form.layout is sem.Layout.Ref:
				return B.isa(self._walkValue(lhs), B.refcast(self.cp.getReflTypeObj(expr.rhs)))
			return self.cp.getBoolConstant(sem.isSubtype(lhs.form.type, expr.rhs))
		
		if isinstance(expr, sem.IsExpr):
			ll = self._walkValue(expr.lhs)
			lr = self._walkValue(expr.rhs)
			if expr.lhs.form.layout != expr.rhs.form.layout:
				return self.cp.getBoolConstant(False)
			return B.equal(ll, lr)
		
		if isinstance(expr, sem.NotExpr):
			return B.notb(self._walkValue(expr.arg))
		
		if isinstance(expr, sem.BoolCastExpr):
			return self._castToBool(expr.arg)
		
		if isinstance(expr, sem.BoolConst):
			return self.cp.getBoolConstant(expr.data)
		
		if isinstance(expr, sem.StrConst):
			return B.refcast(self.cp.getStringConstant(expr.data))
		
		if isinstance(expr, sem.NumConst):
			return self.cp.getIntConstant(expr.data)
		
		if isinstance(expr, sem.SymConst):
			return B.refcast(self.cp.getSymbolConstant(expr.data))
		
		if isinstance(expr, sem.Relayout):
			larg = self._walkValue(expr.arg)
			return self._handleRelayout(larg, expr.arg.form, expr.form)
		
		self.msg.emit("internal error: unknown sem.Expr subclass '{}'", type(expr))
		assert False
	
	def _declareVar(self, v: sem._VarExpr, lv: Optional[llvm.Value] = None) -> llvm.Value:
		lt = self.cp.getCompiledType(v.form)
		lvar = self.builder.createVar(lt, v.name)
		if lv is not None:
			self.builder.store(lvar, lv)
		return lvar
	
	def _castToBool(self, value: sem.Expr) -> llvm.Value:
		lv = self._walkValue(value)
		layout = value.form.layout
		if layout == sem.Layout.Ref:
			lfalsy = self.cp.getSymbolConstant('null')
		else:
			lfalsy = llvm.Constant(lv, None)
		return self.builder.unequal(lv, lfalsy)
	
	def _getVar(self, var: sem._VarExpr) -> llvm.Value:
		if var not in self.vars:
			if isinstance(var, sem.LocalVar):
				lvar = self._declareVar(var)
			else:
				assert isinstance(var, sem.GlobalVar)
				lvar = self.cp.getCompiledGlobal(var)
			self.vars[var] = lvar
		return self.vars[var]
	
	def _handleRelayout(self, larg: llvm.Value, sourceForm: sem.Form, targetForm: sem.Form) -> llvm.Value:
		"""
			# Addr (might get rid of this)
				Addr -> *   : illegal
				*    -> Addr: illegal
			
			# Empty
				*     -> Empty: llvm.Constant(llvm.EmptyType(), llvm.Undefined)
				Empty -> Ref  : get the global object for sourceForm.type
			
			# Bit: type must <= `enum { #true, #false }`, illegal otherwise
				Bit   -> Ref: (0 -> #false, 1 -> #true)
				Ref   -> Bit: (#false -> 0, #true -> 1)
				Empty -> Bit: constant 0 or 1, depending on type
			
			# Ints
				Int<N> -> Ref   : box
				Ref    -> Int<N>: unbox
		"""
		raise TODO("impl")

class LoopInfo:
	def __init__(self, top: llvm.Block) -> None:
		self.top = top
		self.bottom = None # type: Optional[llvm.Block]

class StmtExprInfo:
	def __init__(self, end: llvm.Block) -> None:
		self.end = end
		self.blocks = [] # type: List[llvm.Block]
		self.lvalues = [] # type: List[llvm.Value]
		self.svalues = [] # type: List[sem.Expr]

def _iterParams(sig: sem.FuncSig) -> Iterator[sem.Param]:
	if sig.this is not None:
		yield sig.this
	if sig.value is not None:
		yield sig.value
	if sig.params is not None:
		yield from sig.params

def _iterArgs(args: sem.CallArgs) -> Iterator[sem.Expr]:
	if args.this is not None:
		yield args.this
	if args.value is not None:
		yield args.value
	if args.args is not None:
		yield from args.args

def _erasedLayout(field: sem.Field) -> sem.Form:
	proto = field.owner.proto
	if proto is not None:
		field = proto.fields[field.index]
	return field.form

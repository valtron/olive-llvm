from enum import Enum
from typing import List, Optional, cast as unnecessary_cast
from olive.utils import Messages, Filesystem, TODO, Naming
from olive.phase import sem

from . import llvm
from .codewalker import CodeWalker

class CompilationState(Enum):
	Uncompiled = 1
	InProgress = 2
	Compiled = 3

class TypeCompilation:
	def __init__(self, type: sem.Type) -> None:
		self.type = type
		self.state = CompilationState.Uncompiled

class Compiler:
	"""
		Conventions:
		
		- `getX(sem) -> llvm` methods declare the llvm object
		- `defineX(sem) -> None` methods define the llvm object
	"""
	
	def __init__(self, msg: Messages, cu: sem.CompilationUnit) -> None:
		self.msg = msg
		self.cu = cu
		self.lmod = llvm.Module(cu.target.name)
		self.typeCompilations = {} # type: Dict[sem.TypeDef, TypeCompilation]
	
	def run(self) -> None:
		self._declareInternals()
		# Compile types
		for t in self.cu.typeDefs:
			self.typeCompilations[t] = TypeCompilation(t)
		for t in self.cu.typeDefs:
			self._compileType1(t)
		for t in self.cu.typeDefs:
			self._compileType2(t)
		self.typeCompilations.clear()
		# Compile funcs/globals
		for fd in self.cu.funcDefs:
			self._compileFunc(fd.func, fd.code)
		for gv in self.cu.globalDefs:
			self._compileGlobal(gv)
	
	def createMain(self) -> None:
		func = None
		for item in self.cu.target.items:
			if isinstance(item, sem.Func):
				if item.name == 'main' and item.owner is None:
					func = item
					break
		
		if func is None:
			self.msg.emit("error: 'main' func not found")
			return
		
		lfunc = self.getCompiledFunc(func)
		lint = llvm.IntType(32).as_pointer()
		lptr = llvm.PtrType()
		lft = llvm.FunctionType(lint, [lint, lptr]);
		
		lmain = llvm.Function(self.lmod, lft, 'main')
		
		builder = llvm.Builder(lmain)
		
		for pkg in _getDependencyOrderedPackages(self.cu.target):
			for st in pkg.usedSymbols:
				self._defineCompiledSymbol(st)
			init = pkg.init
			if init is not None:
				linit = self.getCompiledFunc(func)
				builder.call(linit, [])
		
		lMainArgs = []
		plist = func.sig.params
		if plist is not None:
			for param in plist:
				pname = param.name
				f = None
				lTmpArgs = []
				
				if pname == 'args':
					f = self._getPackageFunc('sys.process', '_wrap_argv')
					lTmpArgs.append(lmain.args[0])
					lTmpArgs.append(lmain.args[1])
				elif pname == 'console':
					f = self._getPackageFunc('sys.process', '_console_get')
				elif pname == 'fsys':
					f = self._getPackageFunc('sys.process', '_filesystem_get')
				else:
					self.msg.emit("error: '{}' is not a known main param", pname)
				
				if f is None:
					continue
				
				lf = self.getCompiledFunc(f)
				lMainArgs.append(builder.call(lf, lTmpArgs))
		
		rval = builder.call(lfunc, lMainArgs)
		if isinstance(lfunc.ftype.return_type, llvm.VoidType):
			builder.ret(llvm.Constant(lint, 0))
		else:
			builder.ret(rval)
	
	def saveIR(self, filename: str) -> None:
		if Filesystem.PathExists(filename):
			Filesystem.RemovePath(filename)
		with Filesystem.Open(filename, 'w') as fh:
			fh.write(str(self.lmod))
	
	def _compileFunc(self, func: sem.Func, code: sem.Stmt) -> None:
		lf = self.getCompiledFunc(func)
		cw = CodeWalker(self, self.cu, self.msg, func, lf)
		cw.runStmt(code)
	
	def _compileGlobal(self, gvar) -> None:
		go = self.getCompiledGlobal(gvar)
		go.initializer = llvm.Constant(go.type.pointee, None)
	
	def _compileType1(self, t: sem.TypeDef) -> None:
		proto = t.proto
		if proto is not None:
			t = proto
		
		assert t.proto is None
		
		if t not in self.typeCompilations:
			return
		
		tc = self.typeCompilations[t]
		if tc.state == CompilationState.Compiled:
			return
		
		if tc.state == CompilationState.InProgress:
			self.msg.emit("cycle in supers/options, detected in {!r}", t)
		
		tc.state = CompilationState.InProgress
		
		if isinstance(t, sem.ClassType):
			for x0 in t.supers:
				self._compileType1(x0)
			self._initializeVtable(t)
		elif isinstance(t, sem.EnumType):
			for x1 in t.options:
				if isinstance(x1, sem.TypeDef):
					self._compileType1(unnecessary_cast(sem.TypeDef, x1))
		
		tc.state = CompilationState.Compiled
	
	def _compileType2(self, t: sem.TypeDef) -> None:
		tc = self.typeCompilations[t]
		if not isinstance(t, sem.ClassType):
			return
		
		# Only need vtable for concrete classes (e.g. not roles)
		if not t.isAbstract:
			self._defineTypeVtableObj(t)
		
		if _typeHasRuntimeRepresentation(t):
			self._defineReflTypeObj(t)
	
	def _declareInternals(self) -> None:
		lptr = llvm.PtrType()
		lint = llvm.IntType(32)
		lbool = llvm.IntType(1)
		
		llvm.Function(self.lmod, llvm.FunctionType(lptr, [lint]), 'ol__mem_alloc')
		llvm.Function(self.lmod, llvm.FunctionType(lbool, [lptr, lptr]), 'ol__isa')
	
	def getCompiledFunc(self, func: sem.Func) -> llvm.Function:
		lfunc = self.lmod.get_global(func.id)
		if lfunc != None:
			assert isinstance(lfunc, llvm.Function)
			return lfunc
		
		lrt = self.getCompiledType(func.sig.ret)
		lpt = [] # type: List[llvm.Type]
		
		if func.sig.this is not None:
			lpt.append(self.getCompiledType(func.sig.this.form))
		if func.sig.value is not None:
			lpt.append(self.getCompiledType(func.sig.value.form))
		if func.sig.params is not None:
			for p in func.sig.params:
				lpt.append(self.getCompiledType(p.form))
		
		return llvm.Function(self.lmod, llvm.FunctionType(lrt, lpt), func.id)
	
	def getCompiledType(self, form: sem.Form) -> llvm.Type:
		return self._getCompiledLayout(form.layout)
	
	def getCompiledGlobal(self, gvar: sem.GlobalVar) -> llvm.GlobalVariable:
		name = Naming.Global(gvar.fullName)
		lgv = self.lmod.get_global(name)
		if lgv != None:
			assert isinstance(lgv, llvm.GlobalVariable)
			return lgv
		lgt = self.getCompiledType(gvar.form)
		return llvm.GlobalVariable(self.lmod, lgt, name)
	
	def _getCompiledLayout(self, l: sem.Layout) -> llvm.Type:
		if l == sem.Layout.Bit0:
			return llvm.EmptyType()
		if l == sem.Layout.Bit1:
			return llvm.IntType(1)
		if l == sem.Layout.Bit32:
			return llvm.IntType(32)
		return llvm.PtrType()
	
	def _getPackageFunc(self, pkg: str, name: str) -> Optional[sem.Func]:
		pack = None # type: Optional[sem.Package]
		for p in self.cu.packs:
			if p.name == pkg:
				pack = p
				break
		
		if pack is None:
			self.msg.emit("cannot find package '{}'", pkg)
			return None
		
		for pi in pack.items:
			if not isinstance(pi, sem.Func):
				continue
			if pi.name == name:
				return pi
		
		self.msg.emit("cannot find func '{}' in '{}'", name, pkg)
		return None
	
	def _defineCompiledSymbol(self, type: sem.SymbolType) -> None:
		lgv = self.getSymbolConstant(type.name)
		if lgv.initializer != None: return
		lgt = lgv.value_type.pointee # type: llvm.Type
		lvto = self.getTypeVtableObj(type)
		lgv.initializer = llvm.Constant(lgt, (lvto,))
		self._defineTypeVtableObj(type)
		self._defineReflTypeObj(type)
	
	def _initializeVtable(self, type: sem._ClassOrSymbolType) -> None:
		types = _linearizeSupers(type)
		
		offset = 0
		for s in types:
			if s == type: break
			selfFrag = s.vtable[-1]
			overridingFrag = sem.VtableFragment(s, offset)
			type.vtable.append(overridingFrag)
			for m in selfFrag.methods:
				om = _findOverridingMethod(types, m)
				if om is None:
					om = m
				overridingFrag.methods.append(om)
			offset += len(selfFrag.methods)
		
		selfFrag = sem.VtableFragment(type, offset)
		type.vtable.append(selfFrag)
		
		for m in type.methods:
			if m.kind == sem.FuncKind.Method:
				selfFrag.methods.append(m)
	
	def getReflTypeObj(self, type: sem.Type) -> llvm.GlobalVariable:
		assert _typeHasRuntimeRepresentation(type)
		
		# For typechecker
		assert isinstance(type, sem._ClassOrSymbolType)
		
		name = Naming.TypeObj(type.fullName)
		lgv = self.lmod.get_global(name)
		if lgv != None:
			assert isinstance(lgv, llvm.GlobalVariable)
			return lgv
		
		tyty = self.cu.getTypeType()
		ltyty = self.getStructType(tyty)
		return llvm.GlobalVariable(self.lmod, ltyty, name)
	
	def _defineReflTypeObj(self, type: sem._ClassOrSymbolType) -> None:
		tt = self.cu.getTypeType()
		ltt = self.getStructType(tt)
		
		classHeight = 0
		for ty in _linearizeSupers(type):
			if not ty.isAbstract:
				classHeight += 1
		
		lint = self._getCompiledLayout(self.cu.getIntType().layout)
		lptr = llvm.PtrType()
		
		fields = [
			# vtable* vtbl;
			self.getTypeVtableObj(tt),
			# string name;
			self.getStringConstant(type.fullName.encode('utf-8')).bitcast(lptr),
			# int classHeight;
			llvm.Constant(lint, classHeight),
		]
		
		to = self.getReflTypeObj(type)
		to.initializer = llvm.Constant(ltt, fields)
	
	def getTypeVtableObj(self, type: sem._ClassOrSymbolType) -> llvm.GlobalVariable:
		assert not type.isAbstract
		
		name = Naming.TypeVtableObj(type.fullName)
		lvo = self.lmod.get_global(name) # type: Optional[llvm.GlobalVariable]
		if lvo != None:
			return lvo
		
		lty = self._getTypeVtableType(type)
		return llvm.GlobalVariable(self.lmod, lty, name)
	
	def _defineTypeVtableObj(self, type: sem._ClassOrSymbolType) -> None:
		lptr = llvm.PtrType()
		
		lmethods = [] # type: List[llvm.Constant]
		lroles = [] # type: List[llvm.Constant]
		lclasses = [] # type: List[llvm.Constant]
		lroleoffsets = [] # type: List[llvm.Constant]
		
		for frag in type.vtable:
			t = frag.type
			lto = self.getReflTypeObj(t).bitcast(lptr)
			if t.isAbstract:
				lroles.append(lto)
				lroleoffsets.append(len(lmethods))
			else:
				lclasses.append(lto)
			
			for m in frag.methods:
				lmethods.append(self.getCompiledFunc(m).bitcast(lptr))
		
		lvt = self._getTypeVtableType(type)
		lvo = self.getTypeVtableObj(type)
		lvo.initializer = llvm.Constant(lvt, [
			# i16 nmeth;
			llvm.Constant(lvt.elements[0], len(lmethods)),
			# i8 nrole;
			llvm.Constant(lvt.elements[1], len(lroles)),
			# i8 nclass;
			llvm.Constant(lvt.elements[2], len(lclasses)),
			# void* methods[nmeth];
			llvm.Constant(lvt.elements[3], lmethods),
			# void* roles[nrole];
			llvm.Constant(lvt.elements[4], lroles),
			# void* classes[nclass];
			llvm.Constant(lvt.elements[5], lclasses),
			# i16 roleoffsets[nrole];
			llvm.Constant(lvt.elements[6], lroleoffsets),
		])
	
	def _getTypeVtableType(self, type: sem._ClassOrSymbolType) -> llvm.StructType:
		assert not type.isAbstract
		
		name = Naming.TypeVtableType(type.fullName)
		lt = self.lmod.context.get_identified_type(name) # type: llvm.StructType
		if lt.elements == None:
			lptr = llvm.PtrType()
			li16 = llvm.IntType(16)
			li8 = llvm.IntType(8)
			
			nmeth = 0
			nrole = 0
			nclass = 0
			for frag in type.vtable:
				nmeth += len(frag.methods)
				if type.isAbstract:
					nclass += 1
				else:
					nrole += 1
			
			lt.elements = [
				li16, li8, li8,
				llvm.ArrayType(lptr, nmeth),
				llvm.ArrayType(lptr, nrole),
				llvm.ArrayType(lptr, nclass),
				llvm.ArrayType(li16, nrole),
			]
		
		return lt
	
	def getSymbolConstant(self, sym: str) -> llvm.GlobalVariable:
		name = Naming.SymbolConstant(sym)
		lgv = self.lmod.get_global(name)
		if lgv is not None:
			return lgv
		symty = self.cu.getSymbolType(sym)
		self.cu.target.usedSymbols.add(symty)
		lsymty = self.getStructType(symty)
		return llvm.GlobalVariable(self.mod, lsymty, name)
	
	def getStringConstant(self, b: bytes) -> llvm.Constant:
		tstr = self.cu.getStringType()
		lbyte = llvm.IntType(8)
		
		lchars = llvm.Constant(llvm.ArrayType(lbyte, len(b)), b)
		
		lstr = self.getStructType(tstr)
		return llvm.Constant(lstr, [
			self.getTypeVtableObj(tstr),
			llvm.Constant(lstr.elements[0], len(b)),
			lchars.gep([0, 0])
		])
	
	def getIntConstant(self, n: int) -> llvm.Constant:
		tint = self.cu.getIntType()
		lint = self._getCompiledLayout(tint.layout)
		return llvm.Constant(lint, n)
	
	def getBoolConstant(self, b: bool) -> llvm.Constant:
		tbool = self.cu.getBoolType()
		lbool = self._getCompiledLayout(tbool.layout)
		return llvm.Constant(lbool, b)
	
	def getStructType(self, type: sem.ClassType) -> llvm.StructType:
		assert type.layout is sem.Layout.Ref
		
		name = Naming.Type(type.fullName)
		lt = self.lmod.context.get_identified_type(name) # type: llvm.StructType
		if lt.elements == None:
			elms = [] # type: llvm.Type
			elms.append(self.getTypeVtableObj(type).value_type)
			for f in _flattenFields(type):
				elms.append(self.getCompiledType(f.form))
			lt.elements = elms
		
		return lt
	
	def getBuiltinAlloc(self) -> llvm.Function:
		return self._getBuiltinFunc('ol__mem_alloc')
	
	def getBuiltinIsa(self) -> llvm.Function:
		return self._getBuiltinFunc('ol__isa')
	
	def _getBuiltinFunc(self, name) -> llvm.Function:
		lf = self.lmod.get_global(name)
		assert isinstance(lf, llvm.Function)
		return lf

def _getDependencyOrderedPackages(package: sem.Package) -> List[sem.Package]:
	out = [] # type: List[sem.Package]
	seen = set() # type: Set[sem.Package]
	def _aux(p: sem.Package) -> None:
		if p in seen: return
		seen.add(p)
		for dep in p.deps: _aux(dep)
		out.append(p)
	_aux(package)
	return out

def _typeHasRuntimeRepresentation(t: sem.Type) -> bool:
	return isinstance(t, sem.ClassType)

def _typeIsConcrete(t: sem.Type) -> bool:
	if not isinstance(t, sem.ClassType):
		return False
	return not t.isAbstract

def _linearizeSupers(type: sem._ClassOrSymbolType) -> List[sem._ClassOrSymbolType]:
	out = [] # type: List[sem._ClassOrSymbolType]
	seen = set() # type: Set[sem._ClassOrSymbolType]
	def _aux(t: sem._ClassOrSymbolType) -> None:
		if t in seen: return
		seen.add(t)
		for s in t.supers:
			pr = s.proto
			assert pr is not None
			_aux(pr)
		out.append(t)
	_aux(type)
	return out

def _findOverridingMethod(types: List[sem._ClassOrSymbolType], m: sem.Func) -> Optional[sem.Func]:
	for t in reversed(types):
		for mm in t.methods:
			if _isMethodCompatible(mm, m):
				return mm
	return None

def _flattenFields(type: sem._ClassOrSymbolType) -> List[sem.Field]:
	out = [] # type: List[sem.Field]
	def _aux(t: sem._ClassOrSymbolType) -> None:
		for s in t.supers:
			_aux(s)
		out.extend(t.fields)
	_aux(type)
	return out

def _isMethodCompatible(m: sem.Func, base: sem.Func) -> bool:
	if m.kind != base.kind: return False
	if m.name != base.name: return False
	
	ms = m.sig
	bs = base.sig
	
	# Check param list count
	mn = (-1 if ms.params is None else len(ms.params))
	bn = (-1 if bs.params is None else len(bs.params))
	if mn != bn: return False
	
	# Check this param
	mhs = ms.this
	bhs = bs.this
	if mhs != bhs: return False
	# TODO: Figure out why this check shouldn't be done
	#if mhs is not None and bhs is not None and not sem.isSubtype(bhs.valueType, mhs.valueType):
	#	return False
	
	# Check value param
	mhv = ms.value
	bhv = bs.value
	if mhv != bhv: return False
	if mhv is not None and bhv is not None and not sem.isSubtype(bhv.form.type, mhv.form.type):
		return False
	
	# Check ret type
	if not sem.isSubtype(ms.ret.type, bs.ret.type):
		return False
	
	# Check params
	mpl = ms.params
	bpl = bs.params
	if mpl is not None and bpl is not None:
		for mp, bp in zip(mpl, bpl):
			if not sem.isSubtype(bp.form.type, mp.form.type):
				return False
	
	return True

import os
import inspect
from typing import Iterator, IO, Any, TypeVar, Callable

def D(s: str, *args: object) -> None:
	print("DEBUG:", s.format(*args))

def TODO(s: str, *args: object) -> Any:
	dir = os.path.dirname(__file__)
	(_1, file, line, _2, _3, _4) = inspect.getouterframes(inspect.currentframe())[1]
	file = file[len(dir)+1:]
	file = file.replace('.py', '')
	file = file.replace('\\', '.')
	file = file.replace('/', '.')
	return NotImplementedError("{}/{}: {}".format(file, line, s.format(*args)))

class Messages:
	def __init__(self) -> None:
		self.count = 0
	
	def emit(self, msg, *args: object) -> None:
		self.count += 1
		print(msg.format(*args))
	
	@property
	def hasErrors(self) -> bool:
		return self.count > 0

class Naming:
	@classmethod
	def PackageInit(cls, package: str) -> str:
		return cls._impl('ip', package)
	
	@classmethod
	def Global(cls, globl: str) -> str:
		return globl
	
	@classmethod
	def GlobalInit(cls, globl: str) -> str:
		return cls._impl('ig', globl)
	
	@classmethod
	def FuncParamInit(cls, func: str, param: str) -> str:
		return func + cls._impl('ifp', param)
	
	@classmethod
	def Type(cls, type: str) -> str:
		return type
	
	@classmethod
	def TypeObj(cls, type: str) -> str:
		return cls._impl('to', type)
	
	@classmethod
	def TypeVtableType(cls, type: str) -> str:
		return cls._impl('vt', type)
	
	@classmethod
	def TypeVtableObj(cls, type: str) -> str:
		return cls._impl('vo', type)
	
	@classmethod
	def SymbolConstant(cls, sym: str) -> str:
		return cls._impl('syc', sym)
	
	@classmethod
	def SymbolType(cls, sym: str) -> str:
		return cls._impl('syt', sym)
	
	@classmethod
	def BoxedType(cls, type: str) -> str:
		return cls._impl('tb', type)
	
	@classmethod
	def _impl(cls, pre: str, s: str) -> str:
		return '${}_{}'.format(pre, s)

class Filesystem:
	@staticmethod
	def IsDir(path: str) -> bool:
		raise TODO("impl")
	
	@staticmethod
	def MakeDir(path: str) -> None:
		raise TODO("impl")
	
	@staticmethod
	def FindFilesWithExtension(path: str, ext: str) -> Iterator[str]:
		raise TODO("impl")
	
	@staticmethod
	def DirName(path: str) -> str:
		raise TODO("impl")
	
	@staticmethod
	def GetCurrentDir() -> str:
		raise TODO("impl")
	
	@staticmethod
	def PathExists(path: str) -> bool:
		raise TODO("impl")
	
	@staticmethod
	def RemovePath(path: str) -> None:
		raise TODO("impl")
	
	@staticmethod
	def Open(path: str, m: str) -> IO[Any]:
		return open(path, m)

def unescape(s: str) -> str:
	# TODO
	return s

T = TypeVar('T')
U = TypeVar('U')
def cached_property(f: Callable[[T], U]) -> U:
	tmp = cached_property_impl(f) # type: Any
	return tmp

class cached_property_impl:
	def __init__(self, func):
		self.func = func
		self.__doc__ = getattr(func, '__doc__')
	
	def __get__(self, instance, cls = None):
		if instance is None:
			return self
		f = self.func
		res = f(instance)
		instance.__dict__[f.__name__] = res
		return res

Options for implementing generics:

1. Erasure (currently implemented): Very easy to do, but performance penalty (non-ref types need boxing) and some type information is lost at runtime.
2. Reified, full: what C# does. Requires the source for all generic code to be available at runtime, as well as a compiler. It's the 100% correct solution. Runtime penalty to actually do the instantiations.
3. Reified, static: C++/D. Requires the source for all generic code (from dependencies too) at compile time. Not possible, in general, to do all instantiations at compile time: see Appendix/Reification Example.
4. Reified, limited static/dynamic: Like static, but disallow cases that produce infinite code instantions.

# Reified, limited static/dynamic: Details

See Appendix/Reification Example. Basic idea: disallow use of generics that increase the "depth" (C[T], depth 1, instantiates C[C[T]], depth 2).
But this restriction can be relaxed, since reference types can share compiled code, and only the type info needs to be created at runtime (which is much simpler than specializing code).
Therefore, as long as compiling only requires a finite number of instantiations for non-ref types, option #4 has almost the full power of option #2,
but does not require the complexity of having a compiler (plus source for all generic code) during runtime.

An example of a disallowed program is Appendix/Option 4 Limitation.

# Appendix: Test Cases

## Reification Example

```
role R { R f(); }
class C[T]: R { R f() -> C[C[T]](); }

// code that uses C[int]
```

C# can handle this. C++ and D try to instantiate all possible versions of `C` and error at a predefined depth.

## Option 4 Limitation

```
role R { R f(); }
class C[T](#struct): R { R f() -> C[C[T]](); }

// code that uses C[int]
```

This cannot be handled by options #3 and #4 because `f` needs to be instantiated for the infinite family of (non-ref) types:

```
C[int]
C[C[int]]
C[C[C[int]]]
...
```
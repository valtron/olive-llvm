"""
	Currently only implemented for win+mingw.
"""

"""
	http://www.yolinux.com/TUTORIALS/LibraryArchives-StaticAndDynamic.html
	
	.a (unix), .lib (win)
		archive library
		combination of multiple .o
		created by `ar`, with `ranlib` (or `ar -s`) to generate an index
		statically linked
	
	.so (unix), .dll (win)
		dynamic library
		must be compiled with -fPIC, -shared
	
	.dylib (mac)
		shared library
		mac distinguishes between "shared library" and "dynamically loadable module"
"""

"""
	TODO: Handle multiple versions
	
	gcc\mingw32\4.7.2\libgomp{,.dll}.a
	gcc\mingw32\4.7.2\libquadmath{,.dll}.a
	gcc\mingw32\4.7.2{,\debug}\libstdc++{,.dll}.a
	libmsvcr{}.a
	libd3dx{}d.a
	libmoldname{}.a
	libgettextpo{,.dll}.a
"""

from collections import defaultdict
import common

MINGW_ROOT = 'd:/dev/msys64/mingw64'

def main():
	lib = '{}/lib'.format(MINGW_ROOT)
	
	files = list(common.findext(lib, 'o')) + list(common.findext(lib, 'a'))
	
	symdefs = defaultdict(set)
	
	for f in files:
		for sym, defd in nm(f):
			if not defd: continue
			symdefs[sym].add(f)
	
	for sym, defs in symdefs.items():
		if len(defs) > 1:
			print(sym, ' '.join(defs))

def nm(filename):
	lines = common.shout('nm', '-P', '-g', filename).split('\n')
	
	for line in lines:
		if not line: continue
		parts = line.split(' ')
		if len(parts) < 2: continue
		(symbol, type, *rest) = parts
		assert len(type) == 1
		yield (symbol, type != 'U')

if __name__ == '__main__':
	common.run(main)

A type's layout defines how its values are represented in memory. The default, `ref`, is a pointer to:

```c
struct MyRefType {
	TypeInfo* ti;
	<layout of field 0> <field 0>;
	...
	<layout of field N-1> <field N-1>;
};
```

Only `ref` types can be subclassed.

A `ptr` layout is a pointer to another layout, but it can't be used directly; however, the `this` param is always either a `ref` (in `ref` types) or a `ptr` (in value types).

The other layouts are collectively called **value layouts**. The values of a value type can also be represented as `ref` through **boxing**. Current value layouts are:

* `b<N>` (`N` one of 0, 1, 8, 16, 32, 64): can represent up to 2^N values; types with this layout may not define any fields
* `struct`: equivalent to a C/++/# struct

`enum`s (formally type unions) generally use `ref` layout (by boxing everything non-`ref`), unless:

* they only have one option: same layout as that option
* they only have two options with `b0` layout: `b1`

# Void and Unit

The void type (a.k.a. "bottom") is a type that has no values: the empty set. A unit type is one which has exactly one value: a singleton set. In C-derived languages, the return type of functions that don't return anything is void; in functional languages, a unit type.

In Olive, `void` is a keyword for the unit type `{ #null }`, with some added restrictions:

* `void` can only be used for a return type
* `void` funcs may only return via `ret;` (`ret #null;` disallowed)

Because `void` is a unit type, it requires 0 bits and the compiler can omit writing data for a `ret;` statement. This optimization is more general: singleton values can be passed without using a register/stack. The following snippets of Olive and C are equivalent:

```
// Olive
class C(#b0) {
	void f(int x) { ... }
}
C g(int x, C c) {
	c.f(x);
	ret c;
}

// C
void C_f(int x) { ... }
void g(int x) {
	C_f(x);
}
```

Boxing a unit value involves no memory allocation: they will all be statically allocated. Unboxing is free.

The "proper" void type can also be expressed (an empty enum), but its only use would be to make a func uncallable (void param) or unreturning (void return).

# Bool

Bool is defined as `enum { #f; #t; }`. Since it has two values, it only needs `b1` layout. Practically, `b8` will be used. Also, by adding a rule that the integer representation of an enum of symbols should number them in alphabetical order, its values will be compiled to the expected 0 and 1.

However, so as to not generate an inordinate amount of enum-enum translation tables, if an enum contains more than two values, it will be promoted to `ref`. This still allows efficient conversion from a `b1` enum to any other:

```
enum E1 { #a; #b; }     // b1 layout
enum E2 { #a; #b; #c; } // ref layout

E2 f(E1 e) {
	// Compiles to: 
	// return (e ? <ptr to #b singleton> : <ptr to #a singleton>);
	ret e; 
}
```

# For the future

* despite being common, it's confusing to use `void` for a special unit type; is there a better word?
* by-reference parameters ala C#
* fixed-size array: layout for size known at compile- or run-time
* maybe have a way of doing C-style enums using a custom layout, e.g.

```
enum E(#b8) { #foo = 1; #bar = 2; #baz = 3; }
```

Requirements (newer might work):

* Python 3.5
* LLVM 3.8
* llvmlite 1.14
* GCC 5.4
* [cheech](https://gitlab.com/valtron/cheech) and [chong](https://gitlab.com/valtron/chong)

Build the library (lib-dev -> lib):

```
python script/buildlib.py
```

Run tests:

```
# Run all
python tests

# Run specific tests
python tests tests/dir/file.py::TestClass.test_method
```

Build an executable:

```
python olive exe <file or folder> [-d dependency1 dependency2 ...]
```

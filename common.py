import sys
import glob
import fnmatch
import subprocess
import os
import json
import platform
from os.path import dirname, isdir, join

def fmap(dir, inext, outdir, outext, func):
	for f in findext(dir, inext):
		fin = '{}/{}'.format(dir, f[len(dir) + 1:])
		fout = '{}/{}'.format(outdir, fin[len(dir) + 1:-len(inext)] + outext)
		
		ensure_dir(dirname(fout))
		
		func(fin, fout)

def findext(dir, ext):
	pattern = '*.{}'.format(ext)
	
	for root, dirnames, filenames in os.walk(dir):
		for filename in fnmatch.filter(filenames, pattern):
			yield join(root, filename).replace('\\', '/')

def shell(prog, *args):
	a = [prog]
	
	for arg in args:
		if isinstance(arg, list):
			a.extend(arg)
		else:
			a.append(arg)
	
	retcode = subprocess.check_call(a, stderr = subprocess.STDOUT)
	
	if retcode:
		raise Exception(retcode)

def which(prog):
	wh = {
		'windows': 'where'
	}.get(sysname(), 'which')
	
	return shout(wh, prog)

def sysname():
	return platform.system().lower()

def shout(prog, *args):
	a = [prog]
	
	for arg in args:
		if isinstance(arg, list):
			a.extend(arg)
		else:
			a.append(arg)
	
	return subprocess.check_output(a).decode().strip()

def read_json(filename):
	with open(filename, 'r') as fh:
		return json.load(fh)

def ensure_dir(dir):
	if not isdir(dir):
		os.makedirs(dir)

def toposort(objs, before):
	"""
		Topological sort of `objs`.
		
		objs: Iterable[T]
		before: (T x) -> (Iterable[T] res)
			objects in `res` are those that must come before `x`
	"""
	
	seen = set()
	
	def _add(x):
		if x in seen:
			return
		
		seen.add(x)
		
		for y in before(x):
			for z in _add(y):
				yield z
		
		yield x
	
	for x in objs:
		for y in _add(x):
			yield y

def run(f):
	f(*sys.argv[1:])

class Lib(object):
	def __init__(self, dir):
		self.dir = dir
		self._loaded = {}
	
	@property
	def packages(self):
		for thing in os.listdir(self.dir):
			path = '{}/{}'.format(self.dir, thing)
			
			if not isdir(path):
				continue
			
			yield thing
	
	def package_closure(self, pkg_names):
		return toposort(pkg_names, self.dependencies_of)
	
	def dependencies_of(self, pkg_name):
		return self.load(pkg_name).deps
	
	def load(self, pkg_name):
		if pkg_name not in self._loaded:
			self._loaded[pkg_name] = Pkg(self.dir, pkg_name)
		return self._loaded[pkg_name]

class Pkg(object):
	def __init__(self, lib, name):
		self.lib = lib
		self.name = name
		self.dir = '{}/{}'.format(lib, name)
		self.manifest = read_json('{}/{}/package.json'.format(lib, name))
		self.deps = self.manifest['deps']
		self.link = self.manifest.get('link') or []
	
	def get_object_files(self):
		return findext('{}/obj'.format(self.dir), 'o')

from os.path import join
from cheech.codegen import Codegen
from cheech.algos import State

from .language import (
	SIClass, SIEnum, get_unique_action, init_parser,
	Shift, Accept, Reduce, SAPass, SANew, SAListNew, SAListPush,
	if_tree,
)

def run(lang, outdir):
	pylite_ast = join(outdir, 'phase', 'ast')
	pylite_parse = join(outdir, 'trans', 'parse')
	
	tokenids = lang.tokenids
	grammar = lang.grammar
	
	with open(join(pylite_ast, 'gentokens.py'), 'w', newline = '\n') as fh:
		cg = PythonCodegen(fh)
		sorted_tokens = sorted(tokenids.values(), key = lambda tt: tt.codename)
		for tt in sorted_tokens:
			cg.line('{} = {!r}', tt.codename, tt.id)
		cg.line('KEYWORDS = {{')
		cg.indent()
		for tt in sorted_tokens:
			if tt.id >= 100:
				cg.line('{!r}: {!r},', tt.lexname, tt.id)
		cg.dedent()
		cg.line('}}')
	
	with open(join(pylite_parse, 'genlexer.py'), 'w', newline = '\n') as fh:
		cg = PythonLexerCodegen(fh, lang.lexerspec, tokenids)
		cg.generate('RawLexer')
	
	with open(join(pylite_ast, 'genast.py'), 'w', newline = '\n') as fh:
		cg = PythonCodegen(fh)
		cg.line('from typing import Optional, Union')
		cg.line('from .base import Syntax, Expr, Stmt, Range, ListSyntax')
		for _, nt in sorted(grammar.nonterms.items()):
			generate_ast_class(grammar, cg, nt)
	
	with open(join(pylite_parse, 'genparser.py'), 'w', newline = '\n') as fh:
		cg = PythonParserCodegen(fh, lang)
		cg.generate()

REPLACEMENTS = {
	'$base': 'Syntax',
	'$token': 'Token',
	'$list': 'ListSyntax',
}

def generate_ast_class(grammar, cg, nt):
	si = nt.si
	
	if isinstance(si, SIClass):
		cg.begin('class {}({})', nt.name, _replacements(si.base))
		cg.line('__slots__ = {!r}', tuple(f.name for f in si.fields))
		cg.line('def __init__(self, range: Range')
		cg.indent()
		for f in si.fields:
			cg.line(', {}: {!r}', f.name, _replacements(f.type))
		cg.line(') -> None:')
		cg.line('super().__init__(range)')
		for f in si.fields:
			cg.line('self.{0} = {0}', f.name)
		cg.dedent()
		cg.end()
		cg.blank()
		return
	
	if isinstance(si, SIEnum):
		cg.line('{} = Union[', nt.name)
		cg.indent()
		for _, opt in sorted((opt is not None, opt) for opt in si.options):
			cg.line('{!r},', opt)
		cg.dedent()
		cg.line(']')
		cg.blank()
		return

def _replacements(s):
	for k, v in REPLACEMENTS.items():
		s = s.replace(k, v)
	if s.endswith('?'):
		s = 'Optional[{}]'.format(s[:-1])
	return s

class PythonCodegen(Codegen):
	def begin(self, line, *args, **kwargs):
		self.line(line + ':', *args, **kwargs)
		self.indent()
	
	def end(self):
		self.dedent()

class PythonParserCodegen(PythonCodegen):
	def __init__(self, fh, lang):
		super().__init__(fh)
		init_parser(self, lang)
	
	def generate(self):
		self.line('from olive.phase.ast import *')
		self.line('from .base import Accept, Shift, Reduce')
		self._action()
		self._reduce()
		self._goto()
	
	def _action(self):
		self.line('ACTIONS = {{')
		self.indent()
		for sid, sacts in self.actions.items():
			act = get_unique_action(list(sacts.values()))
			if act:
				self.line('{}: {},', sid - 500, _sa_tocode(act))
			else:
				for tid, act in sacts.items():
					self.line('{}: {},', sid * 199 + tid, _sa_tocode(act))
		self.dedent()
		self.line('}}')
	
	def _reduce(self):
		self.line('REDUCE = {{')
		self.indent()
		for rcid, rdx in enumerate(self.reductions):
			self.line('{}: (lambda S, r: {}),', rcid, _si_tocode(rdx, self.grammar))
		self.dedent()
		self.line('}}')
	
	def _goto(self):
		self.line('GOTO = {{')
		self.indent()
		for sid, sgoto in self.goto.items():
			for hcid, next_sid in sgoto.items():
				self.line('{}: {},', sid * 199 + hcid, next_sid)
		self.dedent()
		self.line('}}')

class PythonLexerCodegen(PythonCodegen):
	def __init__(self, fh, states, token_types):
		assert len(states) > 0
		
		super().__init__(fh)
		
		# Map[state name, state]
		self.state_by_name = states
		# Map[state ids (sid), consecutive ints (cid)]
		self.state_id_map = dict(
			(sid, cid) for (cid, sid) in enumerate(sorted([st.id for st in states.values() ]))
		)
		# Map[cid, state]
		self.state_by_cid = { self.state_id_map[st.id]: st for st in states.values() }
		
		self.state_id_map[999999] = len(self.state_by_cid)
		self.state_by_cid[len(self.state_by_cid)] = None
		
		self.token_types = token_types
	
	def generate(self, clsname):
		self.line('from olive.phase.ast import Token')
		self.line('from .base import FileWindow, charclass')
		
		self.begin('class {}', clsname)
		self._ctor()
		self._current()
		self._moveNext()
		self._emitMethod()
		self.end()
	
	def _ctor(self):
		self.begin('def __init__(self, fw: FileWindow) -> None')
		self.line('self.fw = fw')
		self.line('self.t = None # type: Token')
		self._goto(None, self.state_by_name['START'])
		self._step()
		self.end()
	
	def _moveNext(self):
		self.begin('def moveNext(self) -> bool')
		self.line('fw = self.fw')
		self.begin('while True')
		self.line('s = self.s')
		self.line('c = self.c')
		if_tree(self, self._moveNext_leaf, 's', 0, len(self.state_by_cid))
		self.end()
		self.end()
	
	def _moveNext_leaf(self, cid):
		st = self.state_by_cid[cid]
		
		if st is None:
			self._ret(False)
			return
		
		had_else = False
		
		sorted_edges = sorted(st.edges.items(), key = lambda pair: pair[0] is None)
		
		for term, action in sorted_edges:
			if term:
				self.begin('if {}', self._cond(term))
			else:
				self.begin('else')
				had_else = True
			
			sp = self.state_by_name[action.goto]
			
			if term:
				self._step()
			
			if action.redux:
				self._emit(action.redux)
				self._goto(st, sp)
				self._ret(True)
			else:
				if not term:
					self._step()
				self._goto(st, sp)
				self.line('continue')
			
			self.end()
		
		if st.id == State.START:
			self.begin('if c == {!r}', '\x00')
			self.begin('if fw.j > fw.i')
			self._emit('ERR')
			self._ret(True)
			self.end()
			self._emit('$')
			self.line('self.s = {!r}', 999999)
			self._ret(True)
			self.end()
			
			self.begin('else')
			self._step()
			self._emit('ERR')
			self._ret(True)
			self.end()
		elif not had_else:
			self.begin('else')
			self._emit('ERR')
			self._goto(st, self.state_by_name['START'])
			self._ret(True)
			self.end()
	
	def _emit(self, tok):
		self.line('self._emit({!r})', self.token_types[tok].id)
	
	def _goto(self, current, state):
		if not current or current.id != state.id:
			self.line('self.s = {!r}', self.state_id_map[state.id])
	
	def _step(self):
		self.line('self.c = fw.step()')
	
	def _ret(self, b):
		self.line('return {!r}', b)
	
	def _emitMethod(self):
		self.begin('def _emit(self, tt: int) -> None')
		self.line('self.t = self.fw.flush(tt)')
		self.end()
	
	def _current(self):
		self.line('@property')
		self.begin('def current(self) -> Token')
		self.line('return self.t')
		self.end()
	
	def _cond(self, term):
		if term.type == 'c':
			cond = 'c == {!r}'.format(term.name)
		else:
			cond = 'charclass.{}(c)'.format(term.name)
		
		if term.neg:
			cond = 'not ({})'.format(cond)
		
		return cond

def _sa_tocode(sa):
	if isinstance(sa, Shift):
		return 'Shift({!r})'.format(sa.scid)
	
	if isinstance(sa, Reduce):
		return 'Reduce({!r}, {!r}, {!r})'.format(sa.hcid, sa.rcid, len(sa.r.body))
	
	if isinstance(sa, Accept):
		return 'Accept'
	
	assert False

def _si_tocode(rdx, grammar):
	r = rdx.r
	nt = rdx.nt
	si = r.si
	
	if isinstance(si, SAPass):
		if si.idx is None:
			return 'None'
		return 'S[{}]'.format(si.idx)
	
	if isinstance(si, SANew):
		fields = grammar.nonterms[si.type].si.fields
		idxes = {
			sr.si: i for i, sr in enumerate(r.body)
		}
		args = ['r']
		for f in fields:
			idx = idxes.get(f.name)
			if idx is None:
				args.append('None')
			else:
				args.append('S[{}]'.format(idx))
		return '{}({})'.format(si.type, ', '.join(args))
	
	if isinstance(si, SAListNew):
		return 'ListSyntax(r{})'.format(
			', S[{}]'.format(si.idx_item) if si.idx_item is not None else ''
		)
	
	if isinstance(si, SAListPush):
		return 'S[{}].push(S[{}], r.end)'.format(
			si.idx_list, si.idx_item
		)
	
	raise NotImplementedError("unknown SA subclass: '{}'".format(type(si)))

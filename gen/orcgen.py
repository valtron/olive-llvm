from os.path import join
from cheech.codegen import Codegen
from cheech.algos import State
from chong.models import Rule, SymRef, Term, NonTerm

from .language import (
	SIClass, SIEnum, get_unique_action, init_parser,
	Shift, Reduce, Accept, SANew, SAPass, SAListNew,
	SAListPush, if_tree
)

def run(lang, outdir):
	orc_ast = join(outdir, 'orc.ast', 'src')
	orc_parse = join(outdir, 'orc.parse', 'src')
	
	tokenids = lang.tokenids
	grammar = lang.grammar
	
	with open(join(orc_ast, 'tokens.ol'), 'w', newline = '\n') as fh:
		cg = OliveCodegen(fh)
		cg.line('module orc.ast.tokens;')
		for tt in sorted(tokenids.values(), key = lambda tt: tt.codename):
			cg.line('int {} = {!r};', tt.codename, tt.id)
	
	with open(join(orc_parse, 'rawlexer.ol'), 'w', newline = '\n') as fh:
		cg = OliveLexerCodegen(fh, lang.lexerspec, tokenids)
		cg.generate('RawLexer')
	
	with open(join(orc_ast, 'astgen.ol'), 'w', newline = '\n') as fh:
		cg = OliveCodegen(fh)
		cg.line('module orc.ast;')
		for _, nt in sorted(grammar.nonterms.items()):
			generate_ast_class(grammar, cg, nt)
	
	with open(join(orc_parse, 'rawparser.ol'), 'w', newline = '\n') as fh:
		OrcParserCodegen(fh, lang).generate()

REPLACEMENTS = {
	'$base': 'Syntax',
	'$token': 'Token',
	'$list': 'ListSyntax',
}

def generate_ast_class(grammar, cg, nt):
	si = nt.si
	
	if isinstance(si, SIClass):
		cg.begin('class {}: {}', nt.name, _replacements(si.base))
		for f in si.fields:
			cg.line('{} {};', _replacements(f.type), f.name)
		cg.line('new (')
		cg.indent()
		cg.line('Location start, Location end')
		for f in si.fields:
			cg.line(', {} {}', _replacements(f.type), f.name)
		cg.dedent()
		cg.begin(') ')
		cg.line('this.start = start; this.end = end;')
		for f in si.fields:
			cg.line('this.{0} = {0};', f.name);
		cg.end()
		cg.end()
		return
	
	if isinstance(si, SIEnum):
		cg.begin('enum {}', nt.name)
		for _, opt in sorted((opt is not None, opt) for opt in si.options):
			cg.line('{};', opt or '#null')
		cg.end()
		return

def _replacements(s):
	for k, v in REPLACEMENTS.items():
		s = s.replace(k, v)
	return s

class OliveCodegen(Codegen):
	def begin(self, line, *args, **kwargs):
		self.line(line + ' {{', *args, **kwargs)
		self.indent()
	
	def end(self):
		self.dedent()
		self.line('}}')

class OrcParserCodegen(OliveCodegen):
	def __init__(self, fh, lang):
		super().__init__(fh)
		init_parser(self, lang)
	
	def generate(self):
		self.line('module orc.parse;')
		self.line('use orc: ast;')
		self.line('use tmpdev: List;')
		self.line('use olive.lang: cast;')
		self.line('use orc.ast: Syntax, Stmt, Expr, ListSyntax;')
		self.line('use orc.ast: {};', ', '.join(sorted(
			nt.name for nt in self.grammar.nonterms.values()
			if isinstance(nt.si, SIClass) or isinstance(nt.si, SIEnum)
		)))
		
		self._action()
		self._reduce()
		self._goto()
	
	def _action(self):
		self.begin('void init_actions()')
		self.line('my A = ACTION;')
		for sid, sacts in self.actions.items():
			act = get_unique_action(list(sacts.values()))
			if act:
				self.line('A({}) = {};', sid - 500, _sa_tocode(act))
			else:
				for tid, act in sacts.items():
					self.line('A({}) = {};', sid * 199 + tid, _sa_tocode(act))
		self.end()
	
	def _reduce(self):
		self.begin('Syntax reduce(int rid, List[any] S, Location start, Location end)')
		if_tree(self, self._reduce_leaf, 'rid', 0, len(self.reductions))
		self.end()
	
	def _reduce_leaf(self, rcid):
		rdx = self.reductions[rcid]
		l = len(rdx.r.body)
		self.line('ret {};', _si_tocode(rdx, self.grammar))
	
	def _goto(self):
		self.begin('void init_goto()')
		self.line('my G = GOTO;')
		for sid, sgoto in self.goto.items():
			for hcid, next_sid in sgoto.items():
				self.line('G({}) = {};', sid * 199 + hcid, next_sid)
		self.end()

class OliveLexerCodegen(OliveCodegen):
	def __init__(self, fh, states, token_types):
		assert len(states) > 0
		
		super().__init__(fh)
		
		# Map[state name, state]
		self.state_by_name = states
		# Map[state ids (sid), consecutive ints (cid)]
		self.state_id_map = dict(
			(sid, cid) for (cid, sid) in enumerate(sorted([st.id for st in states.values() ]))
		)
		# Map[cid, state]
		self.state_by_cid = { self.state_id_map[st.id]: st for st in states.values() }
		
		self.state_id_map[999999] = len(self.state_by_cid)
		self.state_by_cid[len(self.state_by_cid)] = None
		
		self.token_types = token_types
	
	def generate(self, clsname):
		self.line('module orc.parse;')
		self.line('use olive.lang: denull;')
		self.line('use orc.ast: Token;')
		self.line('use tmpdev: List;')
		
		self.begin('class {}', clsname)
		self._fields()
		self._ctor()
		self._current()
		self._moveNext()
		self._emitMethod()
		self.end()
	
	def _fields(self):
		self.line('FileWindow fh;')
		self.line('Token? t;')
		self.line('int s;')
		self.line('int c;')
	
	def _ctor(self):
		self.begin('new (FileWindow fh)')
		self.line('this.fh = fh;')
		self.line('this.t = #null;')
		self._goto(None, self.state_by_name['START'])
		self._step()
		self.end()
	
	def _moveNext(self):
		self.begin('bool moveNext()')
		self.line('my fh = this.fh;')
		self.begin('loop')
		self.line('my s = this.s;')
		self.line('my c = this.c;')
		if_tree(self, self._moveNext_leaf, 's', 0, len(self.state_by_cid))
		self.end()
		self.end()
	
	def _moveNext_leaf(self, cid):
		st = self.state_by_cid[cid]
		
		if st is None:
			self._ret(False)
			return
		
		had_else = False
		
		sorted_edges = sorted(st.edges.items(), key = lambda pair: pair[0] is None)
		
		for term, action in sorted_edges:
			if term:
				self.begin('if {}', self._cond(term))
			else:
				self.begin('else')
				had_else = True
			
			sp = self.state_by_name[action.goto]
			
			if term:
				self._step()
			
			if action.redux:
				self._emit(action.redux)
				self._goto(st, sp)
				self._ret(True)
			else:
				if not term:
					self._step()
				self._goto(st, sp)
				self.line('next;')
			
			self.end()
		
		if st.id == State.START:
			self.begin('if c is -1')
			self.begin('if fh._j > fh._i')
			self._emit('ERR')
			self._ret(True)
			self.end()
			self._emit('$')
			self.line('this.s = {!r};', 999999)
			self._ret(True)
			self.end()
			
			self.begin('else')
			self._step()
			self._emit('ERR')
			self._ret(True)
			self.end()
		elif not had_else:
			self.begin('else')
			self._emit('ERR')
			self._goto(st, self.state_by_name['START'])
			self._ret(True)
			self.end()
	
	def _emit(self, tok):
		self.line('this._emit({!r});', self.token_types[tok].id)
	
	def _goto(self, current, state):
		if not current or current.id != state.id:
			self.line('this.s = {!r};', self.state_id_map[state.id])
	
	def _step(self):
		self.line('this.c = fh.step();')
	
	def _ret(self, b):
		self.line('ret #{};', 't' if b else 'f')
	
	def _emitMethod(self):
		self.begin('void _emit(int tt)')
		self.line('this.t = Token(this.fh.start, this.fh.end, tt, this.fh.flush());')
		self.end()
	
	def _current(self):
		self.line('Token current -> denull[Token](this.t);')
	
	def _cond(self, term):
		if term.type == 'c':
			cond = 'c is {!r}'.format(ord(term.name))
		else:
			cond = 'charclass.{}(c)'.format(term.name)
		
		if term.neg:
			cond = 'not ({})'.format(cond)
		
		return cond

def _sa_tocode(sa):
	if isinstance(sa, Shift):
		return 'Shift({!r})'.format(sa.scid)
	
	if isinstance(sa, Reduce):
		return 'Reduce({!r}, {!r}, {!r})'.format(sa.hcid, sa.rcid, len(sa.r.body))
	
	if isinstance(sa, Accept):
		return '#accept'
	
	assert False

def _si_tocode(rdx, grammar):
	r = rdx.r
	nt = rdx.nt
	si = r.si
	
	if isinstance(si, SAPass):
		if si.idx is None:
			return 'cast[Syntax](#null)'
		return 'S({})'.format(si.idx)
	
	if isinstance(si, SANew):
		fields = grammar.nonterms[si.type].si.fields
		idxes = {
			sr.si: i for i, sr in enumerate(r.body)
		}
		args = ['start', 'end']
		for f in fields:
			idx = idxes.get(f.name)
			if idx is None:
				args.append('#null')
			else:
				args.append('S({})'.format(idx))
		return '{}({})'.format(si.type, ', '.join(args))
	
	if isinstance(si, SAListNew):
		return '{}[{}](start, end{})'.format(
			'ListSyntax', nt.si.type,
			', S({})'.format(si.idx_item) if si.idx_item is not None else ''
		)
	
	if isinstance(si, SAListPush):
		return 'cast[{}](S({})).push(end, S({}))'.format(
			nt.si.ast_type(nt, grammar), si.idx_list, si.idx_item
		)
	
	raise NotImplementedError("unknown SA subclass: '{}'".format(type(si)))

from os.path import join
from cheech import specfile as lexerspec
from cheech.codegen import split_range
from chong import specfile as grammarspec
from chong.algos import parse_tables
from chong.codegen import GLRParser
from chong.models import Rule, SymRef, Term, NonTerm

class Language:
	def __init__(self, datadir: str):
		self.tokenids = parse_tokenids(join(datadir, 'tokenids.txt'))
		self.lexerspec = lexerspec.parse(join(datadir, 'lexicon.txt'))
		
		grammar = grammarspec.parse(join(datadir, 'grammar.txt'))
		for tn in grammar.terms.keys():
			assert tn.replace('\'', '') in self.tokenids
		postprocess_grammar(grammar)
		self.grammar = grammar
		self.grammarspec = parse_tables(grammar)
		
		#profile_reduce_actions(self)

def parse_tokenids(filename):
	curid = None
	
	tokenids = {}
	
	with open(filename, 'r') as fh:
		for line in fh:
			indented = line.startswith('\t')
			line = line.strip()
			
			if not line:
				continue
			
			if not indented:
				curid = int(line)
				continue
			
			parts = line.split()
			
			lexname = parts[0]
			
			if len(parts) < 2:
				codename = lexname
			else:
				codename = parts[1]
			
			tokenids[lexname] = TokenType(curid, lexname, codename)
			curid += 1
	
	return tokenids

class TokenType(object):
	def __init__(self, id, lexname, codename):
		self.id = id
		self.lexname = lexname
		self.codename = codename

def profile_reduce_actions(language):
	import glob
	from cheech.codegen import PythonCodegen
	from collections import Counter
	
	lexerspec = language.lexerspec
	(raw_actions, raw_goto) = language.grammarspec
	tokenids = language.tokenids
	
	keywords = {
		k for k, v in tokenids.items()
		if v.id >= 100
	}
	counter = Counter()
	
	with StringIO() as out:
		clsname = 'Toker'
		PythonCodegen(out, lexerspec).generate(clsname)
		env = {}
		exec(compile(out.getvalue(), '<string>', 'single'), env)
		Toker = env[clsname]
	fs = LexerFs()
	
	ignorable = { 'WS', 'CSL', 'CML' }
	
	files = glob.iglob('lib-dev/**/*.ol', recursive = True)
	for file in files:
		with open(file) as fh:
			content = fh.read()
		toker = Toker(content, fs)
		counter_file = Counter()
		parser = ProfilingParser(raw_actions, raw_goto, counter_file)
		while toker.moveNext() and parser.states:
			(tt, i, j, txt) = toker.current
			if tt in ignorable: continue
			if tt == 'Id' and txt in keywords:
				tt = txt
			if tt not in { 'Id', 'ERR', Term.END.name, 'Str', 'Num', 'Sym' }:
				tt = '\'{}\''.format(tt)
			parser.process(tt)
		parser.process(Term.END.name)
		if len(parser.res) != 1:
			raise Exception("can't parse '{}'".format(file))
		
		total = sum(counter_file.values())
		for k, v in counter_file.items():
			counter[k] += v / total
	
	print('rdx,perc,count')
	total = sum(counter.values())
	for rdx, n in counter.most_common():
		print('{},{},{}'.format(rdx, n/total, n))

class ProfilingParser(GLRParser):
	def __init__(self, raw_actions, raw_goto, counter):
		super().__init__((raw_actions, raw_goto), maxstates = 1)
		self.counter = counter
	
	def _reduce_action(self, nt, r, children):
		shape = r.si.shape(r, nt)
		self.counter[shape] += 1
		return None

class LexerFs:
	def a(self, c):
		return c.isalpha() or c == '_'
	
	def n(self, c):
		return c.isdigit()
	
	def an(self, c):
		return c.isalnum() or c == '_'
	
	def ws(self, c):
		return c.isspace()
	
	def nnl(self, c):
		return c and c != '\n'

def postprocess_grammar(grammar):
	nonterms = list(grammar.nonterms.values())
	for nt in nonterms:
		nt.si = _parse_nonterm_si(grammar, nt)
		for r in nt.rules:
			if isinstance(r.si, str):
				r.si = _parse_rule_si(grammar, nt, r)
	
	# The previous step could have added nonterms.
	# Anything that adds nonterms during this step is
	# expected to create them fully initialized.
	nonterms = list(grammar.nonterms.values())
	for nt in nonterms:
		# Attach SI to rules
		nt.si.process(nt, grammar)
		for r in nt.rules:
			r.si.infer(r, nt, grammar)

def _parse_rule_si(grammar, nt, r):
	args, kwargs = _tokenize_si(r.si)
	if not args:
		raise Exception("Unknown SI on rule: `{}`".format(r.si))
	t = args[0]
	args = args[1:]
	
	if t == 'new':
		return SANew(args[0])
	
	raise Exception("Unknown SI on rule: `{}`".format(t))

def _parse_nonterm_si(grammar, nt):
	args, kwargs = _tokenize_si(nt.si)
	if not args:
		raise Exception("Unknown SI on nonterm `{}`: `{}`".format(nt.name, nt.si))
	t = args[0]
	args = args[1:]
	
	if t == 'class':
		return SIClass(args[0], [
			SIClass.Field(idx, name, type)
			for idx, (name, type) in enumerate(kwargs.items())
		])
	if t == 'enum':
		return SIEnum([
			_get_pass_symref(r) for r in nt.rules
		])
	if t == 'list':
		item = kwargs.get('item')
		sep = kwargs.get('sep')
		nonempty = (kwargs.get('nonempty') == 'true')
		if sep:
			sep = grammar.terms[sep]
		return SIList(args[0], sep = sep, item = item, nonempty = nonempty)
	if t == 'syntax':
		return SISyntax(args[0])
	if t == 'op':
		ast_type = args[0]
		unary = args[1]
		binary = args[2]
		_create_op_rules(grammar, nt, ast_type, unary, binary)
		return SISyntax(ast_type)
	
	raise Exception("Unknown SI on nonterm `{}`: `{}`".format(nt.name, t))

def _create_op_rules(grammar, nt, ast_type, unary, binary):
	assert not nt.rules
	
	namebase = nt.name
	i = 0
	while True:
		ops_pre = grammar.nonterms.get('{}_{}_pre'.format(namebase, i))
		ops_inl = grammar.nonterms.get('{}_{}_inl'.format(namebase, i))
		ops_inr = grammar.nonterms.get('{}_{}_inr'.format(namebase, i))
		
		if not any((ops_pre, ops_inl, ops_inr)):
			break
		
		nt_next = NonTerm('{}_{}'.format(namebase, i), SISyntax(ast_type), [])
		
		if ops_pre:
			nt.rules.append(Rule(
				[SymRef(ops_pre, 'op'), SymRef(nt, 'arg')], SANew(unary)
			))
		
		if ops_inl:
			nt.rules.append(Rule(
				[SymRef(nt, 'lhs'), SymRef(ops_inl, 'op'), SymRef(nt_next, 'rhs')], SANew(binary)
			))
		
		if ops_inr:
			nt.rules.append(Rule(
				[SymRef(nt_next, 'lhs'), SymRef(ops_inr, 'op'), SymRef(nt, 'rhs')], SANew(binary)
			))
		
		nt.rules.append(Rule([SymRef(nt_next, None)], None))
		
		grammar.nonterms[nt_next.name] = nt_next
		nt = nt_next
		
		i += 1
	
	atom = grammar.nonterms['{}_atom'.format(namebase)]
	
	nt.rules.append(Rule(
		[SymRef(atom, None)], None
	))

class SA:
	def shape(self, r, nt):
		return 'uniq/{}/{}'.format(nt.name, nt.rules.index(r))

class SANew(SA):
	def __init__(self, type):
		self.type = type
	
	def infer(self, r, nt, grammar):
		for sr in r.body:
			if sr.sym: continue
			field = sr.si
			for f in nt.si.fields:
				if field != f.name: continue
				sr.sym = _get_symbol(grammar, f.type)
				break

class SAPass(SA):
	def __init__(self, idx):
		self.idx = idx
	
	def infer(self, r, nt, grammar):
		pass
	
	def shape(self, r, nt):
		return 'pass/{}'.format(self.idx)

class SAListNew(SA):
	def __init__(self, idx_item):
		self.idx_item = idx_item
	
	def infer(self, r, nt, grammar):
		for sr in r.body:
			if sr.sym: continue
			si = sr.si
			if si == 'list':
				sr.sym = nt
			elif si == 'push':
				sr.sym = _get_symbol(grammar, nt.si.item)

class SAListPush(SA):
	def __init__(self, idx_list, idx_item):
		self.idx_list = idx_list
		self.idx_item = idx_item
	
	infer = SAListNew.infer

class SIClass:
	def __init__(self, base, fields):
		self.base = base
		self.fields = fields
	
	def process(self, nt, grammar):
		for r in nt.rules:
			if r.si: continue
			r.si = SANew(nt.name)
	
	def ast_type(self, nt, grammar):
		return nt.name
	
	class Field:
		def __init__(self, idx, name, type):
			self.idx = idx
			self.name = name
			self.type = type

class SIEnum:
	def __init__(self, options):
		self.options = options
	
	def process(self, nt, grammar):
		new_options = set()
		
		for r, idx in zip(nt.rules, self.options):
			assert r.si is None
			if idx is None:
				new_options.add(None)
			else:
				sym = r.body[idx].sym
				if isinstance(sym, Term):
					ast_type = 'Token'
				else:
					ast_type = sym.si.ast_type(sym, grammar)
				new_options.add(ast_type)
			r.si = SAPass(idx)
		
		self.options = new_options
	
	def ast_type(self, nt, grammar):
		return nt.name

class SIList:
	def __init__(self, type, sep = None, item = None, nonempty = False):
		self.type = type
		self.item = item or type
		self.sep = sep
		self.nonempty = nonempty
	
	def process(self, nt, grammar):
		assert not nt.rules
		
		rcons = [SymRef(None, 'list')]
		if self.sep:
			rcons.append(SymRef(self.sep))
		rcons.append(SymRef(None, 'push'))
		list_rules = [
			Rule([SymRef(None, 'push')], si = SAListNew(0)),
			Rule(rcons, si = SAListPush(0, 2 if self.sep else 1)),
		]
		
		if self.nonempty:
			nt.rules.extend(list_rules)
		else:
			nt_nonempty = NonTerm(nt.name + '_nonempty', rules = list_rules,
				si = SIList(self.type, self.sep, self.item, nonempty = True)
			)
			grammar.nonterms[nt_nonempty.name] = nt_nonempty
			for r in nt_nonempty.rules:
				r.si.infer(r, nt_nonempty, grammar)
			nt.rules.extend([
				Rule([], SAListNew(None)),
				Rule([SymRef(nt_nonempty)], si = SAPass(0))
			])
	
	def ast_type(self, nt, grammar):
		return '{}[{}]'.format('ListSyntax', self.type)

class SISyntax:
	def __init__(self, type):
		self.type = type
	
	def process(self, nt, grammar):
		for r in nt.rules:
			if r.si: continue
			r.si = SAPass(_get_pass_symref(r))
	
	def ast_type(self, nt, grammar):
		return self.type

def _get_symbol(grammar, name):
	if name in grammar.nonterms:
		return grammar.nonterms[name]
	return grammar.terms[name]

def _get_pass_symref(rule):
	for i, sr in enumerate(rule.body):
		if sr.si == 'pass':
			return i
	for i, sr in enumerate(rule.body):
		if sr.si != 'skip':
			return i
	return None

def _tokenize_si(s):
	from collections import OrderedDict
	
	args = []
	kwargs = OrderedDict()
	for p in s.split():
		if ':' in p:
			(k, v) = p.split(':', 1)
			kwargs[k.strip()] = v.strip()
		else:
			args.append(p)
	
	return args, kwargs

def get_unique_action(actions):
	act = actions[0]
	for a in actions[1:]:
		if a != act:
			return None
	return act

def init_parser(self, lang):
	self.grammar = lang.grammar
	raw_actions, raw_goto = lang.grammarspec
	tokenids = lang.tokenids
	
	# hcid: nonterminal id, [0, n)
	# rcid: unique reduction id, [0, m)
	# h: nonterminal
	
	# Map[sid, Map[tid, Action]]
	self.actions = {}
	# Map[rcid, Reduction]
	self.reductions = []
	# Map[sid, Map[hcid, sid]]
	self.goto = {}
	
	mhid = CID()
	
	for (src_sid, sym), dest_sid in raw_goto.items():
		hcid = mhid.add(sym)
		if src_sid not in self.goto:
			self.goto[src_sid] = {}
		self.goto[src_sid][hcid] = dest_sid
	
	reductions_by_shape = {}
	
	for (sid, tok), acts in raw_actions.items():
		act = _resolve_conflicts(acts)
		
		if act.type == 'r':
			(nt, ridx) = act.value
			rule = nt.rules[ridx]
			
			shape = rule.si.shape(rule, nt)
			
			if shape not in reductions_by_shape:
				rcid = len(self.reductions)
				reductions_by_shape[shape] = rcid
				a = Reduce(nt, rule, mhid.to_cid[nt], rcid)
				self.reductions.append(a)
			else:
				rcid = reductions_by_shape[shape]
				a = Reduce(nt, rule, mhid.to_cid[nt], rcid)
		elif act.type == 's':
			a = Shift(act.value)
		else:
			a = Accept()
		
		if sid not in self.actions:
			self.actions[sid] = {}
		self.actions[sid][tokenids[tok.name.replace('\'', '')].id] = a

class Shift:
	def __init__(self, scid):
		self.scid = scid
	
	def __eq__(self, other):
		if not isinstance(other, Shift):
			return False
		return other.scid == self.scid

class Reduce:
	def __init__(self, nt, r, hcid, rcid):
		self.nt = nt
		self.r = r
		self.hcid = hcid
		self.rcid = rcid
	
	def __eq__(self, other):
		if not isinstance(other, Reduce):
			return False
		return other.r == self.r

class Accept:
	def __eq__(self, other):
		return isinstance(other, Accept)

class CID:
	def __init__(self):
		self.to_cid = {}
		self.from_cid = []
	
	def add(self, id):
		if id not in self.to_cid:
			cid = len(self.from_cid)
			self.to_cid[id] = cid
			self.from_cid.append(id)
		return self.to_cid[id]

def _resolve_conflicts(actions):
	assert len(actions) == 1
	for a in actions: return a
	# Prefer shifts, otherwise pick an arbitrary action
	#return sorted(actions, key = lambda act: 0 if act.type == 's' else 1)[0]

def if_tree(codegen, leaf, var, a, b):
	if b - a < 2:
		leaf(a)
		return
	
	(la, lb), (ra, rb) = split_range(a, b)
	
	codegen.begin('if {} < {!r}', var, ra)
	if_tree(codegen, leaf, var, la, lb)
	codegen.end()
	
	codegen.begin('else')
	if_tree(codegen, leaf, var, ra, rb)
	codegen.end()

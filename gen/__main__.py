def main(*args):
	from gen.language import Language
	from gen import orcgen, pygen
	l = Language('gen/data')
	orcgen.run(l, 'lib-dev')
	pygen.run(l, 'olive')

if __name__ == '__main__':
	import sys
	sys.exit(main(*sys.argv[1:]))
